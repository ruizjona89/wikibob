# Spring Exception Handling

# Links

- [Spring Boot Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#application-properties.server)
- [Spring Boot Exception Handling](https://reflectoring.io/spring-boot-exception-handling/)

# Configuration

- src/main/resources/application.yml
     - format for server.error.include-message: always would be as follows:

```
server:
     error:
         include-message: always
         include-binding-errors: always
         include-stacktrace: on-trace-param
         include-exception: false
```

|Stack Trace On Trace Param|Always|
|--|--|
|[On Trace Param}(stackTraceOnTraceParam.png)|[Always](stackTraceAlways.json)