# You Learn better by Teaching

- [DeVry University](https://gitlab.com/bobby.estey/devrystudent)
- [Grand Canyon University](https://gitlab.com/bobby.estey/gcuStudent)

**A picture is worth a thousand words.  A video is worth a million.**

"If you want to understand a subject, then teach" (Estey, 2000)

I have mentioned my experiences with facilitating and teaching.  I have a statement, “If you want to understand a subject, then teach”.  I have personally created technical videos from some of my experiences and I will continue to perform this duty as this clarifies (ideas, processes, examples, concepts) to others who are interested in computing technology.

Starting in the year 2000, my personal website http://cv64.us was stellar and awesome, I had several real world examples of everything I knew, both personal and technical.  The site had several web pages, with content that covered almost every subject.

The year 2021, my subscription with a web service provider was consistently increasing every year, the inflated prices continued to increase, so I decided to cancel after over 2 decades.  My new service provider is now providing the same support from $160 per year to $36.  I could simply move my site from previous to new, however, utilizing newer technologies, I decided, let’s start fresh with everything.

So let’s start over again, with new technology :-)

Two years ago, I have created my own git repository.  I have moved all my technical work to that site which provides configuration management capabilities.  Currently my Classes are sent this link so that they can download any of my repositories.  I learned about GIT about that same time and as always, if I wanted to learn a technology, then utilizing the technology.

When I started on a couple subjects, I started creating videos.  These videos were my experiences that I wanted to share with all, again going back my my statement, “If you want to understand a subject, then teach”. so again, I learned a topic and again, teaching what I have learned.

To make this succinct, here’s the bottom line.  I continuously upload to my website, git and youtube.com   I share my experiences, will continue and provide additional artifacts.  Today this is easier today, than with previous technology.  My finest work is yet to be published, my Genealogy application.  The application is called “Estey Genealogy”, contains a login screen, with all kinds of side screen with general information, login, search, history, hierarchy and a lot more.  The versions are to me the best.  The versions are named after family members and there birth dates of the release, e.g.  first version, Mom, Judy, Mike, Jeanne, Dad, Steve, Bobby  very neat to do something while at the same time learn a technology.

BOTTOM LINE:  You Learn better by Teaching
