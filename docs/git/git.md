# Git

** VERY IMPORTANT - Never use spaces and always use lowercase, e.g. ceis320, bobby.estey, NOT:  CEIS320, First.Last**

- **[Git](https://en.wikipedia.org/wiki/Git) is Configuration Management Software.**

- **Git is NOT Github, Gitlab or any other Git Repository Provider.**

## Git Installation

- Navigate to the Git software download site:  [git download](https://git-scm.com/downloads)
- Press the terminal and the software will start loading on your computer
- Once the software has completed the download, press the executive, e.g. exe file and the installation will start
- Press Next, accept all the defaults and finally Install.  The executive will start installing git
- At the end, **UNCHECK** the read documentation

## GitLab Repository Registration

- This is how to register with the Gitlab Repository
    - Navigate to the [gitlab.com](https://gitlab.com) website
    - Press Get free trial
    - Select Continue with SaaS
    - Fill out the Form
    - Receive email and Click Confirm your account, you will be navigated to the Gitlab website.
    - Log into Gitlab
    - Questionaire will be displayed, fill out form and press Continue
    - Create your group, enter a group name
        - A good group name would be your first . last name, e.g.  bobby.estey
        - Make sure that the group name is the same as the group URL, e.g.  
        - Press Create Group
        
![GroupName](groupName.png)

    - Create / import your first project
        - A good Project name would be course name, e.g. ceis320
        - Make sure that the project name is the same as the project URL, e.g.  
        - Press Create project
        
![ProjectName](projectName.png)
        
## Starting with GitLab

- Navigate to the upper left corner, select -> Menu -> Projects -> Your Projects

![GitLab Projects](yourProjects.png)
- Select your new Project, e.g. groupname/projectname - bobby.estey/ceis320
- ignore security warning, to advanced for beginners
- Press Clone
- Copy / Press the icon Copy with Clone with HTTPS, screenshot has:  https://gitlab.com/cv64/ceis320.git

![gitClone](clone.png)

## Git Configuration
- Open up the command prompt, Windows:  cmd
- Navigate to the C Drive:  cd c:\
- Make a new directory and call the directory git:  mkdir git
- Navigate to the git directory:  cd git
- Make another directory and call the directory cv64:  mkdir cv64
- Make another directory and call the directory your project name that you created on Gitlab, e.g. ceis320
- Navigate to the project name directory:  cd ceis320
- Clone the git repository to your local machine.  VERY IMPORTANT, PASTE the Clone URL that you copied earlier:  git clone https://gitlab.com/cv64/ceis320.git
- **WATCH THE SPARKS FLY**

## Git Commands
- git status : get the status of files, e.g. added, updated, deleted, etc.
- git add : stage files to be added to the repository
- git commit -m "comment" : record changes to the repository
- git reset : undo last commit
- git push : push files to the repository from the local client
- git pull : pull files from the repository to the local client

## Git Configurations
- The first time setting up the Repository other configurations are required and updates the .gitconfig file
     - git config --global user.email "yourEmailAddress", e.g. "bobby.estey@gmail.com"
     - git config --global user.name  "yourName",         e.g. "Bobby Estey"
     
     - .gitconfig
          [user]
	            email = bobby.estey@cv64.us
	            name = Bobby Estey
     
- Setting up your password enter the following, updates the .git-credentials file and then push to the Repository
     - git config credential.helper store
          - https://username:password@repository
          - https://bobby.estey%40gmail.com:password@gitlab.com
     
