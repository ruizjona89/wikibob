# ![](../../docs/icons/cv64us50x50.png) c++ / cpp - C Plus Plus

## pointers - [hobgoblin of C/C++](https://www.oreilly.com/library/view/object-oriented-programming-in/0672323087/ch10.html)
## installation

### cygwin installation (gcc toolchain and make)
 - navigate to [https://www.cygwin.com/](https://www.cygwin.com/)
 - locate setup-x86_64.exe, press the Link.  This will start up the cygwin installation UI
 - the UI is rendered, open up the All widget
 - select Devel -> gcc-g++ -> change Skip to the latest version
 - select Devel -> make -> change Skip to the latest version
 - windows type env - brings up the environments UI
 - edit System variables -> Path
 - press New and enter C:\cygwin64\bin
 - press OK, Press OK, Press OK

This video illustrates how to setup up C++ in the eclipse IDE:
 - [eclipse installation](https://www.youtube.com/watch?v=O0oExvj5CE4)
 - install eclipse
 - start up eclipse
 - select Help -> Select Install New Software
 - select --All Available Sites--
 - select Programming Languages
 - select C/C++ Autotools support
 - select C/C++ Development Tools
 - select C/C++ Development Tools SDK
 - a list of what was selected will display, press Next >
 - press Finish and software will be installed into eclipse
 - restart eclipse dialog appears, press Restart Now, eclipse restarts
 - go to the Workbench
 - select File -> New -> Other -> C/C++ -> C++ Project -> C++ Managed Build
 - fill out Project fields - project name, suggest:  cpp and Hello World
 - make sure Toolchains is Linux GCC or Cygwin GCC
 - press Next > and Finish
 - select Open Perspective for C/C++
 - C/C++ Perspective appears
 - select the project -> New -> Class and press Finish
 - select the project, e.g. cpp -> Project -> Build All or Ctrl-b, rebuilds the project
 - right click the project, e.g. cpp -> Run As -> Local C/C++ Application
 - the Console displays the program execution
 
These images show the files configured for this installation
 - [installation 1 image](https://gitlab.com/bobby.estey/cpp/-/blob/master/resources/eclipseCPPInstall1.png)
 - [installation 2 image](https://gitlab.com/bobby.estey/cpp/-/blob/master/resources/eclipseCPPInstall2.png)