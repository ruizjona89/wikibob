package fundamentals.interfaces;

import java.util.Scanner;

public class ConverterApplication {

	/**
	 * Once you have your classes written, create a class called
	 * ```ConverterApplication``` which has your `main` method. In that main method,
	 * instantiate and use both the `ConverterIf` and `ConverterSwitch` classes.
	 * 
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		System.out.println("Please enter a number between 1 and twelves including both extremes");
		int monthNumber = Integer.parseInt(keyboard.nextLine());

		ConverterIf converterIf = new ConverterIf();
		converterIf.convertMonth(monthNumber);
		ConverterSwitch converterSwitch = new ConverterSwitch();

		keyboard.close();
	}
}
