# Redux - Managing State across the Application

# Video

- [React Redux TypeScript Tutorial - Introduction for Beginners](https://www.youtube.com/watch?v=8DH7Ekp2PWI)
     - 17:07 - interesting, I usually have the following syntax:
       ```<Button color="red">```
     - WebStorm always places {} and on this video the format looks different:
     ```<Button color={'red'}>``` 
     - QUESTION:  Should this be the new syntax we should be using?

# Need to Heed
- [SAID SO PERFECTLY](https://youtu.be/8DH7Ekp2PWI?t=3743)

- This page was an attempt to share what I learned about REDUX - again the industry continues the practice of ABSTRACTION / CONVOLUSION and that's where we are headed.  Java, the Greatest Programming Language on the planet and truly the simplest, most sensible and easy language to program.  Why would anyone want to create CONVOLUTED pollution.  None of this is new, all the same technologies from the past, just a dumber approach.  Even the author of the recording, said so perfectly.

- BOTTOM LINE:  I learned so much from this authors code, than anything else.

# Resources

- [JSONPlaceholder](https://jsonplaceholder.typicode.com)
- ES7 React/Redux/GraphQL/React-Native snippets

# ![Redux Data Flow](reduxDataFlow.drawio.png)

- State - Object, primitive, etc. with a value
- View - React Components that can send an action through an interaction, e.g. submit, click, etc.
- Store - Contains the State and sends to the Component
- Action Creators - Dispatch and Action to the Store
- Reducers - Functions that change the State in accordance with the received action from the Store.
     - Note: State is immutable so a new State is created, not modified, (in other words - not changing but creating a new state)
- Slice - Component
- Capability to log every user action and playback

# File Structure

- src
     - index.tsx
     - App.tsx - main start application
          - components
               - store - Contains the State and sends to the Component
               - rootStore.ts - creates the store
                    - ```<application>```
                    - models
