<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Database Connectivity</title>
  <meta http-equiv="content-type"
 content="text/html; charset=ISO-8859-1">
</head>
<body>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><big><big><b>Database
Connectivity<br>
</b></big></big></div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
This section will address database connectivity. &nbsp;How do we access
data from different types and sources? &nbsp;Data can be anything from
a&nbsp; database, imaging files, to email. &nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">There are also
several different
standards for making links to a database,&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchvb.techtarget.com/sDefinition/0,,sid8_gci214133,00.html">Open
Database Connectivity (ODBC)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">, </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci213404,00.html">eXtensible
Markup Language (XML)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">, etc</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">. &nbsp;Having a vast
mix of files
and types makes database connectivity configuration challenging,
however, after having experience with more than one connection, you
will observer that all databases have the same two requirements, &nbsp;<b>Database
Drivers and Datasources</b>.<br>
</font>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li><b>Database Drivers</b>
- A database driver is software usually provided by the database vendor
that enables application programs to
make connections, disconnects and perform transactions on a database.
&nbsp;<a href="#table1">Table 1</a> illustrates examples of database
driver classes.</li>
  <li><b>Datasource</b> - A
datasource is the actual location of the database, usually mapped
with a <a
 href="file:///C:/estey/bob/secure/classes/uop/dbm405/courseMaterials/week3/week3.html#uri">Uniform
Resource Location</a>. &nbsp;The datasource is usually configured by
the client. &nbsp;<a href="#table1">Table 1</a> illustrates the
datasource syntax and actual examples.<br>
  </li>
</ul>
<table style="font-family: helvetica,arial,sans-serif;" align="center"
 border="1" cellpadding="2" cellspacing="2" width="300">
  <tbody>
    <tr>
      <td valign="top">Database<br>
      </td>
      <td valign="top">Database<br>
Driver<br>
      </td>
      <td valign="top">Datasource<br>
Syntax<br>
      </td>
      <td valign="top">Datasource<br>
Example<br>
      </td>
    </tr>
    <tr>
      <td valign="top">MySQL<br>
      </td>
      <td valign="top">com.mysql.jdbc.Driver<br>
      </td>
      <td valign="top">jdbc:mysql://ipaddress:port/databaseName<br>
      </td>
      <td valign="top">jdbc:mysql://127.0.0.1:3306/cv64us<br>
      </td>
    </tr>
    <tr>
      <td valign="top">MS
Access<br>
      </td>
      <td valign="top">sun.jdbc.odbc.JdbcOdbcDriver<br>
      </td>
      <td valign="top">jdbc:odbc:databaseName<br>
      </td>
      <td valign="top">jdbc:odbc:MSAccess<br>
      </td>
    </tr>
  </tbody>
</table>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><b><a
 name="table1"></a>Table 1 - Database Drivers and Datasource Example</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b><br>
Open DataBase Connectivity (ODBC)</b><br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Open DataBase
Connectivity (ODBC) is an open standard Application Programming
Interface (API) for accessing several databases, e.g. DB2, MySQL,
Oracle, Sybase, Informix, etc. &nbsp;ODBC allows an application program
to access a database using the <a
 href="http://searchdatabase.techtarget.com/sDefinition/0,,sid13_gci214230,00.html">Structured
Query Language (SQL)</a>. &nbsp;Object Link Embedding Database (OLE DB)</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> includes SQL (like
ODBC) and an
Object-Oriented (OO) interface. &nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwin2000.techtarget.com/sDefinition/0,,sid1_gci213761,00.html">ActiveX
Data Objects (ADO)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">
lets a programmer writing applications get access to a relational or an
OO database. &nbsp; Using ADO a client can access a database from the
web. &nbsp;ADO is a wrapper package over OLE DB. &nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://java.sun.com/products/jdbc/">Java Database Connectivity
(JDBC)</a></font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> is also
another product that executes the ODBC protocol.<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<b>Request Sequence</b><br>
<br>
The Database Connectivity tools all follow these basic steps, also
known as the request sequence:<br>
</font>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li>Driver Loaded - loads
the database driver into the servers memory so that applications can
communicate with the <a
 href="http://searchdatabase.techtarget.com/sDefinition/0,,sid13_gci213669,00.html">Database
Management System (DBMS)</a>. &nbsp;The driver processes ODBC / SQL
requests for a specific database. &nbsp;</li>
  <li> Data Source Connection
- locates the actual database for the Server to perform operations. </li>
  <li>Client Request - the
Client makes a request of services to the Server.</li>
  <li>Server Request - the
Server using the Driver executes commands to the DBMS which performs
the requested transaction.</li>
  <li> Execute Request&nbsp;-
the DBMS executes the command and returns results if appropriate.<br>
  </li>
  <li>Server Response - the
application program (caller) gets the results from the database.<br>
  </li>
  <li> Data Source
Disconnected - disconnects the link to the database.</li>
</ul>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a href="#figure1">Figure
1</a>
illustrates each of the steps above making
an ODBC connection between an</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> application and a
database.
&nbsp;This
can be either a standalone or a web application.<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><img
 src="odbcProcess.gif" alt="" title=""
 style="width: 379px; height: 321px;"> <br>
<a name="figure1"></a><b>Figure 1 - ODBC Process</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> <b>Conformance
Levels</b><br>
<br>
Database connectivity tools are in compliance with ODBC and /
or SQL conformance levels. &nbsp;These inform the application program
of the capabilities of the database tools. &nbsp;ODBC has three levels,
Core, Level 1 and Level 2 APIs. &nbsp;SQL has three levels, Minimum,
Core
and Extended grammar. &nbsp;Each of these levels vary from general to
very specific operations.<br>
</font>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li>ODBC Conformance Levels<br>
  </li>
</ul>
<ul style="font-family: helvetica,arial,sans-serif;">
  <ul>
    <li>
      <p><b>Core</b> - a set
of core API functions that corresponds to the functions in the <a
 href="http://www-1.ibm.com/servers/eserver/iseries/whpapr/sql_cli.html">ISO
Call Level Interface (CLI)</a> and X/Open CLI specification. </p>
    </li>
    <li>
      <p><b>Level 1</b> -
includes all Core API functions as well as several extended functions
usually available in an <a
 href="http://search390.techtarget.com/sDefinition/0,,sid10_gci214138,00.html">Online
Transaction Processing (OLTP)</a> relational DBMS.</p>
    </li>
    <li>
      <p><b>Level 2</b> -
includes all Core and Level 1 API functions as well as additional
extended functions.</p>
    </li>
  </ul>
  <li>
    <p>SQL Conformance Levels<br>
    </p>
  </li>
</ul>
<ul style="font-family: helvetica,arial,sans-serif;">
  <ul>
    <li>
      <p><b>Minimum</b> - a
set of SQL statements and data types that meets a basic level of ODBC
conformance.</p>
    </li>
    <li>
      <p><b>Core</b> -
includes all Minimum SQL grammar as well as additional statements and
data types that roughly correspond to the X/Open and&nbsp;SQL Access
Group (SAG) CAE
specification (1992).</p>
    </li>
    <li>
      <p><b>Extended</b> -
includes all Minimum and Core SQL grammar as well as an extended set of
statements and data types that support common DBMS extensions to SQL.</p>
    </li>
  </ul>
</ul>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Data Source Name
(DSN)
</b><br>
</font>
<p style="font-family: helvetica,arial,sans-serif;"> A&nbsp;<a
 href="http://searchvb.techtarget.com/sDefinition/0,,sid8_gci874018,00.html">Data
Source Name (DSN)</a>
is an ODBC data structure that contains the information that a driver
needs to create connections and process requests. &nbsp;The DSN
contains the database name, directory, driver of the database, username
and password. &nbsp;A user DSN enables a client (usually the person who
created the link) to access a database. &nbsp;A system DSN allows any
client access to the database. &nbsp;A file DSN allows only a single
client access to a database. &nbsp;See <a href="#sdsn">Appendix A</a>
how
to set up DSN. </p>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> </font>
<ul style="font-family: helvetica,arial,sans-serif;">
</ul>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><big><big><b>Databases
and Internet Technologies<br>
</b></big></big></div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
This section we will look at how to access databases through the
Internet. &nbsp;There are several technologies today that
allow clients to access databases securely. &nbsp;This discussion will
give an example of how you can set up your personal system and start
serving a database.<br>
<br>
<b>History</b><br>
<br>
Today most people in developed countries have access the </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212370,00.html">Internet</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">, not so long ago
this would of
been unimaginable. &nbsp;The Internet was conceived due to <a
 href="http://whatis.techtarget.com/definitionsCategory/0,289915,sid9_tax1668,00.html">Networking</a>.
&nbsp;Networking enables standalone computer systems to share a common
interface&nbsp; in whichthey can communicate. &nbsp;The first networks
were cable based using Ethernet technology and private, meaning only
the computers that were connected to the same wire had access to the
other computer resources. &nbsp;Today's networks are fiber optic,
wireless or satellite and publicly accessible. &nbsp;The Internet is
the world's network. &nbsp;Computer systems all over the globe can tie
into the network infrastructure by any type of media today. &nbsp;The
Internet is a publicly accessible site that uses
the <a
 href="http://searchnetworking.techtarget.com/sDefinition/0,,sid7_gci214173,00.html">Transmission
Control Program / Internet Protocol (TCP/IP)</a>.<br>
<br>
<b>Internet</b><br>
<br>
In the 1960's the Advanced Research Projects Agency (ARPA) created
ARPANET connecting military, educational and research sites to a
network. &nbsp;In 1984, the U.S. Military was split off to MILNET and
ARPANET was now called Internet. &nbsp;Most of the commands for the
Internet were Unix like and are still used today, e.g. <a
 href="http://searchnetworking.techtarget.com/sDefinition/0,,sid7_gci213976,00.html">File
Transfer Protocol (FTP)</a>, <a
 href="http://searchnetworking.techtarget.com/sDefinition/0,,sid7_gci213116,00.html">Telnet</a>,
News, Mail.<br>
<br>
<b>World Wide Web (W3)</b><br>
<br>
The Internet was utilized with commands and procedures that only a few
understood. &nbsp;After a lot of practice, the novice would later
become a web surfer. &nbsp;Sharing information with others was a
painful tasks and standards were not fully defined, thus the <a
 href="http://w3c.org">World Wide Web Consortium</a> also known as
"W3C", started defining standards for web based applications. &nbsp;In
1989, Tim Berners-Lee of the European Particle Physics Lab (<a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci211767,00.html">CERN</a>)
began research which would later to be known as <a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci214004,00.html">HyperText
Transfer Protocol (HTTP)</a>, enabling accessibility to other Internet
systems using the TCP/IP protocol. &nbsp;Later HTTP would be
standardized with codes that would identify, format, display and
provide links to other documents. &nbsp;The format of HTTP codes would
later be called <a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212286,00.html">HyperText
Markup Language (HTML)</a> a subset of the <a
 href="http://whatis.techtarget.com/definition/0,,sid9_gci214201,00.html">Standard
Generalized Markup Language (SGML)</a>. &nbsp;If you wish to learn HTML
and XHTML you can view the additional lectures provided in the course
materials. &nbsp;The following site has excellent information on the
various Internet technologies: &nbsp;<a href="http://www.w3schools.com">www.w3schools.com</a><br>
<br>
<b>Browsers</b><br>
<br>
In 1993, the <a
 href="http://searchsecurity.techtarget.com/sDefinition/0,,sid14_gci212626,00.html">National
Center for Supercomputing Applications (NCSA)</a> developed <a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212593,00.html">Mosaic</a>
which was an application that read HTML documents, displayed graphics
and was the first of a series of applications called <a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci211708,00.html">Browsers</a>.
&nbsp;Mosaic was the predecessor to the Netscape Navigator browser. <br>
<b><br>
Web Server</b> <br>
<br>
A web server is a computer system that receives a user request through
a message, processes the request and sends back a response message that
the client browser interprets and displays on the client system.
&nbsp;The <a href="http://apache.org">Apache Web Server</a> is one of
the pioneers and <a
 href="http://whatis.techtarget.com/definition/0,,sid9_gci211576,00.html">Most
popular web server on the market today</a>.<br>
<br>
<b>Internet Standards</b><br>
<br>
Previously discussed, W3C became the standards body and wrote most of
the Internet requirements that are in use today. &nbsp;HTML, Dynamic
HTML (DHTML) and eXtensible Markup Language (XML) are the commonly
known standards for the web today. &nbsp;The&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci214160,00.html">Uniform
Resource Identifier (URI)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> standard which
encapsulates
the&nbsp;</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci214160,00.html">Uniform
Resource Locator (URL)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> and&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci214164,00.html">Uniform
Resource Name (URN)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">
are how the web addresses information. &nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">URL specifically
defines a web
address for a web page whereas a URN </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">provides the address
of XML
definitions. &nbsp;Then there is the&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212576,00.html">Multipurpose
Internet Mail Extensions (MIME)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> types, which define
the type of
file, e.g. document, image, video, etc. &nbsp;MIME types are loaded
into a browser to inform the browser how to execute that particular
protocol, e.g. a file that ends with the extension jpg will start up a
jpg viewer program.</font>
<p style="font-family: helvetica,arial,sans-serif;"> </p>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Intranets /
Extranets</b><br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212377,00.html">Intranets</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">&nbsp; are private
networks
usually behind
a firewall that limit the accessibility to computing resources to the
public
Internet. &nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchsecurity.techtarget.com/sDefinition/0,,sid14_gci212089,00.html">Extranets</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> are private networks
that share
information or operations with other private networks. &nbsp;Extranets
are usually
thought of as Business to Business communications between suppliers,
customers, etc.<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> <b><br>
N-Tiered Architecture</b><br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Client / Server
Architectures are officially declared as&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchnetworking.techtarget.com/sDefinition/0,,sid7_gci214381,00.html">N-Tiered</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> by the number of
distribution
levels of computing processes, not necessarily the number of nodes.
&nbsp;Most N-Tiered architectures are three or more, the <a
 href="#j2ee">Java 2 Enterprise Edition (J2EE)</a> uses the Four-Tiered
Architecture as shown in <a href="#figure2">Figure 2</a>. &nbsp;I
will
first go into detail on the J2EE Architecture (Four-Tiered) describing
the different tiers then discuss a Three-Tiered example later.<br>
<br>
The Client / 1st tier uses software usually a Browser that communicates
and sends requests to a Web Server. &nbsp;The Web Server / 2nd tier
reacts to requests from client systems and usually is Server Pages
software,
e.g. </font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://jakarta.apache.org/tomcat/">Java Server Pages</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">, </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwin2000.techtarget.com/sDefinition/0,,sid1_gci213787,00.html">Active
Server Pages</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">.
&nbsp;The Server Pages software gives a website the capability of
providing both static and dynamic web pages. &nbsp;The Web Server can
forward the request to the&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212571,00.html">Middleware</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> software or handle
the
transaction and
send the response back to the client. &nbsp;Middleware / 3rd tier is
the
business logic and services for the enterprise, </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">J2EE uses&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a
 href="http://java.sun.com/products/ejb/">Enterprise Java Beans (EJB)</a></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> as middleware.
&nbsp;Middleware
has the option to do several types of tasks, &nbsp;do all the work and
respond back to the Web Server or perform execution on the Database /
4th tier. &nbsp;The Database / 4th tier is strictly the database that
receives requests and responds to the Middleware software.<br>
<br>
A three tiered example would be a non-GUI application on a client
system that connects directly to the middleware on a server system.
&nbsp;There is no web interface. &nbsp;To recap, the client tier asks
for services, the web tier displays services, the middleware is the
actual brains to the entire operation (management, resource pooling,
security, etc.) and the database is where persistent data is stored.
&nbsp;</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> Look at <a
 href="file:///C:/estey/bob/secure/classes/uop/dbm405/courseMaterials/week3/week3.html#j2ee">Appendix
B</a> for instructions on how to implement a N-Tiered Architecture on
your system.<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center">
<div style="text-align: left;">
<div style="text-align: center;"><img src="j2eeArchitecture.gif" alt=""
 title="" style="width: 417px; height: 321px;"><br>
<b><a name="figure2"></a>Figure 2 - J2EE N-Tiered Architecture</b><br>
<ul style="text-align: left;">
  <big> </big><li><big><font size="2"><big>Client: which is
responsible for the presentation
logic</big></font> </big></li>
  <big> </big><li><big><font size="2"><big>Application Server (could
be a web server): Shares
the duty of application logic</big></font></big></li>
  <big> </big><li><big><font size="2"><big>Application Server:
responsible for the
application logic (business rules)</big></font> </big></li>
  <big> </big><li><big><font size="2"><big>Database server:
responsible for the data access
logic and storage</big></font></big></li>
</ul>
<big></big>
<ul style="text-align: left;">
</ul>
<div style="text-align: left;"><b>Internet
Applications</b><br>
</div>
</div>
</div>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<a
 href="http://searchvb.techtarget.com/sDefinition/0,,sid8_gci212948,00.html">
Scripting Languages</a> are interpreted, limited in capability and
usually used on the client side for processing simple functions.
&nbsp;Scripting languages are used for form validation, stateless calls
and simple calculations. &nbsp;Most common scripting languages are
Javascript, JScript and Visual Basic Script.<br>
<a
 href="http://searchwin2000.techtarget.com/sDefinition/0,,sid1_gci211824,00.html"><br>
Programming Languages</a> are compiled (the code is already in machine
code and performance is much faster) and unlimited in capability.
&nbsp;Programming languages are either Object-Oriented (C++, Java) or
Structured (C). &nbsp;Programming languages can be used both on the
client (Java Applet) or server (Java
Server Pages, CGI) and are harder to implement than a scripting
language.<br>
<br>
<a
 href="http://searchwebservices.techtarget.com/sDefinition/0,,sid26_gci212527,00.html">
Markup languages</a> are a sequence of characters that are interpreted.
&nbsp;HyperText Markup Language (HTML) is a markup language that has
elements (tags) that inform the Browser how to display the web page.
&nbsp;Simple Object Access Protocol (SOAP) based on the eXtensible
Markup Language (XML) is a markup language that informs an application
program how to access
and process distributed objects.</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<br>
<b>Server Pages</b><br>
<br>
HTTP is stateless, meaning that the client will make a request to
a server which responds to the request and the transaction is ended.
&nbsp;Here's an example: &nbsp;Client: &nbsp;What is the time?
&nbsp;Server: 08:33. &nbsp;The server just answers the question and has
no obligation to remember which client asked the question.<br>
<br>
Stateful is the opposite of stateless. &nbsp;A stateful transaction
will keep track on which client asked for what services and remembers
the state of the client. &nbsp;An excellent example of a stateful
transaction would be a shopping cart program. &nbsp;The client will go
to the website, look through a catalog, select the items to purchase
and then checkout. &nbsp;The server is maintaining the client's state
at all times. &nbsp;Shopping cart software gives each client a session
identification. &nbsp;The server maintains the state of the client with
that particular identification.
&nbsp;When a client purchase 10 items of product A and 20 items of
product
B, the server is keeping count on which session identification is
making
what purchases. &nbsp;When the client finally performs the checkout,
the
server sends back a confirmation on items requested and then the server
places the order.<br>
<br>
Server Pages are both stateless and stateful and enable a website to be
dynamic. &nbsp;The two most common Server Pages on the market today
are Java Server Pages (JSP) using the Java Programming Language and
Active Server Pages (ASP) using Visual Basic.<br>
<br>
</font> <font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b> Summary</b><br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> This lecture
discussed briefly the fundamentals of Database Connectivity and </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">how to make a
database connection
through an application program. &nbsp;All databases come with two
fundamental requirements to make connectivity. &nbsp;1) &nbsp;The
Database
Driver is the software that allows applications to communicate with the
database. &nbsp;2) &nbsp;The Datasource is the actual location of the
database, where the server can find the database. &nbsp; We also
talked about the history of the Internet, the World Wide Web, N-Tiered
Architectures
and Internet Applications.<br>
</font>
<hr style="font-family: helvetica,arial,sans-serif;" size="2"
 width="100%">
<p style="font-family: helvetica,arial,sans-serif;"><b> Discussion
Questions:</b>
&nbsp;Please read
the Discussion Question appendix in the syllabus.<br>
</p>
<p style="font-family: helvetica,arial,sans-serif;"> Discussion
Question 1:
&nbsp;Discuss the history and how ODBC works and the various
conformance levels. &nbsp;Mention some of the several technologies that
are using ODBC.<br>
</p>
<p style="font-family: helvetica,arial,sans-serif;">Discussion Question
2:
&nbsp;Discuss the n-tiered architectures and how they interface with
databases, JDBC, ODBC, etc.<br>
</p>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Discussion Question
3: &nbsp;Discuss the role of web servers and any new Internet Database
technologies.</font>
<hr style="font-family: helvetica,arial,sans-serif;" size="2"
 width="100%"><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Individual
Assignment</b><br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Write a 2 - 3 page
paper (at least 300 words per page, </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><span style="font-weight: bold;">Arial
Font, Size 10</span></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">)
that explains in detail the topic of discussion. &nbsp;Make sure your
resources are declared.</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<br>
Discuss any future web technologies that will enhance database
activity, e.g. SOAP, UDDI, WSDL<br>
</font>
<hr style="font-family: helvetica,arial,sans-serif;" size="2"
 width="100%">
<p style="font-family: helvetica,arial,sans-serif;"><big>References</big><br>
</p>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a name="text"></a>David
M.
Kroenke,
Database Processing: &nbsp;Fundamentals, Design &amp; Implementation,
Seventh Edition, 2000<br>
<a name="ado"></a><a href="http://whatis.com">Whatis.com</a>,
http://whatis.com, Accessed: &nbsp;17 May 2003<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
</font>
<hr style="font-family: helvetica,arial,sans-serif;" size="2"
 width="100%"><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b><a name="sdsn"></a>Appendix
A - </b><b><small><small><font size="+2"><small><small>System
Data Source Name (DSN) Setup</small></small></font></small></small></b></font>
<p style="font-family: helvetica,arial,sans-serif;">The following is an
example of
how to set up the System Data Source Name (DSN). &nbsp;Once DSN is
configured application software can connect and perform transactions on
the database, e.g. Java. </p>
<p style="font-family: helvetica,arial,sans-serif;">The first step is
to go to
the Administrator Tools screen as illustrated below: </p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="adminTools.jpg" style="height: 325px; width: 239px;" title=""
 alt=""> </p>
<p style="font-family: helvetica,arial,sans-serif;">Double click on
Data
Sources (ODBC) and click on the System DSN tab and the following screen
will appear. </p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="odbcSystem1.jpg" alt="" title=""
 style="width: 461px; height: 377px;"> <br>
</p>
<p style="font-family: helvetica,arial,sans-serif;">Click <u>A</u>dd
and the
following screen will appear. </p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="newDataSource.jpg" style="height: 345px; width: 468px;" title=""
 alt=""> </p>
<p style="font-family: helvetica,arial,sans-serif;">Select Microsoft
Access
Driver (*.mdb) and press Finish, the following screen will appear.
</p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="accessSetup.jpg" style="height: 312px; width: 471px;" title=""
 alt=""> </p>
<p style="font-family: helvetica,arial,sans-serif;">Click <u>S</u>elect
to
find your
database. </p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="selectDB.jpg" style="height: 243px; width: 402px;" title="" alt="">
</p>
<p style="font-family: helvetica,arial,sans-serif;">Click ok and the
Setup
screen will reappear with your settings.&nbsp; If there are passwords
or other configurations you will have to click on <u>A</u>dvanced...
</p>
<p style="font-family: helvetica,arial,sans-serif;"><img src="setup.jpg"
 style="height: 312px; width: 471px;" title="" alt=""> <br>
</p>
<p style="font-family: helvetica,arial,sans-serif;">Enter the Data
Source Name
and press OK, the following screen will appear.<br>
</p>
<p style="font-family: helvetica,arial,sans-serif;"><img
 src="odbcSystem2.jpg" alt="" title=""
 style="width: 461px; height: 377px;"> </p>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">The System DSN is
now configured.</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
</font>
<hr style="font-family: helvetica,arial,sans-serif;" size="2"
 width="100%"><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b><a name="j2ee"></a>Java
2 Enterprise Edition (J2EE) Database
Configuration
and Example</b><br>
<br>
The following are instructions on how to make a connection to a
database using the Java 2 Enterprise Edition product.<br>
</font>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li>Download and install
the latest version of <a href="http://java.sun.com/j2se/">Java 2
Standard Edition</a> and <a href="http://java.sun.com/j2ee/">Java 2
Enterprise
Edition</a>, remember these are two separate programs.</li>
  <li>Define the following
environment variables:</li>
  <ul>
    <li><b>JAVA_HOME</b>,
where you placed the Standard Edition, e.g. <b>set
JAVA_HOME=c:\j2sdk1.4.1_01</b></li>
    <li><b>J2EE_HOME</b>,
where you place the Enterprise Edition, e.g. <b>set
J2EE_HOME=c:\j2sdkee1.3.1</b></li>
    <li><b>PATH</b>,
containing both environment variable bin directories, e.g. <b>set
%PATH%;%JAVA_HOME%\bin;%J2EE_HOME%\bin</b></li>
  </ul>
  <li>Add the Database
Driver and Datasource to J2EE by typing the following commands at the
command prompt:</li>
  <ul>
    <li><b>j2eeadmin
-addJdbcDriver sun.jdbc.odbc.JdbcOdbcDriver</b></li>
    <li><b>j2eeadmin
-addJdbcDatasource jdbc/MSAccess jdbc:odbc:MSAccess</b></li>
  </ul>
  <li>Copy the Enterprise
Archive (EAR) file to the directory you wish to deploy from, e.g. <b>%J2EE_HOME%\products\cv64\ee</b></li>
  <li>Start the J2EE engine:
&nbsp;<b>j2ee -verbose</b></li>
  <li>Start the deployment
tool: &nbsp;<b>deploytool</b></li>
  <li>Open the EAR file from
the deploytool and deploy the application.</li>
  <li>Open up the application
from a Browser and start interaction with the database: &nbsp;<b>http://localhost:8000/contextPath</b></li>
</ul>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Send me an email if
you wish
an example EAR file and database to deploy.</font><span
 style="font-family: helvetica,arial,sans-serif;"><br>
</span>
<hr style="width: 100%; height: 2px;"><span
 style="font-family: helvetica,arial,sans-serif;"></span><img
 src="cv64.bmp" alt="" style="width: 32px; height: 32px;" align="left"><small><font
 face="Arial,Helvetica"><a href="mailto:information@cv64.us">Web
Contact</a></font></small><br>
<small><font face="Arial,Helvetica">
Last modified:&nbsp; 2003 December 31</font></small><br
 style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
</body>
</html>
