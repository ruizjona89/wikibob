// https://www.youtube.com/watch?v=wDVH3qnXv74
// https://www.youtube.com/watch?v=iiADhChRriM
// JavaScript Object Notation (JSON)
package fundamentals.io;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class CreateJSONFile {

	@SuppressWarnings("unused")
	private static final String INPUT_FILE = "files/input.json";
	private static final String OUTPUT_FILE = "files/output.json";

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("firstName", "Bobby");
		jsonObject.put("lastName", "Estey");

		JSONArray jsonArray = new JSONArray();
		jsonArray.add("Java");
		jsonArray.add("Linux");
		jsonArray.add("Angular");
		jsonObject.put("technologies", jsonArray);

		try (FileWriter fileWriter = new FileWriter(OUTPUT_FILE)) {

			fileWriter.write(jsonObject.toString());

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

		System.out.println(jsonObject);
	}
}