package graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class DrawTriangles extends JFrame implements MouseListener {

	private static final long serialVersionUID = 1L;
	private int x = 50; // leftmost pixel in circle has this x-coordinate
	private int y = 50; // topmost pixel in circle has this y-coordinate

	public DrawTriangles() {
		setSize(800, 800);
		setLocation(100, 100);
		addMouseListener(this);
		setVisible(true);
	}

	// paint is called automatically when program begins, when window is
	// refreshed and when repaint() is invoked
	public void paint(Graphics g) {
		g.setColor(Color.ORANGE);
		g.fillRect(x, y, 100, 100);

		int xpoints[] = { 225, 145, 225, 145, 225 };
		int ypoints[] = { 225, 225, 145, 145, 225 };
		int npoints = 5;

		g.fillPolygon(xpoints, ypoints, npoints);

		g.setColor(Color.RED);
		int apoints[] = { 300, 400, 100, 500, 700, 200 };
		int bpoints[] = { 100, 150, 100, 200, 700, 500 };
		int cpoints = apoints.length;
		g.drawPolygon(apoints, bpoints, cpoints);

		// g.fillOval(x,y,100,100);
	}

	// The next 4 methods must be defined, but you won't use them.
	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
		x = e.getX(); // x-coordinate of the mouse click
		y = e.getY(); // y-coordinate of the mouse click
		repaint(); // calls paint()
	}
}
