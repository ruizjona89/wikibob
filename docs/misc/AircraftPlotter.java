package misc;

import java.util.Scanner;

/** This procedure emulates a pilots computer */
public class AircraftPlotter {

	/** Initiate Scanner */
	Scanner scanner = new Scanner(System.in);

	double radians = 0.0174533;
	double degrees = 57.29578;
	int circle = 360;
	int variance = 0;
	int zero = 0;

	double gl = 0;
	double wradians = 0;
	double wf = 0;
	double ratioSin = 0;
	double headingroundSpeed = 0;
	double headingCos = 0;
	double headingTan = 0;
	double headingRadians = 0;

	int groundSpeed = 0;
	int trueCourse = 0;
	int windVelocity = 0;
	int windDirection = 0;
	int trueAirSpeed = 0;
	int relativeWindDirection = 0;
	int azimuth = 0;
	int windCourseAdjustment = 0;
	int magneticCourse = 0;
	int magneticHeading = 0;

	/** Aircraft Plotter Constructor */
	public AircraftPlotter() {
		getInput();
		caculate();
		printResults();
	}

	/** Receive inputs from user. */
	private void getInput() {

		System.out.println("True Course: ");
		trueCourse = scanner.nextInt();
		System.out.println("Wind Velocity: ");
		windVelocity = scanner.nextInt();
		System.out.println("Wind Direction: ");
		windDirection = scanner.nextInt();
		System.out.println("True Airspeed: ");
		trueAirSpeed = scanner.nextInt();
		System.out.println("Variance: ");
		variance = scanner.nextInt();
	}

	/**
	 * Calculation of angles & velocity Law of sines: sin(a)/A = sin(b)/B =
	 * sin(c)/C Law of circle: sin^2(a) - cosine^2(a) = 1
	 */
	private void caculate() {
		if (trueCourse > zero)
			azimuth = circle - trueCourse;

		relativeWindDirection = windDirection + azimuth;
		if (relativeWindDirection > circle)
			relativeWindDirection = relativeWindDirection - circle;

		wradians = relativeWindDirection * radians;
		ratioSin = Math.sin(wradians) / trueAirSpeed;
		headingroundSpeed = ratioSin * windVelocity;
		headingCos = Math.sqrt(1 - (headingroundSpeed * headingroundSpeed));
		headingTan = headingroundSpeed / headingCos;
		headingRadians = Math.atan(headingTan);

		windCourseAdjustment = (int) (headingRadians * degrees);
		gl = trueAirSpeed * headingCos;
		wf = windVelocity * Math.cos(wradians);
		groundSpeed = (int) (gl - wf);
		magneticCourse = trueCourse + variance;
		magneticHeading = magneticCourse + windCourseAdjustment;

		if (magneticHeading < zero)
			magneticHeading = magneticHeading + circle;
	}

	/** Print Results */
	private void printResults() {
		System.out.println("Magnetic Heading: " + magneticHeading);
		System.out.println("Ground Speed:     " + groundSpeed);
	}

	public static void main(String[] argroundSpeed) {
		new AircraftPlotter();
	}
}
