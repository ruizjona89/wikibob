package fundamentals;

/**
 * IF YOU HAVE A CHOICE NEVER USE ARRAYS (OBSOLETE TECHNOLOGY)
 * 
 * Arrays are a collection of the same type, starting with index 0 and each cell
 * is an element
 * 
 * Arrays are static, primitive, not like a Class where there are methods that
 * the developer can utilize
 * 
 * List Collections are preferred over Arrays, here are some reasons why: 
 * 1) Dynamic - Lists change sizes automatically, the developer can concentrate on
 *                the logic and not worry about the management of the list. 
 * 2) methods - there are several methods available, e.g. add, remove, sort and much more. Arrays,
 *                the developer has to write ALL methods 
 * 3) null pointer - most likely never to have a null pointer or index out of range 
 * 4) interface / class - Arrays are very much like primitives, limited 
 * 5) type safe - <nothing else to add>
 */
public class ArrayExample {

	// example of an array of integers
	private static void intArrayExample() {

		int[] intArray1 = new int[5]; // declare an array size of 5 with no values
//		int[] intArray2 = { 1, 2, 3, 4, 5 };  // declare an array size of 5 with values 1 through 5
//		int[] intArray3 = new int[] { 1, 2, 3, 4, 5 };  // declare an array size of 5 with values 1 through 5

		intArray1[0] = 21;  // add value to intArray index 0
		intArray1[1] = 42;
		intArray1[2] = 33;
		intArray1[3] = 44;
		intArray1[4] = 55;

		System.out.print("int array example: ");
		for (int i = 0; i < intArray1.length; i++) {
			System.out.print(intArray1[i] + " ");
		}
	}

	// example of an array of doubles
	private static void doubleArrayExample() {

		double[] doubleArray = { 1.2, 2.2, 3.3, 4.76, 5.4 };

		System.out.print("\ndouble array example: ");
		for (int i = 0; i < doubleArray.length; i++) {
			System.out.print(doubleArray[i] + " ");
		}
	}

	// example of an array of objects
	private static void objectArrayExample() {
		Person[] persons = new Person[5];

		persons[0] = new Person("Bobby", 12);
		persons[1] = new Person("Teddy", 161);
		persons[2] = new Person("Mary", 22);
		persons[3] = new Person("Fred", 35);
		persons[4] = new Person("Steve", 47);
		
		System.out.println("\nobject array example:");
		for (int i = 0; i < persons.length; i++) {
			System.out.println(persons[i].getName() + " : " + persons[i].getAge());
		}
	}

	public static void main(String[] args) {

		ArrayExample.intArrayExample();
		ArrayExample.doubleArrayExample();
		ArrayExample.objectArrayExample();
	}
}
