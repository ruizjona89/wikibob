package exceptions;

/**
 * create our own Exception Class
 */
public class InfiniteLoopException extends Exception {

	private static final long serialVersionUID = 1L;

	public InfiniteLoopException(String message) {
		super(message);
	}
}
