package graphics;

import java.io.IOException;

public class Image {

	private static final int COLUMNS = 3;
	private int[][] pixelsArray = null;
	private int width = 0;
	private int height = 0;
	private String filename = "";
	private String filetype = "";

	public Image(String filename, String filetype) {
		this.filename = filename;
		this.filetype = filetype;
	}

	public Image(String filename) throws IOException {
		this.filename = filename;
	}

	public void createPixelsArray() {
		pixelsArray = new int[width * height][COLUMNS];
	}

	public int[][] getPixelsArray() {
		return pixelsArray;
	}

	public void setPixelsArray(int[][] pixelsArray) {
		this.pixelsArray = pixelsArray;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
}
