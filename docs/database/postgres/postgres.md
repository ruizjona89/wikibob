# postgres

## videos

- [Ubuntu Postgresql setup](https://www.youtube.com/watch?v=-LwI4HMR_Eg)
- [Ubuntu pgadmin setup](https://www.pgadmin.org/download/pgadmin-4-apt/)

## postgresql

### installation postgresql
- sudo apt-get -y install postgresql postgresql-contrib
- Ubuntu Software Center - pgadmin

#### pgadmin
 - ![General](pgadmin1.png)
 - ![Connection](pgadmin2.png)
 - ![Server](pgadmin3.png)

### notes

| Description | Syntax |
| ----------- | ------ |
| location of postgres files | /etc/postgresql          |
| service postgresql         | start, stop, status, etc |
| sudo su postgres           | login as postgres        |
| manual psql                | man psql                 |
| psql prompt                | psql                     |
| psql status                | systemctl status apache2 |

### commands
 
| Description | Syntax |
| ----------- | ------ |
| list databases | \l |
| list users, role name, attributes | \du |
| ALTER USER postgres WITH PASSWORD 'password'; | change password |
| CREATE USER cv64 WITH PASSWORD 'password'; | create user |
| ALTER USER cv64 WITH SUPERUSER; | set user with superuser |
| DROP USER cv64; | remove user |

### pgadmin tool

- http://localhost/pgadmin4
