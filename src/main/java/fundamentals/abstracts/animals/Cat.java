//class Cat has the implementation of the method
package fundamentals.abstracts.animals;

public class Cat extends Animal {

	public void eat() {
		System.out.println("Cat eatting");
	}

	public void meow() {
		System.out.println("Cat meowing");
	}
}