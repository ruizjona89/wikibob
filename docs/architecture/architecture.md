# Architecture

![System Architecture](systemArchitecture.draw.io.png)

The diagram above is a typical scenario of a web based application system architecture.  The key components are described in further detail below.  The Business Logic component would be representative of a [MONOLITHIC](https://en.wikipedia.org/wiki/Monolithic_application) design, a single instance of a tightly coupled application.  The newest architecture design is working with [MICROSERVICES](https://en.wikipedia.org/wiki/Microservices).  Separating TASKS into smaller loosely coupled applications.  The example illustrates the MICROSERVICES:  Requisitions, Purchase Orders, Receipts, Invoices, etc.

## Front End (User Interface) 

There are many architectures available for developing Front End code.  One popular product is the the [Angular Framework](https://en.wikipedia.org/wiki/Angular_(web_framework)) developed by Google.  Angular utilizes the [TypeScript](https://en.wikipedia.org/wiki/TypeScript) programming language which is a Superset of the [JavaScript](https://en.wikipedia.org/wiki/JavaScript) programming language.  The Front End accepts input from a CLIENT and sends the input to a SERVER in the form of a request.
     - [Client / Server Model](https://en.wikipedia.org/wiki/Client%E2%80%93server_model)

## Business Logic

- The Business Logic is typically a SERVER with programs that perform computations, access / update databases, etc.  The business logic is usually a general programming language, e.g. [Java](https://en.wikipedia.org/wiki/Java_(programming_language)), C++, etc.  The SERVER receives requests from CLIENTs, processes the requests and returns the requests as a response back to the CLIENT.

## Java Persistence Application Programming Interface (JPA) - Interface

- [JPA](https://en.wikipedia.org/wiki/Jakarta_Persistence) is an application programming interface which describes the management of the relational database.  JPA is an interface, defining the structure.  All implementation is performed by Object Relational Mapping (ORM) tools such as Hibernate.  Examples of the various annotations:  @Column, @EntityManager, @Query, @StoredProcedureQuery, @Table

- [Specifications](https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html)

- The [eclipse](https://en.wikipedia.org/wiki/Eclipse_(software)) Integrated Development Environment utilizes EclipseLink](https://en.wikipedia.org/wiki/EclipseLink)) framework allows interaction with various data services.

## Hibernate - Implementation

- [Hibernate](https://en.wikipedia.org/wiki/Hibernate_(framework)) is a Java Framework Object Relational Mapping (ORM) tool for the Java Programming language.  Mapping Java objects to Relational Databases.  Objects and Relational Databases are different structures, the ORM is the interface between these different structures so that transactions can occur.  Hibernate is an implementation of JPA and provides services to store and retrieve data from a relational database.

# Architecture Examples

|Company|Front End|Business Logic|Back End|
|--|--|--|--|
|Proactis|Combination of Angular and Java Server Pages|Java|Oracle RDB|
|[Recovhub](https://www.recovhub.com/)|Angular|PHP|Postgres RDB|
|[cv64](http://www.cv64.us/)|Angular|PHP|MySQL RDB|