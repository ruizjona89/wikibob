package fundamentals.basic;

public class StaticExample {

	private static int count = 0;

	// Constructors create objects - which instances of a Class
	public StaticExample(int objectCount) {
		count++;
//		Color color = Color.BLUE;
	}

	public static int getCount() {
		return count;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		// 1st StaticExample - Class
		// 2nd staticExample - object
		// 3rd StaticExample() - Constructor
		for (int i = 0; i < 3; i++) {
			StaticExample staticExample = new StaticExample(i);
			System.out.println();
		}

		System.out.println(StaticExample.getCount());
	}
}
