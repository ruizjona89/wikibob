# Springboot Basic

# Demo

- [Spring Boot Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#application-properties.server)
- [Spring Tools](springTools.png)
- [Spring Initializer - Start Spring IO](https://start.spring.io/)
- Spring is a Dependency Management Framework
- Execution:  http://localhost:6464/basicService/<<message>>
     - For Example:  http://localhost:6464/basicService/USS%20Constellation%20CV-64
![SpringBoot eclipse IDE](./src/main/resources/docs/springBoot0.png)

# Notes

- Spring Component Scan looks for:  @Component, @Controller, @Repository, @Service
- spring-boot-start-web - web services, REST
- spring-boot-starter-web-services - develop SOAP web services
- spring-boot-starter-test - JUnit, Mockito, Spring Unit Test
- spring-boot-starter-jdbc - JDBC, Spring JDBC
- spring-boot-starter-security - Spring Security
- spring-boot-starter-starter-data-jpa - provides Interfaces to access data and expose services with JPA and Hibernate
- spring-boot-starter-starter-data-rest - provides Interfaces to access data and expose services with REST

| items | description |
| ------ | ------ |
| starter parent | contains several items for a springboot project, e.g. default plugins, dependency versioning, java version, etc.  Versions are inhertied from Starter Parent |
| starter web | web applications |
| application launcher | starts the springboot application, like a main method for the Java Virtual Machine (JVM) |
| autoconfiguration | /src/main/resources/application.properties - logging.level.org.springframework: DEBUG |
| @Autowired | - injects objects into other objects<br>- injects the Environment object into the REST Controller or Service, e.g. @Autowired<br>private Environment environment;|
| @Component | register Java Beans as Spring components |
| @ComponentScan | the root package to scan for Springboot Classes |
| @ConfigurationProperties | gets all the properties and reads into a Bean, e.g. @ConfigurationProperties("spring.datasource", returns all the properties with the prefix "spring.datasource" |
| @DeleteMapping | Deletes the entity|
| @Entity | Plain Old Java Object (POJO) |
| EXCEPTIONS | @ResponseStatus @ExceptionHandler @ControllerAdvice |
| Filter and Interceptor | Both are the same, except Interceptor (Spring only) is more powerful because of the Handler object |
| @GetMapping | uses the path and gets the entity, e.g. @GetMapping("/path")|
| @Id | a primary key is required |
|[Interceptor](https://www.linkedin.com/pulse/introduction-interceptor-spring-mvc-aneshka-goyal/)||
| MessageConverter | look at the request and converts to the appropriate type of message, e.g. JSON |
| @PathVariable | {pathVariable}, the variable being passed in to the method |
| @PersistenceContext | Starting tracking transactions in the persistence context |
| @PostMapping | Adds the entity |
| @PutMapping | Update the entity |
| @Query | Query statement, e.g. @Query("select ...") |
| @RequestBody | HttpRequest body transferred to the Server, e.g. @RequestBody SomeDto someObject) |
| @RequestMapping | all incoming HTTP request URLs, e.g. @RequestMapping("path")|
| @RestController | the controller for a Controller or Service |
| @Repository | the class provides storage, retrieval, search, update and delete operations |
| @Service | the service itself |
| @SpringBootApplication | does a ComponentScan of the package this annotation is defined, e.g. us.cv64.springboot |
| @Transactional | Open / Close Transactions for each method |
| @Value | read the property, e.g. @Value("${server.port}")|

## Spring vs Spring Boot

### Spring

- Dependency Injection - @Autowiring
- Plumbing of Enterprise Applications, in other words, provided a framework to common tasks, so that developers can concentrate on their code and not installations, configurations, etc.
- Make testable applications
- Reduce the Plumbing of Code
- Simple Architecture, templates replaced code
- Integrates with other Frameworks, e.g. Hibernate, Struts
- Moving from Monolith Applications to Microservices
- Reduced Configuration, Framework Setup, Deployment, Logging, Transaction Management, Monitoring, Web Service Configuration

### Spring Boot (SB)

- Create standalone Spring Application execution
- Tries to Auto Configure products and projects
- Provides several features automatically, e.g.  embedded servers, security, metrics, health checks
- SB is not a Application or Web Server
- SB does not implement any specific framework but a wrapper, e.g.  JPA or JMS
- SB does not generate code

# Configuration
- [SpringBoot Tab](./springBoot1.png)
- [SpringBoot Arguments Tab](./springBoot2.png)
- [SpringBoot Classpath Tab](./springBoot3.png)
- [SpringBoot Common Tab](./springBoot4.png)

# Prerequisites
- at least JDK 8 (java -version) [jdk download](https://jdk.java.net/java-se-ri/14)
- at least Maven 3.6.3 (mvn -v) [mvn download](https://maven.apache.org/download.cgi)
     - Maven is a great product that incorporates (Transitive Dependencies) automating dependency management
     - for instance:  Web Applications require logging, Tomcat, etc. and Maven automatically incorporates several Java ARchive (JAR) files without the developer needing to download, configure and incorporate into the project

# pom.xml
## Spring Boot Application Setup
- Spring Boot Starter Parent contains and maintains all the dependencies and versions required
 
        <parent>
		   <groupId>org.springframework.boot</groupId>
		   <artifactId>spring-boot-starter-parent</artifactId>
		   <version>2.1.4.RELEASE</version>
	    </parent>
	    
	    NOTE:  Control + Click this line and will bring up Details:  <artifactId>spring-boot-starter-parent</artifactId>
 
 - Spring Boot Starter Web contains all the dependencies and versions required for Web applications
     - MVC, validation framework, logging
     
        <dependencies>
          <dependency>
		   <groupId>org.springframework.boot</groupId>
		   <artifactId>spring-boot-starter-web</artifactId>
		 </dependency>
		
		 <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test -->
          <dependency>
		   <groupId>org.springframework.boot</groupId>
		   <artifactId>spring-boot-starter-test</artifactId>
		   <scope>test</scope>
          </dependency>
        </dependencies>
     
- The appropriate Java Development Kit Version (minimum JDK 8)
 
- Spring Boot Plugin launching the Web application
     
        <plugin>
          <groupId>org.springframework.boot</groupId>
          <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>

- Spring Book Application Launcher - the Java file that is executed
 	
# Spring Boot Execution

- Execute either of the following
     - right click BasicApplication.java and run as Java Application
     - right click BasicApplication.java and run as Spring Boot App

- Spring Boot will start Tomcat on port specified in the src/main/resources/application.properties file
 
        logging.level.org.springframework: DEBUG
        server.port=8123
        
 - Navigate in a Browser to http://localhost:**<port number>**/basicService/**<enter a String>**
     - for instance:  http://localhost:**8123**/basicService/**USS Constellation CV-64**
