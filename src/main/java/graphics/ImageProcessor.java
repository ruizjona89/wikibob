// https://alvinalexander.com/blog/post/java/getting-rgb-values-for-each-pixel-in-image-using-java-bufferedi/
// System.out.println("x: " + x + " y: " + y + " pixel: " + pixel);
// System.out.println("x: " + pixelsImage1[i][X] + " y: " + pixelsImage1[i][Y] + " pixel: " + pixel);
package graphics;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageProcessor extends Component {

	private static final long serialVersionUID = 1L;
	private BufferedImage bufferedImage = null;
	private static Image image = null;
	private static final int X = 0;
	private static final int Y = 1;
	private static final int PIXEL = 2;
	private static final int COLUMNS = 3;

	public ImageProcessor(Image image) throws IOException {

		ImageProcessor.image = image;
		this.bufferedImage = ImageIO.read(new File(image.getFilename()));
		image.setWidth(bufferedImage.getWidth());
		image.setHeight(bufferedImage.getHeight());
		image.createPixelsArray();

		image2pixels();
	}

	public static Image getImage() {
		return image;
	}

	public static void setImage(Image image) {
		ImageProcessor.image = image;
	}

	public int[][] image2pixels() {
		int[][] pixelsArray = image.getPixelsArray();
		int pixel = 0;
		int i = 0;

		for (int y = 0; y < image.getHeight(); y++) {
			for (int x = 0; x < image.getWidth(); x++) {
				pixel = this.bufferedImage.getRGB(x, y);
//					alpha = (pixel >> 24) & 0xff;
//					red = (pixel >> 16) & 0xff;
//					green = (pixel >> 8) & 0xff;
//					blue = (pixel) & 0xff;

				pixelsArray[i][X] = x;
				pixelsArray[i][Y] = y;
				pixelsArray[i][PIXEL] = pixel;
				i++;
			}
		}

		image.setPixelsArray(pixelsArray);

		return pixelsArray;
	}

	public static BufferedImage pixels2image(String filename, String fileType, int[][] pixels) {

		BufferedImage bufferedImage = new BufferedImage(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_INT_RGB);

		int x = 0;
		int y = 0;
		int pixel = 0;

		try {
			File file = new File(filename);

			for (int i = 0; i < pixels.length; i++) {
				x = pixels[i][X];
				y = pixels[i][Y];
				pixel = pixels[i][PIXEL];

				bufferedImage.setRGB(x, y, pixel);
			}

			ImageIO.write(bufferedImage, fileType, file);

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return bufferedImage;
	}

	public static int[][] difference(ImageProcessor imageProcessor1, ImageProcessor imageProcessor2) {

		int[][] pixelsImage3 = null;
		int pixel1 = 0;
		int pixel2 = 0;
		int pixel = 0;
		int difference = 0;
		int maxWidthPixels = 0;
		int maxHeightPixels = 0;
		int maxPixels = 0;

		try {
			int ip1W = ImageProcessor.getImage().getWidth();
			int ip1H = ImageProcessor.getImage().getWidth();
			int ip2W = imageProcessor2.getWidth();
			int ip2H = imageProcessor2.getHeight();

			if (ip1W < ip2W) {
				maxWidthPixels = ip1W;
			} else {
				maxWidthPixels = ip2W;
			}

			if (ip1H < ip2H) {
				maxHeightPixels = ip1H;
			} else {
				maxHeightPixels = ip2H;
			}

			maxPixels = maxWidthPixels * maxHeightPixels;

			pixelsImage3 = new int[maxPixels][COLUMNS];

			for (int i = 0; i < maxPixels; i++) {

				pixel1 = ImageProcessor.getImage().getPixelsArray()[i][PIXEL];
				pixel2 = ImageProcessor.getImage().getPixelsArray()[i][PIXEL];

				difference = Math.abs(pixel1 - pixel2) / 2;

				if (pixel1 < pixel2) {
					pixel = pixel1 + difference;
				} else {
					pixel = pixel1 - difference;
				}

				pixelsImage3[i][X] = ImageProcessor.getImage().getPixelsArray()[i][X];
				pixelsImage3[i][Y] = ImageProcessor.getImage().getPixelsArray()[i][Y];
				pixelsImage3[i][PIXEL] = pixel;
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		return pixelsImage3;
	}
}
