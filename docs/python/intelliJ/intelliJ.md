# IntelliJ / Python Plugin Installation

# [IntelliJ Installation]

##### Welcome Screen - Press Next >
![Step 1](ij01.png)

##### Choose Installation Location - Press Next >
     - Windows:  c:\git\ij
     - Linux:  /opt/ij
![Step 2](ij02.png)

##### Installation Options - Check Boxes as shown in image - Press Next >
![Step 3](ij03.png)

##### Start Menu Folder - Press Install
![Step 4](ij04.png)

# [Python Installation](../imagesIntelliJ/pythonInstallation.md)

# [IntelliJ Python Plugin](https://www.jetbrains.com/help/idea/plugin-overview.html#a5f1f75a)

- Navigate to the Settings Page (CTRL - ALT - S) 
- Navigate to Plugins
- Search for Python
- Select Install
- When Python is Installed then press OK

![Python Plugin](1_pythonPlugin.png)

- Select New Project
- Select Python
- Select Django Checkbox
- Select Project SDK Dropdown
- Add Python SDK...

![Configurations](2_configurations.png)

- Select Existing Environment Radio Button
- Interpreter:  Locate the SDK on your system and Select
     - Windows:  C:\Program Files\Python310\python.exe
     - Linux:  /usr/bin/python3.9
- Press OK

![Add Interpreter](3_addInterpreter.png)

- Create new Project Name: python, ceis295, ...
- Project Location: set the git directory / project name
     - Windows:  c:\git\ceis295  
     - Linux:  ~/git/ceis295
     - Press Finish

![New Project](4_newProject.png)

- Create New Directory
     - Right Click project directory
     - New Directory
     - Provide name, e.g. python, week1, etc.
     
- Create New Python File
     - Right Click directory
     - New Python File
     - Provide name, e.g. HelloWorld.py
     
![New Python File](5_newPythonFile.png)
- Paste the following code into the HelloWorld.py file
- Save the file

```
print("Hello World")
```

![Run Python Program](6_runPythonProgram.png)
- Right Click Code
- Select Run HelloWorld.py

![Program Success](7_programSuccess.png)
