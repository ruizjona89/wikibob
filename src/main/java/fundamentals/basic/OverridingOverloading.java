package fundamentals.basic;

/**
 * 
 * Overloading methods have the same name only differentiated by the parameters
 * signature.
 * 
 * Overriding methods have the same name and parameters signature. Overriding
 * methods are usually Classes extended from a parent class and when implemented
 * OVERRIDE the parent implementation.
 *
 */
public class OverridingOverloading {

	// Overloading - getAnimal is the method, this is no arguments
	public void getAnimal() {

	}

	// Overloading - getAnimal is the method, this has one argument
	public void getAnimal(String name) {

	}

	// if executed this line (OVERRIDDING) the Object.toString
	@Override
	public String toString() {
		return "";
	}

	public static void main(String[] args) {

		Object object = new Object();

		// if executed this line (OVERRIDDING) the Object.toString
		object.toString();
	}
}
