# ![](../../docs/icons/cv64us50x50.png) linux

- [Code](https://gitlab.com/bobby.estey/linux)
- [Ubuntu](./ubuntu)

This page contains information about Linux / Ubuntu Configurations, Commands, etc.

|ripgrep commands|description|
|--|--|
|installation|sudo apt-get install ripgrep|
|rg --maxdepth 1 \<search item>|rg commands|
|du -a | rg \<search item>|another example|
|rg "name=\"pass\""|search with search value within double quotes, use backslash \|

|command|description|
|--|--|
|top -p<pid> -p<pid>|top only particular processes|
|free -m|free megabytes|
|ln -s /opt2/java/jdk1.6.0_45/bin /usr/java/bin|symbolic link for /usr/java|
|tar -zxvf <tar.gz file> [-C <folder to extract to>]|Extract a tar.gz tarball|
|tar -zcvf <desired tarball name>.tar.gz <file or folder to compress>|Create a tar.gz tarball|
|- sudo su<br>- mkdir -p /mnt/data<br>- apt-get install cifs-utils<br>- mount -t cifs //10.10.1.239/data/ /mnt/data -o<br>- username=rwe001,domain=Corporate.s600605-ad01.corp|- map S drive<br>- NOTE:<br>- Firefox & Chrome: file:///S:/data/techdev/engineering/shared/wiki/index2.html<br>- ie: S:\data\techdev\engineering\shared\wiki\index2.html||
|- hostname -i<br>- ip addr show eth0 | grep 'inet ' | awk '{ print $2 }' | sed 's/\/.*//g'|- ip address of current machine<br>- or you can do it the hardway|
|sudo su - chiefs|super user as chiefs|
|bash -x \<script file>|execute script file verbose|
|fc -ln -<history> | grep \<searchItem>|list the last <history> command line calls|
|rpl -R EBO13 EBO46 .|replace recursively EBO13 with EBO46 starting from the current directory|
|netstat -tulpn|network status|
|grep -riIn 'pattern' * (note uppercase I (eye))|search for a pattern in a file recursively|
|grep -riIl 'pattern' * (note uppercase I (eye) , lowercase l (el))|search for a pattern and displays the filename only|
|find . -type f -newermt '1/30/2017 0:00:00'|find files after date|
|locate \<filename>|locates an executive|
|pgrep "<program name / command|Return PID of matching programs|
|csearch -f ".*.jsp$" -l "string"|search within a file|
|installing .deb file|sudo dpkg -i /path/to/deb/file<br>sudo apt-get install -f|
