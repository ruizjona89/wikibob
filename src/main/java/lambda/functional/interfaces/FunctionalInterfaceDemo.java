package lambda.functional.interfaces;

@FunctionalInterface
public interface FunctionalInterfaceDemo {

	void abstractMethod();
	
	default void printMethod1() { System.out.println("printMethod1"); }
	default void printMethod2() { System.out.println("printMethod2"); }
}
