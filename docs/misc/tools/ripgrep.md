# ripgrep

## ripgrep installation

- curl -Lo ripgrep.deb "https://github.com/BurntSushi/ripgrep/releases/latest" | grep -Po '"tag_name": "\K[0-9.]+')
- sudo apt install -y ./ripgrep.deb
- rg --version

## examples

- rg <search term> case sensitive
- rg -i <search term> example of ignore case
