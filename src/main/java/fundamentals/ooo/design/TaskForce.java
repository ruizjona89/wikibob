/**
 * TaskForce - temporary organizations composed of particular ships, aircraft, submarines, military land forces, 
 * or shore service units, assigned to fulfill certain missions.
 */
package fundamentals.ooo.design;

import java.util.ArrayList;
import java.util.List;

public class TaskForce {

	private int fleet = 0;
	private List<Air> squadrons = new ArrayList<>();
	private List<Ship> ships = new ArrayList<>();
//	private List<Land> forces = new ArrayList<>();

	public TaskForce(int fleet, List<Air> squadrons, List<Ship> ships, List<Land> forces) {

		this.fleet = fleet;
		this.squadrons = squadrons;
		this.ships = ships;
	}

	public TaskForce() {
		this(0, null, null, null);
	}

	/**
	 * @return the attribute values
	 */
	public String toString() {
		String string = "Fleet: " + fleet + "\n";

		string += "\nShips:\n";
		for (Ship ship : ships) {
			string += ship.toString() + "\n";
		}

		string += "\nSquadrons:\n";
		for (Air squadron : squadrons) {
			string += squadron.toString() + "\n";
		}

		// forces
		string += "\n";

		return string;
	}
}