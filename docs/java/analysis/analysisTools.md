[[_TOC_]]

# Set up Java Management eXtensions (JMX) Remote Port
- WARNING:  Never set JMX with JAVA_OPTS for Tomcat

```CATALINA_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=10.1.1.1"```

# jvisualvm
- jvisualvm can connect and look at the vm as it's running instead of waiting for a heap dump (though it can force a heap dump so that you can use jvisualvm to analyze the dump later).

## Execution
- The executive is located in $JAVA_HOME/bin
  - $JAVA_HOME/bin/jvisualvm

## jsvisualvm Configuration
![jvisualvm.png](jvisualvm.png)
- Right Click Remote -> Add Remote Host...
- Enter Host name with port, e.g. 10.1.1.1:9010, Display name is optional
- Press OK
![remoteHost.png](remoteHost.png)
- Right Click the newly created Remote Host -> Add JMX Connection...
- Enter Connection with port, e.g. 10.1.1.1:9010, Display name is optional, if not already populated.
- Check Do not require SSL connection, press OK
![jmxConnection.png](jmxConnection.png)

# [Memory Analyzer Tool (MAT)](https://www.eclipse.org/mat/)
- [MAT Tutorial](https://eclipsesource.com/blogs/2013/01/21/10-tips-for-using-the-eclipse-memory-analyzer/)
![mat.png](mat.png)

# Video
- [MAT Video](http://www.cv64.us/cv64/recordings/mat.mp4)

