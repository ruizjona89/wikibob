package generics;

import java.util.ArrayList;

// Set Box Class as a Generic
@SuppressWarnings("rawtypes")
public class LinkedListNode<T> extends ArrayList {

	private static final long serialVersionUID = 1L;
	// T - type parameter
	private T t = null;

	// T - type argument
	public LinkedListNode(T t) {
		System.out.println("LinkedListNode.Constructor: " + t);
		this.t = t;
	}

	// T - type argument
	public void set(T t) {
		System.out.println("LinkedListNode.set: " + t);
		this.t = t;
	}

	// T - return a Type
	public T get() {
		System.out.println("LinkedListNode.get: " + t);
		return t;
	}
}
