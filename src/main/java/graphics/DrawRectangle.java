package graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class DrawRectangle extends JFrame implements MouseListener {

	private static final long serialVersionUID = 1L;
	private int x = 50; // leftmost pixel in circle has this x-coordinate
	private int y = 50; // topmost pixel in circle has this y-coordinate

	public DrawRectangle() {
		setSize(800, 800);
		setLocation(100, 100);
		addMouseListener(this);
		setVisible(true);
	}

	// paint is called automatically when program begins, when window is
	// refreshed and when repaint() is invoked
	public void paint(Graphics g) {
		g.setColor(Color.ORANGE);
		g.fillRect(x, y, 100, 100);
	}

	// The next 4 methods must be defined, but you won't use them.
	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseClicked(MouseEvent e) {
		x = e.getX(); // x-coordinate of the mouse click
		y = e.getY(); // y-coordinate of the mouse click
		repaint(); // calls paint()
	}
}
