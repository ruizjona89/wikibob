# Collections

## Big O (Algorithm Complexity)

- [Big O Cheat Sheet](https://www.bigocheatsheet.com/)
- [Big O](https://en.wikipedia.org/wiki/Big_O_notation)

## Collection Types

- arraylist
- btree
- comparator
- heap
- queue
- stack
    - ![stack](stack/stack.png)
- tree