# Spring

## Demo

- service1 - http://localhost:8111/service1Service/{id}
- service2 - http://localhost:8112/service2Service/{id}
- service3 - http://localhost:8113/service3Service/{id}

- [service1 example](http://localhost:8111/service1Service/bobby)
- [service2 example](http://localhost:8112/service2Service/bobby)
- [service3 example](http://localhost:8113/service3Service/bobby)