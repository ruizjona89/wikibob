package collections.hashMap;

import java.util.HashMap;
import java.util.Map;

public class HashMapExample {

	public static void main(String[] args) {

		Map<Integer, String> animals = new HashMap<>();
		animals.put(1, "Dog");
		animals.put(2, "Cat");
		animals.put(3, "Goat");
		animals.put(4, "Chicken");

		for (Map.Entry<Integer, String> entry : animals.entrySet()) {
			System.out.println(entry.getKey() + ":" + entry.getValue());
		}
	}
}