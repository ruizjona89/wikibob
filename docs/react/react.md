# React / [Redux](./redux/redux.md) / [Nexus](./nexus/nexus.md)

# Installation

|Command|Description|
|--|--|
|[WebStorm Instructions](./webStorm/webStorm.md)|WebStorm Instructions|
|[excellent tutorial](https://www.jetbrains.com/webstorm/guide/tutorials/react_typescript_tdd/project_setup/)|https://www.jetbrains.com/webstorm/guide/tutorials/react_typescript_tdd/project_setup/|
|npm install -g create-react-app| Install React|
|npx create-react-app <b>projectName</b>|Creating a new React app in /home/cv64/git/cv64/react/fundamentals.<br>Installs node packages. This might take a couple of minutes.<br>Installing react, react-dom, and react-scripts with cra-template...<br>Created git commit.<br>Success! Created fundamentals at /home/cv64/git/cv64/react/fundamentals
Inside that directory, you can run several commands:|
|[npm start](npmStart.png)|Starts the development server and application|
|npm run build|Bundles the app into static files for production.|
|npm test|Starts the test runner.|
|npm run eject|Removes this tool and copies build dependencies, configuration files and scripts into the app directory. If you do this, you can’t go back!|
|install react test|npm install --save-dev @testing-library/react react-test-renderer @testing-library/jest-dom babel-jest @babel/preset-env @babel/preset-react enzyme @wojtekmaj/enzyme-adapter-react-17 @types/jest|

|Terminology|Description|
|--|--|
|Callback Functions|Child components passes state variable back to parent component|
|[useCallback Hook](https://www.youtube.com/watch?v=IL82CzlaCys)|Return a memoized version of the callback function that only changes if one of the dependencies has changed|
|JSX|JavaScript Extension or JavaScript XML|
|[Properties](https://lucybain.com/blog/2016/react-state-vs-pros)|static variables passed from a parent component to a child component|
|Rerender|export default React.memo(Component)  Only rerender if properties or state has changed|
|[State](https://lucybain.com/blog/2016/react-state-vs-pros)|dynamic variable groups initialized by props|

# React Libraries

- React - framework for web development
     - The React libraries required to execute a React program building User Interfaces (UI)
     - Platform Agnostic - React can be a web, mobile phone or native application that do not use the web DOM library
     
- ReactDOM 
     - Imports ES6 load libraries and is used only in index.js file and applies only to web applications and websites 

- React Native - framework for mobile development
     - React Framework to build native mobile applications which allows React developers to write programs on a mobile client without requiring knowledge of Android or iOS

# React Component

![React Component](react.component.drawio.png)

- Class Components
     - same as Functional Components with additional and more complex and interactive components
     - has properties and methods
     - adopting Object Oriented Features
     - holds state
     - add lifecycle events
- Custom Components
     - Reducing the quantity of code on a page
- Functional Components
     - Returns JSX Code
     
# React Component Example

![React Component Example](react.component.example.drawio.png)

# Fat Arrow Examples
|ES5|ES6 old|ES6 new|
|--|--|--|
|function name()|function()|() =>|
|function name(x)|function(x)|x =>|
|function name(x)|function(x)|(x) =>|

# Code

- Hello React Program

```
import React from 'react';
import ReactDOM from 'react-dom';

/**
  The “App” function name is a commonly used starting point for many React applications.  
  The purpose is to display JSX code to the browser. 
  JSX looks like HTML, is a unique Javascript to React. 
  @see https://reactjs.org/docs/introducing-jsx.html
*/
const App = () => {
  return <div>Hello React</div>
}

/**
  Displays the entire application in the web page identified as #root. 
  Open public/index.html file and locate first element <div id=”root” />. 
  The standard name is "root" can be named differently.
*/
ReactDOM.render(<App />, document.querySelector("#root"));
```

# Errors

## Code
|Error|Solution|Reference|
|--|--|--|
|react-jsx-dev-runtime.development.js:117 Warning: Each child in a list should have a unique "key" prop.|add key id to the element, e.g. key = {album.id} or key = {incrementer++}||
|error enospc system limit for number of file watchers reached watch '/snap/code'|sudo gedit /etc/sysctl.conf<br>add last line the following and save fs.inotify.max_user_watches=524288<br>test with the following:  sudo sysctl -p|https://stackoverflow.com/questions/55763428/react-native-error-enospc-system-limit-for-number-of-file-watchers-reached|
|TS2322: Type '(props: PropsWithChildren<DataGridProperties>) => void' is not assignable to type 'FC<DataGridProperties>'.  Type 'void' is not assignable to type 'ReactElement<any, any> \| null'.|- Calling program must pass parameters, e.g. ```MyComponent dataGridDetailedRecords={dataGridDetailedRecords}/>```<br>- Program being called must have return, e.g.  ```return(<div></div>);```||

## Testing
|Error|Solution|Reference|
|--|--|--|
|Error: Method “props” is meant to be run on 1 node. 0 found instead.|node was not found, fix with correct id, class, etc.||
|<--- Last few GCs --->[51482:0x67344e0]    33117 ms: Scavenge 2034.5 (2076.5) -> 2032.3 (2077.0) MB, 4.2 / 0.0 ms  (average mu = 0.151, current mu = 0.116) allocation failure FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory|Cannot find element||

# Glossary / Notes
|Item|Description|
|--|--|
|[Debugging](https://egghead.io/lessons/react-render-an-object-with-json-stringify-in-react)|{JSON.stringify(attributeName)}|
|Types|Use for React Props and States, types are more constrained|
|Interfaces|Have more features which goes against type safety, use for public API|

# References

- [React calling REST Service](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/react/reactCallingREST.mp4)
- [JavaScript Commands](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements)
- [bablejs.io - |converts latest js to old js](https://babeljs.io/)
- [react axios](https://www.digitalocean.com/community/tutorials/react-axios-react)
