# Streams2

## Parallel Stream

- Uses multiple processor cores, so code can execute in parallel on separate cores and result outcome combined.
  
# Intermediate Operation

- Operations that return another stream as a result
     - filter(), map(), distinct(), sorted(), limit(), skip()
     - peek() - helps debugging
     - reduce() - combine elements into a single value
          - ```System.out.println(list.stream().reduce((a,b) -> a+b).get());```
     - Lazily Loading - need a Terminal Operation to display results
     - operations can be chained
     - multiple operations are permitted
     - do not produce end result
     
# Terminal Operation     

- Operations that return non-stream values (objects, primitives or void)
     - forEach(), toArray(), reduce(), collect(), min(), max(), count(), anyMatch(), allMatch(), noneMatch(), findFirst(), findAny()
     - Eagerly Loaded
     - operations cannot be chained
     - only one operation is permitted
     - produce end result
     
