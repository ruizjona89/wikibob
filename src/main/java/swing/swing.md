Swing Basics - Swing is Java's User Interface Application Programming Interface (API) that contains a toolset of Widgets.  Think of Widgets as things you can apply to a webpage, e.g.  buttons, checkboxes, images, videos, etc.

Model / View / Controller (MVC)
 - model - data - database or file
 - controller - manager - server
 - view - user interface

SwingSet

All Swing Classes start with J

The fundamental Widget to Swing is the Frame.  The Frame is like a picture Frame which encapsulates the User Interface (UI).  Below is an Example of a Frame.  Frames only contain:
 - title bar - the title of the UI Application, in this case "Frame Example"
 - minimize button - the dash to minimize the UI Application
 - maximize button - the open square to maximize the UI Application
 - close button - the circle X - to close the UI Application
 - container - the outer boundary of the title bar, buttons and the blank space that is encapsulated
 - [code example](./FrameExample.java)

 ![frame example](./frameExample.png)

Adding a Widget to the Frame.  The Button is one of several Widgets in the Swing Toolset

Layouts

Events / Listeners



