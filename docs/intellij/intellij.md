# IntelliJ

|Command|Description|
|--|--|
|ctrl click| |
|ctrl h|show hidden files|
|ctrl shift f| |
|target|locate code in Project explorer|

|database commands|Description|
|--|--|
|Database -> QL button|opens up the console|


|git commands|Description|
|--|--|
|Git -> Show Git Log| Repository Tree|
|Right Git -> Show History| |