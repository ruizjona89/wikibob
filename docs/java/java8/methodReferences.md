# Method References

- Replacement of Lambda Expressions
- Used for Code Reusability
- Use method references if there are abstract methods in the Functional Interface

```
// AnotherClass::methodWithinClass; - method reference
FunctionalInterfaceExample functionalInterfaceExample = AnotherClass::methodWithinClass;
functionalInterfaceExample.singleAbstractMethod();
```-
  