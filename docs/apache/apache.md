# ![](../../docs/icons/cv64us50x50.png) Apache Software Foundation

[Apache Software Foundation](https://apache.org)

- Amazing Products that are available for free.

# Recordings

## [Open Office](https://www.youtube.com/watch?v=IRdM8JfOqDI&t=4s)

- Office technology is old, very old and obsolete so stop paying for, utilizing static software that cannot compare to freeware today
- If you have to have Office products, STOP paying for proprietary, licensed products
- Open Office, Libre Office and Google Docs are the best and superior solutions today and can read / write other product solutions, including proprietary
     
## [Word Processor History](https://www.youtube.com/watch?v=vFg7AQo9I8A)
- Learn the history and the Truth about Word Processors