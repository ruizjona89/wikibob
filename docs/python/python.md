# python

## links

 - [online python compiler](https://www.onlinegdb.com/online_python_compiler)
 - [eclipse installation download](https://www.eclipse.org/downloads/)
 - [python compiler download](https://www.python.org/downloads/)
 - [eclipse python installation and configuration](https://www.youtube.com/watch?v=R-YJ1qdZwPY)
 - [intelliJ installation and configuration](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/python/intelliJ/intelliJ.md)
 
## installation

- [Python Installation intelliJ](imagesIntelliJ/pythonInstallation.md)
- [Python Installation eclipse](imagesEclipse/pythonInstallation.md)

### Ubuntu

- sudo apt update
- sudo apt install software-properties-common
- sudo add-apt-repository ppa:deadsnakes/ppa
- sudo apt install python3.9
- python3 --version
- ll /usr/bin/python3
     - lrwxrwxrwx 1 root root 9 Dec 22 01:42 /usr/bin/python3 -> python3.9*
- sudo apt install python3-pip
- pip3

- [ubuntu1.png](ubuntu1.png)
- [ubuntu2.png](ubuntu2.png)
- [ubuntu3.png](ubuntu3.png)
 
## notes

 - [relative / absolute paths](https://www.youtube.com/watch?v=uwtupH7LJco)
 - [importing-files-from-different-folder](https://stackoverflow.com/questions/4383571/importing-files-from-different-folder)
 
## Big O

 - O(n)         : Linear (Simplest)
 - O(n2)        : Selection
 - O(log n)     : BinarySearch
 - O(n * log n) : Quicksort