# ![](./docs/icons/cv64us50x50.png) wikiBob (Bobby's Wiki)

|General Links|USS Constellation (CV64)|![](./docs/icons/cv64BattleE.jpg)|
|--|--|--|
|[Apollo 11<br/><br/>20 July 1969<br/><br/>MODERN MARVELS](https://www.youtube.com/watch?v=NaNw12Q_ZI0)|Greatest Human Story Ever|I remember as a kid 10 years old, watching the entire event as is yesterday.  I remember clearly.  This was the AMERICA I remember, the Greatest Country Ever.  My entire family was huddled around our 25" Curtis Mathis TV Entertainment System watching the Astronauts emerge and eventually, "Walk on the Moon".  They did all that COOL STUFF and everyone on the Planet was Proud and at Peace.  I then walked out my home's front porch and then looked DIRECTLY at the Moon and knew there were TWO MEN currently ON THE MOON.  Let me be clear.  I was looking at the Moon and there were TWO MEN ON THE MOON and one Orbiting.  Something I will never forget, EVER.  Kids today, I will let you know a Hero:  Nurses, Doctors, Teachers, YOUR PARENTS and finally YOUR GOD, your Creator.  GOD LOVES YOU MORE THAN EVERYTHING HE CREATED.  John 3:16 - "For God so loved the world that He gave His only begotten Son, that whoever believes in Him should not perish but have everlasting life."|
|[Acronyms and Terminology](./docs/terminology/acronymsGlossary.md)|[Architecture](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/architecture/architecture.md)|This is my Personal Wiki Page which is in work at this time.|
|[Resume](https://gitlab.com/bobby.estey/bobby/-/tree/main/resume)|[Teaching](./docs/teaching.md)|[Miscellaneous](./docs/misc/misc.md)|

## History of Wiki Bob

I first received the nickname (Wiki Bob) when I was employed at Lockheed Martin.  I documented everything and always utilized the Internet as the source due to the fact, I was able to get to my notes anywhere on the Planet.  When employed at Lockheed we traveled 100% for 1.5 years.  I use to carry a scrapbook that kept my notes but that meant to carry another item along with me at all times.  I also noted that, others could also share and update my work so that TRUE COLLABORATION was in action.

## Technologies

|Icon<br>- Markdown<br>- Repository|Icon<br>- Markdown<br>- Repository|Icon<br>- Markdown<br>- Repository|Icon<br>- Markdown<br>- Repository|Icon<br>- Markdown<br>- Repository|Icon<br>- Markdown<br>- Repository|
|--|--|--|--|--|--|
|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Android_robot_head.svg/320px-Android_robot_head.svg.png" width="50" height="27"><br>- [Android](./docs/android/android.md)<br>- [Repository](https://gitlab.com/bobby.estey/android)|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/240px-Angular_full_color_logo.svg.png" width="50" height="50"><br>- [Angular](./docs/angular/angular.md)<br>- [Repository](https://gitlab.com/bobby.estey/angular)|<img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/ASF_Logo.svg" width="40" height="40"><br>- [Apache](./docs/angular/apache.md)<br>- [Repository](https://gitlab.com/bobby.estey/apache)|<img src="https://hackr.io/tutorials/assembly-language/logo-assembly-language.svg?ver=1603208610" width="40" height="40"><br>- [Assembler](./docs/assembler/assembler.md)<br>- [Repository](https://gitlab.com/bobby.estey/assembler)|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/213px-ISO_C%2B%2B_Logo.svg.png" width="30" height="30"><br>- [CPP](./docs/cpp/cpp.md)<br>- [Repository](https://gitlab.com/bobby.estey/cpp)|<img src="https://upload.wikimedia.org/wikipedia/commons/0/0d/C_Sharp_wordmark.svg" width="30" height="30"><br>- [C#](./docs/cpp/cpp.md)<br>- [Repository](https://gitlab.com/bobby.estey/cpp)|
|<img src="https://upload.wikimedia.org/wikipedia/commons/d/d0/Eclipse-Luna-Logo.svg" width="100" height="22"><br>- [eclipse](./docs/eclipse/eclipse.md)<br>- [Repository](https://gitlab.com/bobby.estey/eclipse)|<img src="https://upload.wikimedia.org/wikipedia/commons/b/b8/Fortran_logo.svg" width="30" height="30"><br>- [ForTran](./docs/fortran/fortran.md)<br>- [Repository](https://gitlab.com/bobby.estey/fortran)|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/320px-Git-logo.svg.png" width="50" height="26"><br>- [git](./docs/git/git.md)<br>- [Repository](https://gitlab.com/bobby.estey/git)|<img src="https://cdn.icon-icons.com/icons2/2552/PNG/512/chrome_devtools_browser_logo_icon_153005.png" width="35" height="35"><br>- [Google Devtools](./docs/googleDevtools/googleDevtools.md)|<img src="https://miro.medium.com/max/1400/1*lJ32Bl-lHWmNMUSiSq17gQ.png" width="60" height="60"><br>- [HTML / CSS](./docs/htmlCss/htmlCss.md)<br>- [Repository](https://gitlab.com/bobby.estey/htmlCss)|<img src="https://upload.wikimedia.org/wikipedia/en/thumb/3/30/Java_programming_language_logo.svg/800px-Java_programming_language_logo.svg.png" width="28" height="50"><br>- [Java](./docs/java/java.md)<br>- [Repository](https://gitlab.com/bobby.estey/java)|
|<img src="https://www.w3schools.com/whatis/img_js.png" width="40" height="40"><br>- [JavaScript](./docs/javascript/javascript.md)<br>- [Repository](https://gitlab.com/bobby.estey/angular)|<img src="https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg" width="40" height="50"><br>- [Linux](./docs/linux/linux.md)<br>- [Repository](https://gitlab.com/bobby.estey/linux)|<img src="https://upload.wikimedia.org/wikipedia/commons/2/27/PHP-logo.svg" width="73" height="40"><br>- [PHP](./docs/php/php.md)<br>- [Repository](https://gitlab.com/bobby.estey/php)|<img src="https://miro.medium.com/max/1400/1*lJ32Bl-lHWmNMUSiSq17gQ.png" width="60" height="60"><br>-[Postscript](./docs/postscript/postscript.md)|<img src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg" width="40" height="40"><br>- [Python](./docs/python/python.md)<br>- [Repository](https://gitlab.com/bobby.estey/python)|<img src="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" width="50" height="50"><br>- [React](./docs/react/react.md)<br>- [Repository](https://gitlab.com/bobby.estey/react)|
|<img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/UML_logo.svg" width="50" height="36"><br>- [UML](./docs/uml/uml.md)|<img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Progressive_Web_Apps_Logo.svg" width="70" height="25"><br>- [Web Application](./docs/webApplication/webApplication.md)|DATABASES<br>- [Databases](./docs/database/databases.md)<br>- [Repository](https://gitlab.com/bobby.estey/databases)|||<img src="https://openjsf.org/wp-content/uploads/sites/84/2019/03/openjsf-color-textw.svg" width="100" height="40"><br>- [NodeJS](./docs/nodejs/nodejs.md)<br>- [Express](./docs/nodejs/expressApplication/express.md)|
|<img src="https://spring.io/images/spring-logo-9146a4d3298760c2e7e49595184e1975.svg" width="116" height="30">|[Spring Boot](./docs/spring/boot/springBoot.md)|[Spring Cloud](./docs/cloud/springCloud.md)|[Spring Framework](./docs/spring/boot/springFramework.md)|[Spring Microservices](./docs/spring/microservices/microservices.md)|[Spring Rest](./docs/spring/rest/springRest.md)|
 
## References
 
Who should you watch and learn from:

- [Dr Francis Sandy Hill (RIP)](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/cpp/francisSandyHill.md) / Computer Graphics
- [Koushik Kothagal / Java Brains](https://javabrains.io/)
- [Ranga Karanam / in28Minutes](https://in.linkedin.com/in/rangakaranam)
- [Derek Banas](https://www.youtube.com/user/derekbanas)
- [Mkyong](https://mkyong.com)
- [Brad Traversy](https://www.youtube.com/user/TechGuyWeb)
- [Maximilian Schwarzmuller](https://www.youtube.com/watch?v=ug56AF2FaDQ)
- [Jasper Zhao](https://www.youtube.com/channel/UCb_eL_TfwVVcYBt8ssnsmig)

## Notes to Self :-)

- [whiteboard](https://www.tutorialspoint.com/whiteboard.htm)
- git push https://bobby.estey@gitlab.com/bobby.estey/java.git/

