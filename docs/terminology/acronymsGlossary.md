# Acronyms / Glossary

## Quick Terms

|Term|Description|Reference|
|--|--|--|
|Application Server vs Web Server|||
|[Asynchronous](https://martech.zone/wp-content/uploads/2012/09/asynchronous.png)|Callback - threading, doing things in the background||
|Cascading Style Sheets (CSS)|element: h1 {...} class .classname {...} id #id {...} ||
|Continuous Delivery (CD)|e.g. 2 week sprints||
|Continuous Integration (CI)|- updating<br>version control - git||
|Cross-Site Scripting (XSS)|Script gets placed on suspected site and clients accidentally execute the script and information is stolen||
|CRUD|Create Read Update Delete||
|DevOps (Development / Operations)|plan, code, build, test, release, deploy, operate, monitor||
|[Document Object Model](https://en.wikipedia.org/wiki/Document_Object_Model#/media/File:DOM-model.svg)|- web page loaded, browser creates DOM which consists of nodes of a document, e.g. <html>, <head>, etc.||
|Domain Driven Design|Software should match the Business Domain, e.g. Classes, objects, methods, attributes should match domain characteristics.  Aircraft Manufactures would have Fuselage, Engine, Wing Classes||
|[draw.io](https://www.youtube.com/watch?v=WlCKv49Pkvg)|- tutorial on how to use a fantastic drawing tool||
|Data Access Object|provides an abstract interface to a database||
|Data Transfer Object|an object of attributes only and only behavior is storage, retrieval, serialization, deserialization||
|Dynamically Typed|Type associated with a runtime value and can change during runtime||
|Encoding|converts characters into a format that can be transmitted, e.g. Bobby Estey = Bobby%20%Estey||
|Extreme Programming|- increase updates many times||
|[Functional Interface](https://gitlab.com/bobby.estey/wikibob/-/tree/master/java/lambda/functional/interfaces)|@FunctionalInterface (receives a single value and returns a single value)||
|[Idempotency](https://nordicapis.com/understanding-idempotency-and-safety-in-api-design/)|||
|[Lambda](https://gitlab.com/bobby.estey/wikibob/-/tree/master/java/lambda)|anonymous function that can pass / return data from a method||
|[markdown syntax](https://www.markdownguide.org/basic-syntax/)|markdown cheatsheet||
|Pipeline|Jenkins and Azure Builds|Git Checkout->Build Custom Environment->Test(client and server)->build image(docker)->misc ops, e.g. database,patches->deploy|
|Stateful|persistent|less secure than stateless, used with binding operations|
|Stateless|non persistent|more secure than stateful, execute and done.  A new transaction, process, etc. every time|
|Synchronous|standing in line||
|Test-Driven Development (TDD)|Software Requirements converted to Test Cases before software is fully developed.  Software development being repeatedly tested against test cases|Write Test -> Check Test -> Write Code -> Check with All Tests -> Refactoring (update tests)|
|Type Erasure|[Type Erasure](https://www.youtube.com/watch?v=lxcywrPzCGI)<br>- used at compile time to check the types, not runtime||
|Typed vs Untyped (loose) Language|JavaScript allows variables to be of any type, e.g. var, where Typed is specifying the type||

## Product Terms

- [JPA / Hibernate](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/jpaHibernate/jpaHibernate.md) 
- [Patterns](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/terminology/patterns/patterns.md)

## [Storage](https://i.stack.imgur.com/sMNoo.png)

|Term|Description|Reference|
|--|--|--|
|Local Storage|Not Session Based, data is persists even when browser is closed and reopened, Client Side Reading Only|localStorage.setItem('key', 'value');<br>localStorage.getItem('key');<br>localStorage.removeItem(name);<br>localStorage.clear();//Delete all
|Session Storage|Session Based available only during the session, works per window or tab, Client Side Reading Only|sessionStorage.setItem('key', 'value');<br>var data = sessionStorage.getItem('key');|
|Cookie|Expiry depends on the setting and working per window or tab, Server and Client Side Reading||
