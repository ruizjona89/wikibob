# Function Interface Function<T, R> { R apply(T t); }

- predefined Functional Interface having one abstract method
- A method receives an input type (T) and returns a result type (R)
- Unlike Predicate which always returns boolean

```
import java.util.function.Function;

public class FunctionDemo {
  public static void main(String[] args) {

    // Function<T, R>
    Function<Integer, Integer> squareMe = i -> i * i;
    System.out.println("Square of 5 is: " + squareMe.apply(5));
  }
}
```

## Functional Chaining

- Combine (Chain) multiple functions together with "andThen"
- function1.andThen(function2).apply(input);  // execute function1 first and then function2
- function1.compose(function2).apply(input);  // first function2 and then function1
- f1.andThen(f2).andThen(f3).apply(inputs);   // multiple functions


