
# MySQL / MariaDB
MySQL Relational Database (RDBC) was an awesome database until Oracle purchased in 2010.  The pioneers and creators of the MySQL Relational Database (RDBC) did not like the direction or decisions for the future of MySQL.  Thus MariaDB was developed.

## Videos
- [JDBC Example Mac](jdbcExampleMac.mp4)
- [MariaDB setup](mariadb.mp4)
- [bind variables](https://youtu.be/MnISfllmK74)

## Installation

- [how to install MariaDB](https://linuxize.com/post/how-to-install-mariadb-on-ubuntu-18-04/)
- [default password to MariaDB](https://stackoverflow.com/questions/20270879/whats-the-default-password-of-mariadb-on-fedora)
    - quick answer:  leave blank
- [reset root password](https://www.digitalocean.com/community/tutorials/how-to-reset-your-mysql-or-mariadb-root-password)
- [SQLDeveloper connection to MySQL / MariaDB](https://alvinbunk.wordpress.com/2017/06/29/using-oracle-sql-developer-to-connect-to-mysqlmariadb-databases/)
- [MariaDB maven repository](https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client)
- [MySQL maven repository](https://mvnrepository.com/artifact/mysql/mysql-connector-java)
- HeidiSQL: sudo snap install heidisql-wine --beta

### Apache Installation

- sudo apt update
- sudo apt install apache2
- sudo gedit /etc/apache2/apache2.conf
     - last line append:  ServerName 127.0.0.1
- sudo apache2ctl configtest
     - output should be "Syntax OK"
- sudo systemctl restart apache2
     - verify Apache is working:  http://localhost
     - [Apache2 Ubuntu Default Page](apache2UbuntuDefaultPage.png)

### remove and reinstall MySQL 
```  
sudo apt-get remove --purge mysql*
sudo apt-get purge mysql*
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get remove dbconfig-mysql
sudo apt-get dist-upgrade
sudo apt-get install mysql-server
```

### set plugin to mysql_native_password and password to blank

mysql -u root -p
mysql> select user, host, plugin from mysql.user;
mysql> update user set plugin='mysql_native_password' where user='root';
mysql> alter user 'root'@'localhost' IDENTIFIED BY '';
mysql> flush privileges;
mysql> exit;
service mysql restart

### MySQL Workbench
sudo snap install mysql-workbench-community

#### TOOK ME FOREVER TO FIND THIS - disable the app armor
sudo snap connect mysql-workbench-community:password-manager-service :password-manager-service

### PHP working with MySQL
- sudo apt install php libapache2-mod-php php-mysql
- sudo gedit /etc/phpmyadmin/config.inc.php
     - uncomment two lines with the following:  ```$cfg['Servers'][$i]['AllowNoPassword'] = TRUE;```
- sudo systemctl restart apache2
- sudo systemctl status apache2
- apt-cache search php- | less
- sudo gedit /var/www/html/info.php
     - add test php code -> <?php phpinfo(); ?>
- sudo systemctl restart apache2
     - http://127.0.0.1/info.php
     - [PHP Info Test](phpInfo.png)
     
### phpMyAdmin Installation

- sudo apt install phpmyadmin
- Configure database for phpmyadmin with dbconfig-common? Yes
- enter password, <cr>
- sudo cp /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf
- sudo a2enconf phpmyadmin
- sudo service apache2 restart
- [http://127.0.0.1/phpmyadmin/](phpMyAdmin.png)

## Notes / Troubleshooting

| Description | Syntax |
| ----------- | ------ |
| Start MariaDB | sudo mysqld & (run in background) |
| Start MariaDB | sudo systemctl restart or start |
| MariaDB Status | sudo systemctl status mariadb |
| Login MariaDB | mysql -u root -p |
| Login MariaDB | mariadb -u root -p |
| Set Timezone | SET @@global.time_zone = '+00:00'; SET @@session.time_zone = '+00:00'; SELECT @@global.time_zone, @@session.time_zone; |

## Log Example

* 2020-07-16  7:59:13 0 [Note] mysqld (mysqld 10.4.13-MariaDB-1:10.4.13+maria~eoan-log) starting as process 62336 ...
* 2020-07-16  7:59:13 0 [Note] InnoDB: Using Linux native AIO
* 2020-07-16  7:59:13 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
* 2020-07-16  7:59:13 0 [Note] InnoDB: Uses event mutexes
* 2020-07-16  7:59:13 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
* 2020-07-16  7:59:13 0 [Note] InnoDB: Number of pools: 1
* 2020-07-16  7:59:13 0 [Note] InnoDB: Using SSE2 crc32 instructions
* 2020-07-16  7:59:13 0 [Note] mysqld: O_TMPFILE is not supported on /tmp (disabling future attempts)
* 2020-07-16  7:59:13 0 [Note] InnoDB: Initializing buffer pool, total size = 256M, instances = 1, chunk size = 128M
* 2020-07-16  7:59:13 0 [Note] InnoDB: Completed initialization of buffer pool
* 2020-07-16  7:59:13 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
* 2020-07-16  7:59:13 0 [Note] InnoDB: 128 out of 128 rollback segments are active.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Creating shared tablespace for temporary tables
* 2020-07-16  7:59:13 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
* 2020-07-16  7:59:13 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Waiting for purge to start
* 2020-07-16  7:59:13 0 [Note] InnoDB: 10.4.13 started; log sequence number 107969; transaction id 157
* 2020-07-16  7:59:13 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
* 2020-07-16  7:59:13 0 [Note] Plugin 'FEEDBACK' is disabled.
* 2020-07-16  7:59:13 0 [Note] InnoDB: Buffer pool(s) load completed at 200716  7:59:13
* 2020-07-16  7:59:13 0 [Note] Server socket created on IP: '127.0.0.1'.
* 2020-07-16  7:59:13 0 [Note] Reading of all Master_info entries succeeded
* 2020-07-16  7:59:13 0 [Note] Added new Master_info '' to hash table
* 2020-07-16  7:59:13 0 [Note] mysqld: ready for connections.
* Version: '10.4.13-MariaDB-1:10.4.13+maria~eoan-log'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  mariadb.org binary distribution
| Description | Syntax |
| ----------- | ------ |
| Install MariaDB | sudo apt install mariadb-server |
| Start MariaDB | sudo mariadb |
| Start HeidiSQL | /snap/bin/heidisql-wine & |

## Bind variable

* parsing
    - setting up the query, memory management, process creation, all the overhead, etc.  very expensive
    - is the query systematically correct
    - is the statement correct
        - columns, tables, grants, views, etc.
        - optimizer - the best approach to execute the query |

* bind variable
    - reuse the parsing
    - temporary variable as place holders, later replaced with values
    - utilize a query multiple times
        - sql - bind variable are defined with the := character
        - java - PreparedStatement, CallableStatement
        