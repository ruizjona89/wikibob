// https://www.geeksforgeeks.org/linear-search/
package collections.search;

public class LinearSearch {

	private static int array[] = { 2, 3, 5, 8, 13, 20, 40 };
	private static final int ELEMENT_2_FIND = 13;

	public static int search() {

		for (int i = 0; i < array.length; i++) {
			if (array[i] == ELEMENT_2_FIND)
				return i;
		}

		return -1;
	}

	public static void main(String[] args) {

		int result = search();

		if (result == -1)
			System.out.println("Element not found");
		else
			System.out.println("Element at index: " + result);
	}
}
