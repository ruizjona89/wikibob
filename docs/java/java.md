# ![](../../docs/icons/cv64us50x50.png) Java 

- [Java General Examples](https://gitlab.com/bobby.estey/wikibob/-/tree/master/java)
- [Maven Examples](https://gitlab.com/bobby.estey/java/-/tree/master/maven)
- [Java8](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/java/java8/java8.md)
- [JavaFX](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/java/javaFx/javafx.md)
- [Javalib](javalib.md)

# Acronyms / Glossary

|Term|Description|Reference|
|--|--|--|
|[Anonymous Class](https://docs.oracle.com/javase/tutorial/java/javaOO/anonymousclasses.html)|Declare and instantiate a class, simultaneously.  Local class without a name||
|Constructor Chaining|subclass calling this() itself or super() calling a superclass||
|Copy Constructor|clone() method||
|[equals hashcode](https://www.geeksforgeeks.org/equals-hashcode-methods-java/)|||
|[@FunctionalInterface](https://www.geeksforgeeks.org/functional-interfaces-java/)|one abstract method||
|[Inner Nested Static Class](https://www.geeksforgeeks.org/static-class-in-java/)|- Nested - class within class<br>- Inner (static) - Static Class<br>- 
Inner (non-static) - Inner Class||
|[Lambda](https://www.javatpoint.com/java-lambda-expressions)|One method interface using expressions (implementation of a functional interface)||
|[Polymorphism](https://www.w3schools.com/java/java_polymorphism.asp)|Classes that are related to each other by inheritance|Dog and Cat are both Animals|
|[Private Constructors](https://www.geeksforgeeks.org/private-constructors-and-singleton-classes-in-java/?ref=lbp)|Internal Constructor Chaining and Singleton Pattern||
|Serialization|Translating a Data Structure into a format that can be transmitted, e.g. bits over the network||
|[Static Block](https://www.geeksforgeeks.org/static-blocks-in-java/?ref=lbp)|Code executed once when the code is loaded into memory, e.g. static { code }||
|Wrapper Class|Use primitive types as Objects, e.g. instead of int -> Integer, double -> Double||
