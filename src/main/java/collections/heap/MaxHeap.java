// https://www.geeksforgeeks.org/max-heap-in-java/
package collections.heap;

public class MaxHeap<T> implements MaxHeapInterface<T> {
//data fields
	private T[] heap; // array of heap entries, ignore heap[0]
	private int lastIndex; // index of last entry
//	private boolean integrityOK = false;
	private static final int DEFAULT_CAPACITY = 25;
//	private static final int MAX_CAPACITY = 10000;
	private static String[] nameArray = new String[DEFAULT_CAPACITY];

//constructors
	public MaxHeap(String[] nameArray) { // for the add method
//if (initialCapacity == 0; rootIndex--) {
//reheap(rootIndex);
	}

	public MaxHeap() {
		this(nameArray);
	}

	@Override
	public void add(T newEntry) {
		// checkIntegrity();
		int newIndex = lastIndex + 1;
//		int parentIndex = newIndex / 2;
//		while ((parentIndex>0) && newEntry.compareTo(heap[parentIndex])>0){
//		heap[newIndex]=heap[parentIndex];
//		newIndex=parentIndex;
//		parentIndex=newIndex/2;
//		}
		heap[newIndex] = newEntry;
		lastIndex++;
		// ensureCapacity();
	}

	@Override
	public T removeMax() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T getMax() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return 0;
	}
}