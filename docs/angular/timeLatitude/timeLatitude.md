# Relationship between Time and Latitude

This page explains the relationship between Time and Latitude, including the history, tools and examples.

# Recordings
- [World Time Zone Map](https://youtu.be/fOHwOJ65h2g)
- [How to Compute Latitude](https://youtu.be/UGJuQUle9Ns)

# Excellent Links and Learn about Time
- [World Time Zone Map](https://www.worldtimezone.com/)
     - Look at the top of the [World Time Zone Map](https://www.worldtimezone.com/) and observe the UTC values.  Look at the bottom of the Map and observe what time zone you are located:
          - Eastern Time Zone is Romeo (R)
          - Central Time Zone is Sierra (S)
          - Mountain Time Zone is Tango (T)
          - Pacific Time Zone is Uniform (U)
          - Zulu (Z) Time Zone is the Coordinated Universal Time (UTC) / Greenwich Mean Time (GMT)
               - 0th degree on the earth
     - US Military uses 2 time zones.  The local time zone and Zulu (Z) time zone
          - Example 1:  A Ship is in the Bravo (B) UTC+2 time zone and the local time is 1300B (1PM)
               - the time would be 1300B which is 2 time zones ahead Zulu time equates to 1300B - 500 = 1100Z
               - 1300B = 1200A = 1100Z
          - Example 2:  A Train is in the Sierra (S) UTC-6 time zone and the local time is 1100S (11AM)
               - the time would be 1100S which is 6 time zones behind Zulu time equates 1100S + 600 = 1700Z  
          - Example 3:  A Ship is in the Hotel (H) UTC+8 time zone and the local time is 1600H (4PM)
               - the time would be 1600H which is 8 time zones ahead Zulu time equates 1600H - 800 = 0800Z
          
- [Eratosthenes](https://en.wikipedia.org/wiki/Eratosthenes) is Credited to be the first to compute the Circumference of the Earth

# History - Navigation (Latitude / Longitude)

- Let's do some history:
     - Why are World Navies so interested in Time (this is important past, present and future)
     - What is the primary mission of the [US Naval Observatory](https://www.usno.navy.mil/USNO)?
          - To Produce Positioning, Navigation and Timing for the Department of Defense
          - look in the upper right corner of the web page for the actual time
- The [Sextant](https://en.wikipedia.org/wiki/Sextant) was a tool used by Ships for navigation and can only calculate [Longitude](https://www.worldatlas.com/what-is-longitude.html) - the North / South Coordinates on the earth.   
     - How is the Latitude - the East / West Coordinates computed?
         - The combination of both a SEXTANT and a CLOCK.
- This is how to compute the Latitude with the SEXTANT and a CLOCK:
     - The earth is a sphere and spheres have 360 degrees
     - The earths circumference is approximately:
          - 24,901 miles at the equator
          - 24,859 miles at the poles
     - The earth rotates 24 hours in a day, thus there are 24 timezones, almost matches the circumference of the earth
     - 360 degrees / 24 hours = 15 degrees per timezone
     
## Example
- A Ship is going to travel from London, England which is UTC(0) to New York, New York, USA UTC-5.  New York is 5 time zones (5 hours) behind London time.
- Before leaving port, a Sextant measures the angle of the Sun, to make this easy to comprehend, let's say the Sun is straight up overhead 12 Noon (1200 Local Time).  A clock is synchronized and set to read 12 Noon.
- The ship leaves port and cruises for a couple days WEST and needs to know the Longitude and Latitude coordinates:
     - Longitude is easily computed with the Sextant alone, using the Sun or Stars and the horizon
     - Latitude requires both a Sextant and Clock.  The Ship waits for the Sun to be directly overhead as when leaving port, this is verified by the Sextant.  The next procedure is to read the time on the CLOCK.  The Clock reads 1400 (2PM), not 1200 Noon.  This means you are 2 hours (2 time zones) behind the base time zone (London Time).  When traveling WEST time zones are negative.  When traveling EAST the times zones are positive.  
- Thus in the past all measurements on a map of Longitude and Latitude were measured in we will use Omaha, Nebraska as an example.

|Standard|DEGREES (°)|MINUTES (')|SECONDS ('')|DIRECTION (N, E, S, W)|
|--|--|--|--|--|
|Old Longitude|41°|15'|25.7760''|N|
|Old Latitude|95°|59'|42.3672''|W|
|New Longitude|41.2565|||Direction - North of Equator (+) / South of Equator (-)|
|New Latitude|-95.9345|||Direction - West of UTC (-) / East of UTC (+)|

- Omaha is almost 96 degrees WEST of the Prime Meridian, 96 / 15 Degrees = 6.4 Time Zones

## Latitude / Longitude Examples

- The following are addresses you can use to test or go to the following site for your own personal use:

- [Get Lat Long from Address](https://www.latlong.net/convert-address-to-lat-long.html)
     - North and East are positive
     - South and West are negative

|City|State|Longitude|Latitude|
|--|--|--|--|
|Omaha|Nebraska|41.256538|-95.934502|
|Northglenn|Colorado|39.739235|-104.990250|
|Chula Vista|California|32.640072|-117.084038|
|Maricopa|Arizona|33.058105|-112.047646|
|Hayes|Virginia|37.298390|-76.439910|
|Subic Bay|Philippines|14.7954|120.2827|
