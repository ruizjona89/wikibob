# .npmrc Configuration

# Resources

- [Nexus Repos](https://blog.sonatype.com/using-nexus-3-as-your-repository-part-2-npm-packages)
- [Encode Software](https://www.base64encode.org/)

# Process

- Navigate to the [Encode Software Site](https://www.base64encode.org/)
     1. Enter your Nexus username and password in the top text field
     2. Press Encode
     3. Copy the Results, e.g. Ym9iYnk6cGFzc3dvcmQ=

![Encode Software UI](encode.png)

- Create the .npmrc file in the root folder of a react project, e.g.  /home/git/reactProject/.npmrc
- Edit the .npmrc file and add the following 2 lines:
     - registry=the Nexus server URL, e.g. http://example.nexus.com/repository/
     - _auth=<paste the Results from the Encode Software Site>, e.g. the results from the Encoding Software, Ym9iYnk6cGFzc3dvcmQ=
     
     ```
     registry=http://example.nexus.com/repository/
     _auth=Ym9iYnk6cGFzc3dvcmQ=
     ```
