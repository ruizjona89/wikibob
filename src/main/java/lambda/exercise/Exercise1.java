package lambda.exercise;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercise1 {

	public static void main(String[] args) {

		List<Person> people = new ArrayList<>();
		people.add(new Person("Bobby", "Class", 12));
		people.add(new Person("Ester", "Class", 13));
		people.add(new Person("Rebby", "Class", 14));
		people.add(new Person("Class", "Bobby", 22));
		people.add(new Person("Class", "Ester", 23));
		people.add(new Person("Class", "Rebby", 24));

		// Sort List by lastname - 7 to 8
//		Collections.sort(people, new Comparator<Person>() {
//			@Override
//			public int compare(Person p1, Person p2) {
//				return p1.getLastName().compareTo(p2.getLastName());
//			}
//		});
//		
		Collections.sort(people, (person1, person2) -> person1.getLastName().compareTo(person2.getLastName()));

		// print all elements
		System.out.println("Print All Elements");
		printConditionally(people, p -> true);

		// print all people with last name beginning with C
		System.out.println("Print All Elements with Last Name beginning with C");
		printConditionally(people, p -> p.getLastName().startsWith("C"));

		// print all people with first name beginning with C
		System.out.println("Print All Elements with First Name beginning with C");
		printConditionally(people, p -> p.getFirstName().startsWith("C"));
	}

	private static void printConditionally(List<Person> people, Condition condition) {
		for (Person person : people) {
			if (condition.test(person)) {
				System.out.println(person);
			}
		}
	}

	interface Condition {
		boolean test(Person person);
	}
}
