/* The most
 * commonly used type parameter names are: E - Element (used extensively by the
 * Java Collections Framework) K - Key N - Number T - Type V - Value S,U,V etc.
 * - 2nd, 3rd, 4th types
 */
package generics;

public class Driver1 {

	public static void singleTypeParameter() {
		// create an Integer Type with value of 64
		Integer setInteger = Integer.valueOf(64);

		// create an Double Type with value of 64.64
		Double setDouble = Double.valueOf(64.64);

		// create a String Type with value Constellation
		String setString = new String("Constellation");

		// call Box Constructor and and define the Box type as Integer or Double
		Box<Integer> integerBox = new Box<>(64);
		Box<Double> doubleBox = new Box<>(64.64);

		// set the Type with value in the Box Class
		integerBox.set(setInteger);
		doubleBox.set(setDouble);

		// get the Type from the Box Class
		Integer getInteger = integerBox.get();
		Double getDouble = doubleBox.get();

		// print the Integer type
		System.out.println("Driver - getInteger:  " + getInteger);

		// print the Double type
		System.out.println("Driver - getDouble:  " + getDouble);

		// same Box Type but using a String Type
		Box<String> stringBox = new Box<>("Connie");
		stringBox.set(setString);
		String getString = stringBox.get();
		System.out.println("Driver - getString: " + getString);
	}

	public static void multipleTypeParameters() {
		Pair<String, Integer> pair1 = new OrderedPair<String, Integer>("CV", 64);
		Pair<String, String> pair2 = new OrderedPair<String, String>("USS", "Constellation");
		OrderedPair<String, Box<Integer>> pair3 = new OrderedPair<>("CV", new Box<Integer>(64));

		System.out.println("pair1.key: " + pair1.getKey() + " pair1.value: " + pair1.getValue());
		System.out.println("pair2.key: " + pair2.getKey() + " pair2.value: " + pair2.getValue());
		System.out.println("pair3.key: " + pair3.getKey() + " pair3.value: " + pair3.getValue().get());
	}

	public static <T> void main(String[] args) {

		singleTypeParameter();
		multipleTypeParameters();
	}
}
