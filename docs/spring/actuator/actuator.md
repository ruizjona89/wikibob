# Spring Boot Actuator

# Recordings

- [Actuator](https://www.youtube.com/watch?v=ojc_Jy_0EgI)

# Dependency
```
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```