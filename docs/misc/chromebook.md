# Chromebook

## How to disable/turn off touchpad Chromebook

- chrome://flags/#ash-debug-shortcuts
- Shift Search P (doesn't work, just go to settings)
- Settings -> Device -> Mouse and touchpad 
- Enable tap-to-click -- off
- Enable tap dragging -- off
- Enable touchpad acceleration -- off
- Touchpad speed - Slow
