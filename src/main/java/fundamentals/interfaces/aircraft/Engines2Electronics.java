package fundamentals.interfaces.aircraft;

public interface Engines2Electronics {

	public int getSpeed();
	public void setSpeed(int speed);
}
