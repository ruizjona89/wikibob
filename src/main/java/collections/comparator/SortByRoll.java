package collections.comparator;

import java.util.Comparator;

public class SortByRoll implements Comparator<Student> {

	// Comparator is simply (-1 less than 0 equal 1 greater than)
	// Note: simple math
	public int compare(Student studentA, Student studentB) {
		return studentA.rollno - studentB.rollno;
	}
}
