package collections.comparator;

import java.util.Comparator;

public class ColumnComparator implements Comparator<Object> {
	int columnToSort = 0;

	ColumnComparator(int columnToSort) {
		this.columnToSort = columnToSort;
	}

	// overriding compare method
	// compare the columns to sort
	// all this is returning
	// -1 if the first row is less than the second row (switch)
	// 0 if both rows are equal (no switch)
	// 1 if the first row is greater than the second row (no switch)
	public int compare(Object object2, Object object1) {
		String[] row1 = (String[]) object1;
		String[] row2 = (String[]) object2;

		int result = row2[columnToSort].compareTo(row1[columnToSort]);

		System.out.println("result: " + result + " row1: " + row1[1] + " row2: " + row2[1]);
		return result;
	}
}