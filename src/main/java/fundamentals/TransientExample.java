package fundamentals;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class TransientExample implements Serializable {

	private static final long serialVersionUID = 1L;
	int i = 10;
	int j = 20;

	transient int k = 30;

	// Transient doesn't work with static or final
	transient static int l = 40;
	transient final int m = 50;

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {
		TransientExample input = new TransientExample();

		// serialization
		FileOutputStream fileOutputStream = new FileOutputStream("abc.txt");
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		objectOutputStream.writeObject(input);

		// de-serialization
		FileInputStream fileInputStream = new FileInputStream("abc.txt");
		ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
		TransientExample transientExample = (TransientExample) objectInputStream.readObject();
		System.out.println("i = " + transientExample.i);
		System.out.println("j = " + transientExample.j);
		System.out.println("k = " + transientExample.k);
		System.out.println("l = " + TransientExample.l);
		System.out.println("m = " + transientExample.m);
	}
}