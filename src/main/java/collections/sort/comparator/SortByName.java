package collections.sort.comparator;

import java.util.Comparator;

public class SortByName implements Comparator<Person> {

	// Used to sort by name - compareTo method that compares the name
	// person.name of the Person (two Classes being passed in of Person) that is
	// being passed in to the Class (this) and return
	public int compare(Person person1, Person person2) {

		// return negative or positive or zero
		return person1.name.compareTo(person2.name);
	}
}
