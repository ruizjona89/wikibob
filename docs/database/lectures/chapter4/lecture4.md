<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Enterprise Data</title>
  <meta http-equiv="content-type"
 content="text/html; charset=ISO-8859-1">
</head>
<body>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><big><big><b>Enterprise
Data<br>
</b></big></big></div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
This week we will discuss how databases started from teleprocessor
technology to client / server and distributed architectures.
&nbsp;The&nbsp;</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">
On-Line Analytical Processing (OLAP), taking a view at multidimensional
queries. &nbsp;Data Warehouses, how an organization maintains their
intangible asset (information) and Data Marts, subsets of Data
Warehouses.</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> &nbsp;The
emphasis on this lecture is
to relate how database access has changed over the decades due to
network and computing technologies.<br>
<br>
<b>Teleprocessor Systems</b></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<br>
Teleprocessing was a very popular method of acquiring data for
several decades. &nbsp;Computing systems were so expensive even in the
1980's that companies would have a single computer, containing the
central
database and monitors that would directly connect to the system.
&nbsp;Actual computer hardware costs that I was first experienced with
in 1985 are shown in <a href="#table1">Table 1</a>.&nbsp; Later in the
1980's, terminal servers could make connections to this single
database. &nbsp;The Local Area Transport Terminal (LAT) Server was just
a Multiplexer (MUX) that terminals could connect directly and then the
Terminal Server had a network connection to the computer. &nbsp;A
Terminal Server would extend the distance of the monitor to the
computer, previously monitors were limited by the RS-232
specifications. &nbsp;Distributed databases still weren't an option for
several more years. &nbsp;Teleprocessor systems are defined as one
computer
with a database that everyone accesses through a monitor.<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><br>
<table border="1" cellpadding="2" cellspacing="2" width="400">
  <tbody>
    <tr>
      <td valign="top">Digital
Equipment<br>
VAX 11/780 (1 MIPS) Computer<br>
4MB Memory<br>
120MB Disk Drive<br>
      </td>
      <td valign="top">$0.7
Million<br>
      </td>
    </tr>
    <tr>
      <td valign="top">VT220
Terminal<br>
      </td>
      <td align="left" valign="top">$500 each<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Local
Area Transport (LAT) Terminal
Server<br>
      </td>
      <td valign="top">$1000
each<br>
      </td>
    </tr>
  </tbody>
</table>
</div>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><b><a
 name="table1"></a>Table 1 - Actual Cost of Equipment 1985</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
<b>File Sharing<br>
</b><br>
File Sharing is simply a server transferring all the required files to
perform operations including any data, e.g. database to requesting
systems (clients). &nbsp;The server executes none of the application,
just servers the clients with the appropriate files. &nbsp;The client
is using all local&nbsp; resources performing operations on the
database.
&nbsp;Just because this is an old architecture doesn't mean file
sharing
is obsolete. &nbsp;File sharing can be beneficial, e.g. server to
server
processing. &nbsp;Let's say that you have multiple web pages. &nbsp;A
one
time download from the primary server to a secondary server is all the
network
bandwidth that is required for that period of time. &nbsp;This keeps
the
master server from having to perform all the operations of the
corporate
business.<br>
<br>
<b>Client / Server</b><br>
<br>
Client / Servers became popular as networking technology was becoming
an affordable alternative. &nbsp;Client / Server means you have at
least two computing systems (one client / one server). &nbsp;The client
system is a standalone system that is perfectly capable of processing
user requests, usually a microprocessor. &nbsp;What makes client /
server a popular alternative is the server another computer with a
centralized database and operational programs can fulfill requests from
client systems. &nbsp;With the personal computers becoming very popular
many companies databases were becoming decentralized and the original
Information Systems (IS) departments were slowing loosing ownership
over the data. &nbsp;A lot of the Security, Services and other IS tasks
were no long being controlled by the IS department. &nbsp;The data was
also transitioning to personal systems and the client system usually
never made updates to the central database. &nbsp;With client / server
technology this help alleviate these problems by letting the client
systems perform their operations but making sure that the modified data
was sent back to the central computer known as the server. &nbsp; Also
client systems could download the current database records or perform
queries of
the database through the server.<br>
<br>
<b>Distributed Databases</b><br>
<br>
Distributed databases are simply a database being partitioned either
through a system or systems. &nbsp;There are four configurations to
partitioning a database. &nbsp;The partitions are are broken up by the
letters in the alphabet to illustrate how the database is being
partitioned, e.g.&nbsp; </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">The first quarter of
the database,
letters A through G records are logically partitioned as the first
partition, e.g. databaseAG. &nbsp;The next quarter of the database,
letters H through M are partitioned in database as databaseHM and so
on, </font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">see <a href="#figure1">Figure 1</a>.<br>
</font>
<ol style="font-family: helvetica,arial,sans-serif;">
  <li>Non-Partitioned /
Non-Replicated, the database is a single entity by the type of data and
accessed by a single server.</li>
  <li>Partitioned /
Non-Replicated, the database is actually segregated, physically
(subset) or logically (record type).&nbsp; Records A through M are
now being served by a single Server, Server A. &nbsp;The remaining
records are being served by Server B.<br>
  </li>
  <li>Non-Partitioned /
Replicated, the database is has been replicated (shadowed, mirrored)
and two or more systems are severing the data.</li>
  <li>Partitioned /
Replicated, parts of the database have been replicated, note:&nbsp;
That server A and B both replicate records N through T.</li>
</ol>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><img
 src="distributedTypes.gif" alt="" height="1059" width="379"> <br>
<b><a name="figure1"></a>Figure
1 - Partition Configurations<br>
</b>
<div style="text-align: left;">
<table style="text-align: left; width: 100%;" border="1" cellpadding="2"
 cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;"><br>
      </td>
      <td style="vertical-align: top;">Non-Partitioned / Non-Replicated<br>
      </td>
      <td style="vertical-align: top;">Partitioned / Non - Replicated<br>
      </td>
      <td style="vertical-align: top;">Non - Partitioned / Replicated</td>
      <td style="vertical-align: top;">Partitioned / Replicated</td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Advantages </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small>System Management Easiest</small></li>
        <li><small>Lowest Cost (HW &amp; SW)<br>
          </small></li>
        <li><small>No Compatibility Issues</small></li>
        <li><small>Database simplicity<br>
          </small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small><small><big>Increased I/O Performance</big></small></small></li>
        <li><small><small><big>Increased Reliability through Software<br>
          </big></small></small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small><small><big>Increased I/O Performance</big></small></small></li>
        <li><small><small><big>Highest Reliability<br>
          </big></small></small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small><small><big>Highest Performance</big></small></small></li>
        <li><small><small><big>Highest Reliability<br>
          </big></small></small></li>
      </ul>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;">Disadvantages<br>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small>Lowest Performance</small></li>
        <li><small>Single Point of Failure<br>
          </small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small>Increased Software Cost</small></li>
        <li><small>Increased Database Complexity<br>
          </small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small>Increased Hardware Cost</small></li>
      </ul>
      </td>
      <td style="vertical-align: top;">
      <ul>
        <li><small>Highest Cost (HW &amp; SW)<br>
          </small></li>
        <li><small>Database Complexity</small><br>
        </li>
      </ul>
      </td>
    </tr>
  </tbody>
</table>
</div>
</div>
<ol style="font-family: helvetica,arial,sans-serif;">
</ol>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Distributed
Databases
using File Sharing</b><br>
<br>
Sometimes File Sharing can be beneficial for a business. &nbsp;Using
file sharing principals a user is closer to the data. &nbsp;The primary
server can download several secondary servers or clients with several
databases. &nbsp;The data can then be used for a given time period
before
a new download is required. &nbsp;The benefit using this process is
user
programs can use the database specific for their operations. &nbsp;The
office in the Western Section retrieves only Western data and doesn't
need
additional resources that are not required, e.g. disk drivespace with
Eastern
data. &nbsp;The Western section can also update the records locally.
&nbsp;The problem is now apparent. &nbsp;The data could cause
corruption when transitioned back the Master Server database as updates.<br>
<br>
There are three processes to &nbsp;consider when updating a distributed
database from a local database. &nbsp;Coordination, means what
processes are to be followed to ensure that the database is updated, by
whom or resource. &nbsp;Consistency, making changes on the local server
that won't effect the distribution server. &nbsp;Access Control, who
has access to the various parts of the database. &nbsp;Sensitive
(either corporate or personal) data should be only accessible to the
correct individuals and systems. &nbsp;Computer fraud, is just that.
&nbsp;Anything you can use against an organization or individual,
unfair advantage to a competitor, theft, etc. &nbsp;With the
convenience of the Internet, all these steps are much harder to
maintain security.<br>
<br>
<b>On-Line Analytical Processing (OLAP)</b><br>
<br>
On-Line Analytical Processing (OLAP) is combining several different
points-of-view of data from a single entity. &nbsp;The best way to
describe the many views is dimensions. &nbsp;A relational database
table can be thought of as two-dimensional container. &nbsp;Rows
(records) are the first dimension. &nbsp;Columns (fields) are the
second dimension, see <a href="#figure2a">Figure 2a</a>. &nbsp;To
retrieve information from the database
a complex query is required because there are only two dimensions to
filter
the data, e.g. <b>state</b> and <b>year</b>. &nbsp;After the first
query,
secondary queries are required on the subset, known as a join.
&nbsp;Queries are much harder in the relational database than OLAP
which we will see in a minute.<br>
<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><img src="OLAP1.gif" alt=""
 height="721" width="961"> <br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><a
 name="figure2a"></a><b>Figure 2a - OLAP Hypercube Example</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">OLAP while having
additional reserved words, e.g. CROSSJOIN, DIMENSION, LEVEL, actually
allows easier extraction of data. &nbsp;The two dimensional relational
database table in <a href="#figure2a">Figure 2a</a> has been
transitioned to an OLAP format in <a href="#figure2b">Figure 2b</a>.
&nbsp;We can now see the multidimensional views of the <b>state, year,
party, sex</b> where previously we could only see two dimensions. &nbsp;<a
 href="#table2">Table 2</a> illustrates some of the OLAP terminology.<br>
</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><img src="OLAP2.gif" alt=""
 height="625" width="927"> <br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><b><a
 name="figure2b"></a>Figure 2b - OLAP Hypercube Example<br>
</b><br>
</div>
<table style="font-family: helvetica,arial,sans-serif;" border="1"
 cellpadding="2" cellspacing="2" width="100%">
  <tbody>
    <tr>
      <td valign="top">Terminology<br>
      </td>
      <td valign="top">Description<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Axis<br>
      </td>
      <td valign="top">Actual
coordinates in the Hypercube<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Dimension<br>
      </td>
      <td valign="top">Think
of these as sheets. &nbsp;Our example only had one dimension (sheet) of
congressional parties. &nbsp;We could have broken up the data into 51
dimensions for each State and Washington, DC.<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Level<br>
      </td>
      <td valign="top">Hierarchical&nbsp;subset
of a dimension, e.g. {State, Party, Incumbent, Sex, Decade}<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Member<br>
      </td>
      <td valign="top">Data
value in a dimension.<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Measure<br>
      </td>
      <td valign="top">Source
data for the hypercube or the cells of the cube, getCount.<br>
      </td>
    </tr>
    <tr>
      <td valign="top">Slice<br>
      </td>
      <td valign="top">Dimension
/ Measure that when viewed renders data for that particular subject.<br>
      </td>
    </tr>
  </tbody>
</table>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><a
 name="table2"></a><b>Table 2 - OLAP Terminology<br>
<br>
</b>
<div align="left"><a
 href="file:///C:/estey/bob/secure/classes/uop/dbm405/courseMaterials/week4/week4.html#table3">Table
3</a> shows how to define the OLAP Hypercube for <a
 href="file:///C:/estey/bob/secure/classes/uop/dbm405/courseMaterials/week4/week4.html#figure2b">Figure
2b</a>. &nbsp;The syntax is very similar to SQL just a few new key
words. &nbsp;The following syntax is creating the Hypercube
congressionalCube, with the row dimension, location and incumbentSex
dimension. &nbsp;The levels are further breakdowns of the dimensions
and finally the data is getCount. &nbsp;The number that is in
correspondence to that particular measure.<br>
<br>
</div>
</div>
<table style="font-family: helvetica,arial,sans-serif;" align="center"
 border="1" cellpadding="2" cellspacing="2" width="300">
  <tbody>
    <tr>
      <td valign="top">create
cube congressionalCube (<br>
&nbsp;&nbsp;&nbsp; dimension location type location,<br>
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;level state type state,<br>
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;level party type party,<br>
&nbsp;&nbsp;&nbsp; dimension incumbentSex type incumbantSex,<br>
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; level incumbent type boolean,<br>
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; level sex type boolean,<br>
&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; level decade type int,<br>
&nbsp;&nbsp;&nbsp; measure getCount type number,<br>
      <div align="left"> )<br>
      </div>
      </td>
    </tr>
  </tbody>
</table>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><b><a
 name="table3"></a>Table 3 - OLAP Hypercube Creation</b><br>
<br>
<div align="left">Finally, we
can do a select statement with OLAP. &nbsp;Again the syntax is very
similar to SQL with a few exceptions. &nbsp;The CROSSJOIN command is
what give OLAP the multidimensional views. &nbsp;The select statement
will compare all the data of the hypercube and select all Males from
Alaska that are Republican and Females in 1990 that are Republican from
Alaska, see <a href="#table4a">Table 4a</a>. &nbsp;Now try and do this
in standard SQL. &nbsp;A LOT HARDER, <a href="#table4b">Table 4b</a>.&nbsp;
By the way notice that the SQL select statement still hasn't pulled out
the column information for the Republicans.<br>
<br>
</div>
</div>
<div style="font-family: helvetica,arial,sans-serif;" align="center">
<table border="1" cellpadding="2" cellspacing="2" width="500">
  <tbody>
    <tr>
      <td valign="top">select
crossjoin ({incumbent,nonIncumbant} ,{Male,Female}) <br>
on columns, {AK.R} on rows from congressionalCube <br>
where (male = 'true', male = 'false' and year = 1990);<br>
      </td>
    </tr>
  </tbody>
</table>
<a name="table4a"></a><b>Table
4a - OLAP Select Statement<br>
</b>
<table style="text-align: left; width: 50%;" border="1" cellpadding="2"
 cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;">select i.sex, u.sex, i.year,
u.year, i.state, u.state from incumbant i, unincumbant u where
((i.state = 'AK' or u.state = 'AK') and ((i.male = 'true' or u.male =
'true') or ((i.male &lt;&gt; 'true' and i.year = '1990') or (u.mail =
'false' and u.year = '1990'))));</td>
    </tr>
  </tbody>
</table>
<a name="table4b"></a><b>Table
4b - SQL Select Statement</b>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b></b></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b><br>
Data Warehouses</b><br>
<br>
A data warehouse is a central repository for an enterprise's data
(business, educational, military, etc.) and is not a new term.
&nbsp;When
computing systems were first operational in an enterprise there was
just
one computer and one database. &nbsp;The department custodian of all
the
software and hardware was known as the Information Systems (IS)
department
which had a central staff that supported, programmed, input records and
produced reports for the various management levels. &nbsp;The various
levels
would make decisions based on the data that pertained to their
interest.
&nbsp;IS offered these services as charges to their particular.
&nbsp;The
problem was this central database was distant from the actual scope of
the unit performing the work, see <a href="#figure3a">Figure 3a</a>.<br>
<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><img
 src="architecture1.gif" alt="" height="398" width="681"> <br>
<b><a name="figure3a"></a>Figure
3a - Prior to Networks Database Architectures</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
Technology advanced and computing resources became cheaper for the
other departments to sever the umbilical cord from IS. &nbsp;This also
severed communications from the lower organization levels from the
higher levels of business. &nbsp;A department's scope of operations was
local while IS was
global. &nbsp;The data that once use to information the central
database was
slowly eroding. &nbsp;The departments were now using their own local
data
and receiving reports from their local database, see Figure 3b.<br>
<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><img
 src="architecture2.gif" alt="" height="436" width="738"> <br>
<b><a name="figure3b"></a>Figure
3b - Networks Database Architectures with Department Scope</b><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
</font> <font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Newer technologies
that were discussed in lecture 3 are enabling both the central database
and local databases cohesion where everyone has access to the correct
data.
&nbsp;</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Data
Warehousing,
was back and very much alive.</font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">, see Figure 3c.
&nbsp;An
organization
can have single or multiple data warehouses, depending on the
complexity
and size. &nbsp;The following are some Data Warehouse Components:<br>
<br>
</font>
<div style="font-family: helvetica,arial,sans-serif;" align="center"><b><img
 src="architecture3.gif" alt="" height="388" width="719"> </b><br>
<b><a name="figure3c"></a>Figure
3c - Data Warehousing</b><br>
</div>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li>Data Extraction Tools -
application programs that use the server interfaces usually providing a
GUI so that the user can produce queries / reports or have reports
automated.</li>
  <li>ODBMS / OLAP Server
-&nbsp;standard interfaces provided by the warehouse for the users to
add, change, delete or report on the data.</li>
  <li>Database Management
Tools - applications to manage the system, e.g. user accounts,
privileges, indexing, table creation, etc.</li>
</ul>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">While Data Warehouses
can be
an excellent resource for an organization, the Data Warehouse must
be thoroughly designed. &nbsp; Most companies when planning the Data
Warehouse will bring in outside consultants who understand Data
Warehousing technology and internal personnel who understand the
business. &nbsp;Some things
to consider:<br>
</font>
<ul style="font-family: helvetica,arial,sans-serif;">
  <li>Inconsistent data
can be detrimental to the organization because your not working with a
single specific database but the enterprise data. &nbsp;If the Data
Warehouse
takes inputs from several sources, the data needs to be accurate or the
possibility of data inconsistency, corruption could be cascaded
throughout
the system.</li>
  <li>Tool Integration is
making sure that all the different software components that interact
with Data Warehouse work and most of all communicate with no defects.
&nbsp;There are several different types of data with different
structures, syntax and schemas. &nbsp;Making sure that the external
components send and receive data correctly is crucial to enterprise
computing.<br>
  </li>
</ul>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Data Mart</b><br>
</font> <font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><br>
Some Data Warehouses can become quite large, very complex and very
hard to manage. &nbsp;Data Marts are simply a Data Warehouse that has
been
specifically subdivided into manageable segments. &nbsp;Data Marts are
usually broken down by either the type of data, functions,
organizational,
geographical, etc. or a combination that the particular organization is
focused.<br>
<a href="http://www.cio.com/archive/070197/gartner.html"><br
 style="font-weight: bold;">
</a></font>
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font></div>
<a href="http://www.cio.com/archive/070197/gartner.html"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><span style="font-weight: bold;">Comparison
between Data Warehouses and Data
Marts</span></font></a><br>
<center>
<div style="text-align: center;"><br>
</div>
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b> Summary</b></font><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font>
<div style="text-align: left;"><br>
</div>
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> This weeks
discussion
was on the history of enterprise database processing, e.g.
teleprocessing to distributed database processing. &nbsp;The </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">On-Line Analytical
Processing
(OLAP)</font><font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"> viewing
databases from several dimensions. &nbsp;Data Warehouses, although this
is not a new technology the creation, management and retrieval of
information are most complex. &nbsp;Finally, the data mart is simply
breaking down the Data Warehouse hierarchy to simplify the management
of the enterprise's data.</font><br>
</div>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font>
<div style="text-align: left;"></div>
<hr
 style="font-family: helvetica,arial,sans-serif; margin-left: 0px; margin-right: auto;"
 size="2" width="100%">
<div style="text-align: left;"></div>
<p style="font-family: helvetica,arial,sans-serif; text-align: left;"><b>
Discussion
Questions:</b>
&nbsp;Please read the
Discussion Question appendix in the syllabus.<br>
</p>
<div style="text-align: left;"></div>
<p style="font-family: helvetica,arial,sans-serif; text-align: left;">
Discussion
Question 1:
&nbsp;Discuss the differences between the data technologies, advantages
and disadvantages and any examples, e.g. teleprocessor, file sharing,
client / server and distributed databases.<br>
</p>
<div style="text-align: left;"></div>
<p style="font-family: helvetica,arial,sans-serif; text-align: left;">Discussion
Question
2:
&nbsp;Diagram (an illustration is required here) a simple OLAP design
and discuss. &nbsp;When discussing, explain what each of these are and
explain where they are locacted in your hypercube: &nbsp;<b>Axis,
Dimension, Level, Member, Measure,
Slice</b><br>
</p>
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Discussion Question
3:
&nbsp;Discuss the role of Data Warehouses and Data Marts.</font>
</div>
<hr
 style="font-family: helvetica,arial,sans-serif; margin-left: 0px; margin-right: auto;"
 size="2" width="100%">
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b>Individual
Assignment</b></font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Write a 2 - 3 page
paper (at least 300 words per page, </font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><span style="font-weight: bold;">Arial
Font, Size 10</span></font><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">)
that explains in detail
the topic of discussion. &nbsp;<b>Make sure your resources are declared.</b></font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif">Find resource(s),
e.g. websites that discusses how to design
an architecture with OLAP and Data Warehouse or how a particular
company
has implemented both of these technologies. &nbsp;You do not have to
have
the actual schemas. &nbsp;After reviewing these resources, write up how
you would implement a Data Warehouse and what you would do differently.
&nbsp;Include how the hardware would be configured. <b>&nbsp;You can
use
what was discussed in the class.</b></font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><b></b></font>
</div>
<hr
 style="font-family: helvetica,arial,sans-serif; margin-left: 0px; margin-right: auto;"
 size="2" width="100%">
<div style="text-align: left;"></div>
<p style="font-family: helvetica,arial,sans-serif; text-align: left;"><big>References</big><br>
</p>
<div style="text-align: left;"><font
 style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a name="text"></a>David
M.
Kroenke, Database Processing: &nbsp;Fundamentals, Design &amp;
Implementation, Seventh Edition, 2000</font><br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"><a name="ado"></a><a
 href="http://whatis.com">whatis.com</a>,
http://whatis.com, Accessed: &nbsp;17 May 2003<br>
<a name="dwvsdm"></a><a
 href="http://www.cio.com/archive/070197/gartner.html">Data Warehouses
versus Data Marts</a>, http://www.cio.com/archive/070197/gartner.html,
Accessed:&nbsp; 17 March 2004</font><span
 style="font-family: helvetica,arial,sans-serif;"><br>
</span>
<hr style="width: 100%; height: 2px;"><span
 style="font-family: helvetica,arial,sans-serif;"></span><img
 src="cv64.bmp" alt="" style="width: 32px; height: 32px;" align="left"><small><font
 face="Arial,Helvetica"><a href="mailto:information@cv64.us">Web
Contact</a></font></small><br>
<small><font face="Arial,Helvetica">
Last modified:&nbsp; 2004 March 17</font></small><br>
<br>
<font style="font-family: helvetica,arial,sans-serif;"
 face="Helvetica, Arial, sans-serif"></font> <br
 style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
<br style="font-family: helvetica,arial,sans-serif;">
</div>
<br style="font-family: helvetica,arial,sans-serif;">
</center>
</body>
</html>
