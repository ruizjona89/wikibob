	.Title	Undelete		"Undelete" a file

	.Library /sys$library:lib.mlb/

	$FCHDEF			;file characteristics definitions
	$FH2DEF			;file header definitions
	$FI2DEF			;file ident area definitions
	$FIBDEF			;file information block definitions
	$HM2DEF			;homeblock definitions

;----------------------------------------------------------------
;
;   Macros
;
;----------------------------------------------------------------

.Macro	Erjmp	Destination,?Labl
	Blbs	R0,Labl
	Jmp	Destination
Labl:
.Endm

;----------------------------------------------------------------
;
;   Storage
;
;----------------------------------------------------------------

	.Psect	Data

FilFAB:		$FAB	Fna=InputBuffer,FAC=<GET,PUT>, -
			SHR=<GET,PUT,UPD,DEL,UPI>,FOP=UFO

Channel:	.Blkl	1			;channel number of INDEXF
IOSB:		.Blkq	1			;I/O status block

InputBufDsc:	.Long	80			;input for disk name
		.Address InputBuffer
InputBuffer:	.Blkb	80			;must have room for filename

Filename:	.Ascii	/[000000]INDEXF.SYS/	;filename to open
Filenamelen:	.Long	.-Filename		;length of filename

Dirname:	.Ascii	/.DIR;1/		;directory file?
Dirnamelen:	.Long	.-Dirname

Prompt:		.Ascid	/Disk name (must include colon): /
WildPrompt:	.Ascid	/Enter wildcarded string for filename matching: /

UndelMsg:	.Ascid	-
	/  Should the above file be UNDELETED? (YES, NO, QUIT, ALL) : /
FailedEntryMsg:	.Ascid	-
	/  ?Failed to make directory entry...will be in [SYSLOST]/
DoneMsg:	.Ascid	/File has been undeleted./

WildDsc:	.Long	255			;buffer for file spec
		.Address Wild
Wild:		.Blkb	255

OutputbufDsc:	.Long	255			;output for constructed name
		.Address OutputBuffer
OutputBuffer:	.Blkb	255

FileBufDsc:	.Long	255			;file name buffer
		.Address FileBuffer
FileBuffer:	.Blkb	255

HeaderBase:	.Blkl	1			;base of headers
CurrentHeader:	.Blkl	1			;current header
MAXFILES:	.Blkl	1			;maximum number of files
AllFlag:	.Blkl	1			;flag for all undelete

FIBDSC:		.Long	FIB$C_LENGTH		;length of block
		.Address FIB
FIB:		.Blkb	FIB$C_LENGTH		;FIB

AnalyzePrompt:	.Ascid	~Should ANALYZE/DISK/REPAIR be run on the volume? ~
AnalyzeMessage:	.Ascid	~Running ANALYZE/DISK/REPAIR on the volume...~
AnalyzeDisk:	.Ascii	~ANALYZE/DISK/REPAIR ~
AnalyzeDisk2:	.Blkb	20
AnalyzeDiskDsc:	.Long	AnalyzeDisk2-AnalyzeDisk
		.Address AnalyzeDisk

QuestionDir:	.Ascii	/?/

	.Psect	Pages,Page

HeaderBuf:	.Blkb	512			;buffer for header
DirHeaderBuf:	.Blkb	512			;buffer for directory header

;----------------------------------------------------------------
;
;   Start of code
;
;----------------------------------------------------------------

	.psect  rick,exe,nowrt
	.Entry	Undelete,^M<>

;
;   Get the disk name
;

	Pushal	InputBufDsc			;where to put the length
	Pushal	Prompt				;what to say
	Pushal	InputBufDsc			;where to put the data
	CallS	#3,G^LIB$GET_INPUT		;get the disk name
	 Erjmp	Error

	Locc	#^A/:/,#80,InputBuffer		;did they give a colon?
	Bneq	GotColon			;yup
	Movl	InputBufDsc,R6			;get length
	Movb	#^A/:/,InputBuffer(R6)		;put in the colon
	Incl	InputBufDsc			;count it
GotColon:	
	Movl	AnalyzeDiskDsc,R6		;get length of AN/DSK command
	Movc3	InputBufDsc,InputBuffer,AnalyzeDisk(R6)	;move in disk name
	Addl2	InputBufDsc,AnalyzeDiskDsc	;get total length
;	Movl	AnalyzeDiskDsc,R6		;get length
;	Movb	#^A/"/,AnalyzeDisk(R6)		;enclose in quotes
;	Incl	AnalyzeDiskDsc			;and count it

;
;   Open the INDEXF.SYS file
;

	Moval	InputBuffer,R3			;get base of string
	Addw2	InputBufDsc,R3			;add in offset
	Movc3	FilenameLen,Filename,(R3)	;move in the file name
	Addb3	FilenameLen,InputBufDsc,FilFAB+FAB$B_FNS
						;put total length in FAB

	$Open		FilFAB			;open the file
	 Erjmp	Error
	Movl	FilFAB+FAB$L_STV,Channel	;keep the channel

;
;   we now have the file open.  Read the home block to find out
;	where the actual headers start.  Also get the maxfiles
;

	$Qiow_S		Chan=Channel, -		;the channel
			Func=#IO$_READVBLK, -	;the function
			IOSB=IOSB, -		;I/O status block
			P1=HeaderBuf, -		;where to put it
			P2=#512, -		;how much to get
			P3=#2			;VBN of homeblock
	 Erjmp	Error
	Movzwl	IOSB,R0
	 Erjmp	Error

;
;   we have the homeblock, extract the fields we want
;

	Movzwl		HeaderBuf+HM2$W_IBMAPVBN,HeaderBase
	Addw2		HeaderBuf+HM2$W_IBMAPSIZE,HeaderBase
	Movl		HeaderBase,CurrentHeader
	Decl		HeaderBase		;decrement for one based
	Movl		HeaderBuf+HM2$L_MAXFILES,MAXFILES

;----------------------------------------------------------------
;
;   Get the (possibly) wildcarded file specification for matching
;	on the found files.
;
;----------------------------------------------------------------

	Pushal	WildDsc			;where to put length
	Pushal	WildPrompt		;prompt to use
	Pushal	WildDsc			;where to put string
	Calls	#3,G^LIB$GET_INPUT	;get the string
	 Erjmp	Error

;  prepend and postpend an asterisk to the string

	Movzwl	WildDsc,R6		;get count
	Movc3	R6,Wild,Wild+1		;shift over 1
	Movb	#^A/*/,Wild		;prepend the asterisk
	Movb	#^A/*/,Wild+1(R6)	;postpend
	Addl2	#2,WildDsc		;make length correct

;  uppercase the string

	Pushal	WildDsc			;destination
	Pushal	WildDsc			;source
	CallS	#2,G^STR$UPCASE		;upper case it
	 Erjmp	Error

;----------------------------------------------------------------
;
;   Mainloop
;	Process the next header
;
;----------------------------------------------------------------

MainLoop:
	$Qiow_S		Chan=Channel, -		;channel
			Func=#IO$_READVBLK, -	;function code
			IOSB=IOSB, -		;I/O status block
			P1=HeaderBuf, -		;where to put it
			P2=#512, -		;how much to get
			P3=CurrentHeader	;where to get it from
	 Erjmp	EOFile				;assume END-OF-FILE on error
	Movzwl	IOSB,R0
	 Erjmp	EOFile

;
;   we have another header
;	check for MARKDEL bit set and SEG_NUM is zero
;	if either is false, skip this header
;

	Tstw		HeaderBuf+FH2$W_SEG_NUM	;zero segment number?
	Beql		ZeroSegment		;yup - check MARKDEL
	Jmp		NextHeader		;nope - skip it
ZeroSegment:
	Bitl		#FCH$M_MARKDEL,HeaderBuf+FH2$L_FILECHAR
	Bneq		FoundDeleted		;found one - process
	Jmp		NextHeader		;nope - skip it

;----------------------------------------------------------------
;
;   here we found a primary header that is deleted.  Build a file
;	name and ask if it should be undeleted.
;
;	the filename is constructed by grabbing the directory
;	names up the BACKLINK list until we reach (4,4,0), which 
;	is the MFD.
;
;----------------------------------------------------------------

FoundDeleted:
	Movc5	#0,#0,#0,#255,OutputBuffer	;clear the buffer
	Movzbl	HeaderBuf+FH2$B_IDOFFSET,R5	;get ID offset (in words)
	Ashl	#1,R5,R5			;get bytes
	Movl	R5,R6				;copy the base
	Addl2	#FI2$T_FILENAME,R6		;get offset of filename
	Addl2	#HeaderBuf,R6			;get address of filename
	Locc	#^A/ /,#20,(R6)			;find a space
	Subl3	R0,#20,R7			;get length of filename
	Movl	R7,R8				;save length so far
	Movc3	R7,(R6),OutputBuffer		;move in the filename
	Cmpl	R7,#20				;more to get?
	Blss	GotFileName			;nope

;   here we need to get the extension filename

	Subl2	#FI2$T_FILENAME,R6		;subtract original offset
	Addl2	#FI2$T_FILENAMEXT,R6		;add new offset
	Locc	#^A/ /,#66,(R6)			;find terminator
	Subl3	R0,#66,R5			;get new length
	Pushl	R5				;save new length
	Movc3	R5,(R6),OutputBuffer(R7)	;move the rest
	Popl	R5				;restore new length
	Addl2	R5,R8				;get total length

;
;   set up to make a directory entry for this file so it is not "lost"
;	the actual IO$_CREATE is done when the user says to undelete
;

GotFileName:
	Movc3	R8,Outputbuffer,FileBuffer	;move the name of the file
	Movl	R8,FileBufDsc			;make a descriptor
	Movc3	#6,Headerbuf+FH2$W_BACKLINK,FIB+FIB$W_DID
	Subl3	Headerbase,CurrentHeader,R10	;get file number
	Movw	R10,FIB+FIB$W_FID		;put in FIB
	Ashl	#-16,R10,R10			;shift out low-order
	Movb	R10,FIB+FIB$B_FID_NMX		;put high-order in FIB
	Movw	Headerbuf+FH2$W_FID_SEQ,FIB+FIB$W_FID_SEQ	;get sequence
	Movb	Headerbuf+FH2$W_FID_RVN,FIB+FIB$W_FID_RVN	;get RVN

;
;   now create the pathname to the file by getting directory names
;

	Movc3	R8,Outputbuffer,Outputbuffer+1	;move the string over
	Movb	#^A/]/,OutputBuffer		;move in directory terminator
	Incl	R8				;count it in

;----------------------------------------------------------------
;
;   directory name loop
;	move our header into DIRHEADERBUF so that we have common code
;	extract the first longword of the BACKLINK field
;	compare to ^X00040004
;	if so, exit
;	if not, grab that header
;	extract name and loop
;
;----------------------------------------------------------------

	Movc3	#512,HeaderBuf,DirHeaderBuf	;move our header
DirLoop:
	Movzwl	DirHeaderBuf+FH2$W_BACKLINK,R6	;get header number
	Addl2	HeaderBase,R6			;add in base

	$Qiow_S		Chan=Channel, -		;channel
			Func=#IO$_READVBLK, -	;function
			IOSB=IOSB, -		;I/O status block
			P1=DirHeaderBuf, -	;where to put data
			P2=#512, -		;how much to get
			P3=R6			;what to get
	 Erjmp	NextHeader		;bad backlink, ignore this file
	Movzwl	IOSB,R0
	 Erjmp	NextHeader		;bad backlink, ignore this file

;   we have the new header, put the name in

	Movzbl	DirHeaderBuf+FH2$B_IDOFFSET,R6	;get ID offset (in words)
	Ashl	#1,R6,R6			;get bytes
	Addl2	#FI2$T_FILENAME,R6		;get offset of filename
	Addl2	#DirHeaderBuf,R6		;get address of filename
	Locc	#^A/./,#20,(R6)			;find the period
	Subl3	R0,#20,R7			;get length
	Cmpl	R7,#20				;is it to long?

;
;   Directory header is no good, so just put a ? in for the
;	directory name and let them suffer
;

	Blss	GoodDirname			;good name
	Movc3	R8,OutputBuffer,OutputBuffer+2	;shift over what we have
	Movb	#^A/?/,OutputBuffer		;put in a ?
	Incl	R8				;make length correct
	Jmp	GotDirName			;just use the ?

GoodDirname:
	Movc3	R8,OutputBuffer,OutputBuffer(R7)	;move over what we have
	Movc3	R7,(R6),OutputBuffer		;move in the directory
	Addl2	R7,R8				;add in to total length

	Cmpl	DirHeaderBuf+FH2$W_BACKLINK,#^X00040004	;all done?
	Bneq	NextDirHeader			;nope - continue
	Jmp	GotDirName
NextDirHeader:
	Movc3	R8,OutputBuffer,OutputBuffer+1	;shift so far
	Movb	#^A/./,OutputBuffer		;move in the period
	Incl	R8				;count it in
	Jmp	DirLoop				;get the next one

;----------------------------------------------------------------
;
;   here we have the full pathname to the deleted file, so
;	ask them about it
;
;   options here are:  YES, ALL, QUIT, NO
;	YES - undelete the file
;	NO - don't undelete the file
;	ALL - do the rest of the files on the volume without prompt
;	QUIT - exit now
;
;----------------------------------------------------------------

GotDirname:
	Movc3	R8,OutputBuffer,OutputBuffer+1	;shift it over
	Movb	#^A/[/,OutputBuffer		;put in pre-terminator
	Addl3	#1,R8,OutputBufDsc		;put in length

;   check to see if this matches the wildcarded string input earlier

	Pushal	WildDsc				;descriptor of pattern
	Pushal	OutputBufDsc			;descriptor of candidate
	CallS	#2,G^STR$MATCH_WILD		;match wildcarded string
	Cmpl	R0,#STR$_MATCH			;does it match?
	Beql	GotOne				;yup - process it
	Jmp	NextHeader			;nope - skip it

GotOne:
	Pushal	OutputBufDsc			;what to output
	CallS	#1,G^LIB$PUT_OUTPUT		;output file name
	 Erjmp	Error

	Tstl	AllFlag				;doing them all
	Beql	PromptThem			;nope - prompt
	Jmp	UndeleteIt			;yup - go do it

PromptThem:
	Movl	#80,InputBufDsc			;set up length
	Pushal	InputBufDsc			;where to return length
	Pushal	UndelMsg			;what to say
	Pushal	InputBufDsc			;where to put the data
	CallS	#3,G^LIB$GET_INPUT		;get some input
	 Erjmp	Error

	Cmpb	InputBuffer,#^A/Y/		;say yes?
	Beql	UndeleteIt			;yup
	Cmpb	InputBuffer,#^A/y/		;how about it?
	Beql	UndeleteIt			;yup
	Cmpb	InputBuffer,#^A/A/		;say yes?
	Beql	DoThemAll			;yup
	Cmpb	InputBuffer,#^A/a/		;how about it?
	Beql	DoThemAll			;yup
	Cmpb	InputBuffer,#^A/Q/		;say yes?
	Beql	AllDoneAssist			;yup
	Cmpb	InputBuffer,#^A/q/		;how about it?
	Beql	AllDoneAssist			;yup
	Jmp	NextHeader			;nope - skip this one

AllDoneAssist:					;branch assist
	Jmp	AllDone
DoThemAll:
	Movl	#1,AllFlag			;mark them all

;----------------------------------------------------------------
;
;   here we want to undelete the file header and all it's extent
;	headers.  We do this by turning off the MARKDEL bit, and
;	then rechecksum the header (16-bit addition of header).
;	Also need to load the FID back into place in the header,
;	and create a directory entry for it.
;
;----------------------------------------------------------------

UndeleteIt:
	Movl	CurrentHeader,R10		;header to undelete
UndeleteExtent:
	Subl3	HeaderBase,R10,R9		;get FID
	Movw	R9,Headerbuf+FH2$W_FID_NUM	;move in low-order file number
	Ashl	#-16,R9,R9			;shift out low-order
	Movb	R9,Headerbuf+FH2$B_FID_NMX	;put in high-order part

	Bicl	#FCH$M_MARKDEL,Headerbuf+FH2$L_FILECHAR	;turn off the bit

;   check for a .DIR;1 in the file name.  If found, set the 
;	directory bit

	Matchc	Dirnamelen,Dirname,OutputBufDsc,OutputBuffer
	Bneq	NoDirectory			;nope - skip it
	Bisl	#FCH$M_DIRECTORY,Headerbuf+FH2$L_FILECHAR
NoDirectory:
	Clrw	Headerbuf+510			;clear checksum
	Movl	#255,R6				;word counter
	Clrl	R7
Checksumit:
	Addw2	Headerbuf[R6],R7		;add in the word
	Sobgeq	R6,Checksumit			;checksum the whole header
	Movw	R7,Headerbuf+510		;put it in

;
;   we have the new header, write it out
;

	$Qiow_S		Chan=Channel, -		;channel
			Func=#IO$_WRITEVBLK, -	;write function
			IOSB=IOSB, -		;I/O status block
			P1=Headerbuf, -		;where to write from
			P2=#512, -		;how much to write
			P3=R10			;where to write it to
	 Erjmp	Error
	Movzwl	IOSB,R0
	 Erjmp	Error

;
;   make the directory entry, if this is the first time through
;

	Cmpl		R10,CurrentHeader	;first time through?
	Beql		MakeEntry		;yup - make the entry
	Brw		NoEntry			;nope - skip it
MakeEntry:
	$Qiow_S		Chan=Channel, -		;channel to use
			Func=#IO$_CREATE, -	;create an entry
			IOSB=IOSB, -		;I/O status block
			P1=FIBDSC, -		;FIB descriptor
			P2=#FileBufDsc		;file name to enter
	 Erjmp	FailedEntry
	Movzwl	IOSB,R0
	 Erjmp	FailedEntry

;
;   check for extent headers
;

NoEntry:
	Movzwl	HeaderBuf+FH2$W_EXT_FID,R10	;anything there?
	Bneq	DoExtent			;grab the extent header
	Jmp	DoneHeader			;nope - go to next one

FailedEntry:
	Pushal	FailedEntryMsg
	CallS	#1,G^LIB$PUT_OUTPUT		;tell them it failed
	 Erjmp	Error
	Jmp	NoEntry				;and recover extent headers

DoExtent:
	Addl2	HeaderBase,R10			;calculate offset
	$Qiow_S		Chan=Channel, -		;channel to use
			Func=#IO$_READVBLK, -	;function code
			IOSB=IOSB, -		;I/O status block
			P1=Headerbuf, -		;where to put data
			P2=#512, -		;how much to get
			P3=R10			;new header
	 Erjmp	Error
	Movzwl	IOSB,R0
	 Erjmp	Error

	Jmp	UndeleteExtent			;and undelete it

DoneHeader:
	Pushal	DoneMsg				;tell them it's done
	CallS	#1,G^LIB$PUT_OUTPUT
	 Erjmp	Error

;	Brw	NextHeader			;go do the next one

;----------------------------------------------------------------
;
;   NEXTHEADER - do the next header
;
;----------------------------------------------------------------

NextHeader:
	Incl	CurrentHeader			;increment current header
	Cmpl	CurrentHeader,Maxfiles		;are we at the end?
	Bgtr	AllDone				;yup - exit
	Jmp	MainLoop			;nope - loop

;----------------------------------------------------------------
;
;   Alldone - close out the file
;
;----------------------------------------------------------------

EOFile:
AllDone:
	$Dassgn_S	Chan=Channel		;deassign the channel
	 Erjmp	Error

	Movl	#80,InputBufDsc			;set up length
	Pushal	InputBufDsc			;where to return length
	Pushal	AnalyzePrompt			;what to say
	Pushal	InputBufDsc			;where to put the data
	CallS	#3,G^LIB$GET_INPUT		;get some input
	 Erjmp	Error

	Cmpb	InputBuffer,#^A/Y/		;say yes?
	Beql	DoAnalyze
	Cmpb	InputBuffer,#^A/y/		;how about it?
	Beql	DoAnalyze			;yup
	Jmp	NoAnalyze

DoAnalyze:
	Pushal	AnalyzeMessage			;tell them about the ANALYZE
	CallS	#1,G^LIB$PUT_OUTPUT
	 Erjmp	Error

	Pushal	AnalyzeDiskDsc			;and do it
	CallS	#1,G^LIB$DO_COMMAND

NoAnalyze:
Error:	$Exit_S	R0

	.End	Undelete
