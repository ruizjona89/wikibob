# Asynchronous JavaScript and eXtensible Markup Language (XML) - AJAX

# Videos / Images

- [v1](https://www.youtube.com/watch?v=QDVg-hNBlbM)
- [AJAX Architecture](https://en.wikipedia.org/wiki/Ajax_(programming)#/media/File:Ajax-vergleich-en.svg)
- [AJAX](https://stackify.com/wp-content/uploads/2017/07/how-ajax-works-chart-12821.png)

# Overview

- XMLHttpRequest Object - exchanges data with the server
- JavaScript and HTML Document Object Model (DOM) - (process data)
- AJAX processes requests Asynchronously behind the scenes, threaded and only parts of the page are updated, not the entire page
- AJAX is not standalone (does not work independently)
- JavaScript 
     - Loosely Typed Script Language
     - Function is called when events occur
     - Programmatically change the DOM and CSS
     
- DOM
     - API for accessing and manipulating structured documents
     - Structure of HTML / XML documents

# XMLHttpRequest Object

```
function loadDocument() {
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {
    document.getElementById("demo").innerHTML = this.responseText;
  }
  xhttp.open(HTMLmethod, URL, asyncBoolean);  // xhttp.open("GET", "us.cv64/service/handshake", true);
  xhttp.send();  // GET
  xhttp.send(String);  // POST
}
```

# Process

- event occurs
- XMLHttpRequest object created
- XMLHttpRequest object configured
- XMLHttpRequest object makes asynchronous request to server
- Server returns the response containing XML document
- XMLHttpRequest object calls the callback() function and processes the response
- HTML DOM is updated

# AJAX Server Response
|Property|Description|
|--|--|
|onreadystatechange|function to be called when readyState property changes\
|readyState|status of the XMLHttpRequest, e.g. 0 - request not initialized, 1 - server connection established, 2 - request received, 3 - processing request, 4 - request finished and response is ready|
|status|HTML Status, e.g. 200 OK, 404 Page Not Found|
|statusText|return status string, e.g. OK, Not Found|
