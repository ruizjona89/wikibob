# Functional Interface
     - one abstract method
     - can have multiple static and default methods
     - providing a reference to a Lambda Express
     - BiConsumer is a Functional Interface providing a reference to a Lambda Expression
     - Comparable and Runnable are preexisting Functional Interfaces
     - Adding @FunctionalInterface adds additional checking, e.g. multiple abstract 
     methods
```
import java.util.function.BiConsumer;

// BiConsumer is the Functional Interface
// biConsumer is the reference to a Lambda Expression
// (a, b) -> System.out.println(a + b); is the Lambda Expression

BiConsumer<Integer, Integer> biConsumer = (a, b) -> System.out.println(a + b);
biConsumer.accept(10,5);

@Functional Interface
public Interface Runnable
public abstract void run();

```
## Create Functional Interface

```
@Functional Interface
public interface FunctionalInterfaceExample {
	void singleAbstractMethod();
	
	default void method1() {
		System.out.println("FunctionalInterfaceExample");
	}
	
	default void method2() {
		System.out.println("FunctionalInterfaceExample");
	}
```
