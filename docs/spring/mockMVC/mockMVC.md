# MockMvc - Spring Testing the Web Layer

# Links

- [Mock MVC](https://spring.io/guides/gs/testing-web/#initial)
- [Mock MVC Examples](https://howtodoinjava.com/spring-boot2/testing/spring-boot-mockmvc-example/)

# Testing Process

- Start Spring Boot Application as Spring Application
- Start Test Application as JUnit Test
     - within IDE ./mvn test or ./gradlew test

# Annotations

|Name|Description|
|--|--|
|@SpringBootTest|Locate the SpringBootApplication and start as the context|