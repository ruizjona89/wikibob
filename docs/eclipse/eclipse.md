# ![](../../docs/icons/cv64us50x50.png) eclipse Integrated Development Environment (IDE)

## video

- [eclipse installation video](eclipseInstallation.mp4)
- [eclipse installation video](https://www.youtube.com/watch?v=YQN4C1CmHcg)
- [eclipse basics - java and debugger perspectives](https://www.youtube.com/watch?v=LWOvS-fhQqE)

The video explains how to install the eclipse Integrated Development Environment (IDE) on a Linux Ubuntu Operating System.  The process is very similar to Windows and Mac installations.

 - Navigate to eclipse.org website [eclipse download][1]
 
 [1]: https://eclipse.org
 
 - Download the appropriate version
 	- this example is going to download the 64 bit linux version of eclipse.

 - Extract the compressed file and copy to the /opt/eclipse folder for Linux.  
 	If installing on Windows or Mac, DO NOT INSTALL in "Program Files".  Installing software in the
 	directory or filenames with spaces is a terrible practice and you will have issues with configurations.

Only install software with filenames and directories without spaces.

Linux:  /opt/eclipse
Windows:  c:\opt\eclipse

- To start up eclipse, execute the eclipse executable and the eclipse IDE starts up.

# Installation

- [eclipse Installation](images/eclipseInstallation.md)
