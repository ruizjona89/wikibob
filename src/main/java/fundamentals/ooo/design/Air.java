/**
 * Naval Air Forces - aircraft onboard ships flown by Naval Aviators (US Navy or US Marine Corps)
 * 
 * Air JavaBean - JavaBean is defined as attributes, Constructors and getters / setters defined
 */
package fundamentals.ooo.design;

public class Air {

	private String airwing = "";
	private String squadronDesignation = "";
	private String squadronName = "";
	private String aircraftType = "";

	public Air(String airwing, String squadronDesignation, String squadronName, String aircraftType) {

		this.airwing = airwing;
		this.squadronDesignation = squadronDesignation;
		this.squadronName = squadronName;
		this.aircraftType = aircraftType;
	}

	public Air() {
		this("n/a", "", "", "unknown"); // calls the 4 argument Constructor with the values
	}

	/**
	 * @return the attribute values
	 */
	public String toString() {
		String string = "";

		string = "Air Wing: " + airwing + " Squadron Designation: " + squadronDesignation + " Squadron Name: "
				+ squadronName + " Aircraft Type: " + aircraftType;

		return string;
	}

	// getters and setters are also known as (AKA) accessors (get) and mutators
	// (set)
	public String getAirwing() {
		return airwing;
	}

	public void setAirwing(String airwing) {
		this.airwing = airwing;
	}

	public String getSquadronDesignation() {
		return squadronDesignation;
	}

	public void setSquadronDesignation(String squadronDesignation) {
		this.squadronDesignation = squadronDesignation;
	}

	public String getSquadronName() {
		return squadronName;
	}

	public void setSquadronName(String squadronName) {
		this.squadronName = squadronName;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
}
