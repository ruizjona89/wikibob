# Eureka - Spring Boot

![Eureka Architecture](eurekaSpringBoot.png)

- 1 - Start Eureka Server
- 2 - Microservices Register and Publish
- 3 - Microservices Locate / Consume

- [Eureka Introduction / Overview](https://www.youtube.com/watch?v=_QezR9wkBKs)
- [Eureka Configuration](https://www.youtube.com/watch?v=GTM2J0nYmbs)
     - Example- https://github.com/koushikkothagal/spring-boot-microservices-workshop.git

# Eureka Products

- Eureka Discover - Client - discover or provide services
- Eureka Server - Server

# Creating Eureka Server

![Eureka Server 1](eurekaServer1.png)
![Eureka Server 2](eurekaServer2.png)
![Eureka Server 3](eurekaServer3.png)
![Eureka Server 4](eurekaServer4.png)





