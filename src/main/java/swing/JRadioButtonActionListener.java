package swing;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class JRadioButtonActionListener extends JFrame {

	private static final long serialVersionUID = 224725631499826389L;

	public JRadioButtonActionListener() {

		setTitle("Grouping Example");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel(new GridLayout(0, 1));

		ButtonGroup group = new ButtonGroup();
		JRadioButton aRadioButton = new JRadioButton("A");
		JRadioButton bRadioButton = new JRadioButton("B");

		ActionListener actionListener = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				AbstractButton aButton = (AbstractButton) actionEvent.getSource();
				System.out.println("Selected: " + aButton.getText());
				panel.setBackground(Color.RED);
			}
		};

		panel.add(aRadioButton);
		group.add(aRadioButton);
		panel.add(bRadioButton);
		group.add(bRadioButton);

		aRadioButton.addActionListener(actionListener);
		bRadioButton.addActionListener(actionListener);

		add(panel);
		setSize(300, 200);
		setVisible(true);
	}

	public static void main(String args[]) {
		new JRadioButtonActionListener();
	}
}