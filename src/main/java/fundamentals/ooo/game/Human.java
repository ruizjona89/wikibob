package fundamentals.ooo.game;

public class Human {

	private String name = "";
	private int strength = 0;
	private int health = 0;
	private int stamina = 0;
	private int speed = 0;
	private int attackPower = 0;

	public void run(boolean running) {
	}

	public void attack(int attackPower) {
	}

	/**
	 * @param decreaseHealth the amount of health to decrease the health parameter
	 *                       for instance, if health is 100 and decreaseHeath is 10,
	 *                       then change health to 90
	 */
	public void decreaseHealth(int decreaseHealth) {
		this.health -= decreaseHealth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getAttackPower() {
		return attackPower;
	}

	public void setAttackPower(int attackPower) {
		this.attackPower = attackPower;
	}
}
