#Regular Expression (RegEx)

Regular Expressions (RegEx) are a sequence of characters that define a search pattern, primarily used for validation. Most programming languages utilize RegEx to validate user input on forms, for instance a Web Page that receives user input:

 - Phone Number
 - Zip / Postal Code
 - Social Security Number

Here’s a site that shows a Java example VALIDATING a String contains 3 numbers, dash and 3 numbers:

[Java RegEx Example](https://regex101.com/r/qCNRVA/1/codegen?language=java)

The 4th line is setting up the regex. Quickly going through the statement:

 - ^ - start RegEx
 - d{3} - decimal length of 3
 - $ - end of RegEx

After looking at this, we are looking for a pattern of 3 decimals, dash and 3 decimals, e.g. 131-131, 999-999, 000-000, etc.

## Test Example
[RegEx Test](../../../test/java/regex/RegexTest.java)