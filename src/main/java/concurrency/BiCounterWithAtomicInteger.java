package concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class BiCounterWithAtomicInteger {

	private AtomicInteger i = new AtomicInteger();
	private AtomicInteger j = new AtomicInteger();

	public void incrementI() {

		i.incrementAndGet(); // atomic, thread safe
	}

	public int getI() {
		return i.get();
	}

	public void incrementJ() {

		j.incrementAndGet(); // atomic, thread safe
	}

	public int getJ() {

		return j.get();
	}
}
