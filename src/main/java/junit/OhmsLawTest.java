package junit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class OhmsLawTest {
	
	private static OhmsLaw ohmsLaw = null;

	// Should rename to @BeforeTestMethod
	// e.g. Creating an similar object and share for all @Test
	@Before
	public void runBeforeTestMethod() {
		System.out.println("@Before - runBeforeTestMethod");
		ohmsLaw = new OhmsLaw();
	}

	@Test
	public void test_method_1() {
		System.out.println("@Test - test_method_1");
		ohmsLaw.getCurrent();
	}

	// Should rename to @AfterTestMethod
	@After
	public void runAfterTestMethod() {
		System.out.println("@After - runAfterTestMethod");
		ohmsLaw = null;
	}
}
