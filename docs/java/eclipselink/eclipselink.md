# eclipseLink

## installation

- [eclipseLink download](https://www.eclipse.org/eclipselink/downloads/)
- unzip files into git directory, e.g. ~/git/cv64/javalib/eclipseLink

![user library](eclipseLinkLibrary.png)

## configuration

Example of a local workspace project instead of the team project.  The purpose of the JPA project is to generate the Classes which include the @ annotations required utilizing the JPA framework.

![jpa1.png](jpa1.png)

Example of setting up the database connection.

![jpa2.png](jpa2.png)

This panel shows the local instance of the data connection, with the javalib classes within the PerfectProcure product also including the EclipseLink product, required to generate the JPA Classes.

![jpa3.png](jpa3.png)
![jpa4.png](jpa4.png)
![jpa5.png](jpa5.png)
![jpa6.png](jpa6.png)

Eclipse web application setup parameters

![eclipseLink.png](eclipseLink.png)
![eclipseLinkLibrary.png](eclipseLinkLibrary.png)