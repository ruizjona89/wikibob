// Method reference is replacement of lambda expressions for code reusability
package lambda.functional.interfaces;

public class MethodReferenceDemo {
	public static void main(String[] args) {
//		SimpleFunctionalInterface simpleFunctionalInterface = Test :: testImplementation;
//		simpleFunctionalInterface.applyAsInt(5);
		FunctionalInterfaceDemo functionalInterfaceDemo = Test :: testImplementation;  // method referencing Class :: method
		functionalInterfaceDemo.abstractMethod();
		
		FunctionalInterfaceDemo f = () -> System.out.println("Implementation of abstract method");
		f.abstractMethod();
	}
}

class Test {
	public static void testImplementation() {
		System.out.println("Test implementation of abstract method");
	}
}
