# ![](../../docs/icons/cv64us50x50.png) Javascript

# Definitions

|Definition|Description|
|--|--|

- use strict - strict coding standards, e.g. all attributes must be initialized
- typeof - typeof operator - returns a string indicating the type of the operand
     - console.log(typeof 64); // prints "number"
     - console.log(typeof "Hello world");  // prints "string"
     
## Template Literals (this character `)

[Template Literal Example](javascript.html)

#
<p id="demo">someTitle</p>
var x = document.getElementById("demo");
x.style.color = "red"

## Prototype

- JavaScript objects inherit properties and methods from a prototype

```
<script>
  // function constructor
  function Person(name, job, yearOfBirth){  
    this.name = name;
    this.job = job;
    this.yearOfBirth = yearOfBirth;
  }

  // adding calculateAge() method to the Prototype property
  Person.prototype.calculateAge= function(){ 
    console.log('The current age is: '+(2019- this.yearOfBirth));
  }

  console.log(Person.prototype);
  
  // creating Object Person1
  let Person1= new Person('Jenni', 'clerk', 1986); 
  console.log(Person1)
  let Person2= new Person('Madhu', 'Developer', 1997);
  console.log(Person2)
  
  Person1.calculateAge();
  Person2.calculateAge();
</script>
```

## What are the differences between var, let, const?

- const, constant
- var, global scope, doesn't need to be initialized AKA Hoisting = var = undefined, redeclaration legal
- let, local scope, cannot be a global variable, required to be initialized = let = ReferenceError, redeclaration illegal

## What is difference between == and ===?

|compares value|compares value and type|
|--|--|
|if('1' == 1) // true|if('1' === 1) // false|

## What is the difference null and undefined (both are empty value)?

|null|undefined|
|--|--|
|you are setting null|when define a variable, automatically puts a placeholder called undefined|
|typeof(null) => object|js is setting undefined|
||typeof(undefined) => undefined|

## What is the use of arrow function?

```
Const profile = {
	firstname: '',
	lastname: '',
	
	setName: function(name) {
		let splitName = function(name) {
			let splitName = (n) => {
				let nameArray = n.split(' ');
				this.firstName = nameArray[0];
				this.lastname = nameArray[1];
			}
		splitName(name);
	}
}

profile.setName('john doe');

console.log(window.firstname);
console.log(profile.firstname);
```