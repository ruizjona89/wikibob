package lambda.basic;

public class Greeter {

	// JDK7 and earlier
	public void greet() {
		System.out.println("Greeter.greet()");
	}

	// JDK8 and later
	// public void greet(() -> { ... }));
	public void greet(Greeting greeting) {
		greeting.perform();
		System.out.println("Greeter.greet(greeting)");
	}

	public static void main(String[] args) {

		// JDK7 and earlier
		Greeter greeter = new Greeter();
		greeter.greet();

		// JDK8 and later
		HelloWorldGreeting helloWorldGreeting = new HelloWorldGreeting();

		// passing in a Class that contains a behavior to the greet method
		greeter.greet(helloWorldGreeting);

		// utilizing interior interface
		LambdaFunction lambdaFunction = () -> System.out.print("Lambda");
		System.out.println(lambdaFunction);

		// utilizing exterior interface (Greeting Class and the perform() method)
		Greeting greetingFunction = () -> System.out.print("Lambda");
		Greeting lambdaGreeting = () -> System.out.print("Lambda");

		// instance of Greeting Class the implements the perform method
		System.out.print("\nCalling instance Greeting and perform method:         ");
		greetingFunction.perform();

		// lambda expression of type Greeting Interface
		System.out.print("\nCalling Lambda expression of type Greeting Interface: ");
		lambdaGreeting.perform();

		// HERE'S SOMETHING INTERESTING - let's compare an anonymous inner class and
		// compare to a lambda expression
		Greeting innerClassGreeting = new Greeting() {
			public void perform() {
				System.out.println("Lambda");
			}
		}; // DO NOT FORGET THE ; AT THE END OF AN ANONYMOUS / INNER CLASS - GET BURNED
			// EVERYTIME

		System.out.print("\nCalling Inner Class and perform method:               ");
		innerClassGreeting.perform();

		// utilizing interior interface
		LambdaAdd addFunction = (int a, int b) -> a + b;
		System.out.println(addFunction);

		// another example of calling the one argument greet method passing either the
		// instance or lambda expression
		System.out.print("\nCalling greeter.greet(greetingFunction):              ");
		greeter.greet(greetingFunction);
		System.out.print("Calling greeter.greet(lambdaGreeting):                  ");
		greeter.greet(lambdaGreeting);
	}
}

// interior interface
interface LambdaFunction {
	void noArguments(); // can name this anything, must be unique
}

interface LambdaAdd {
	int add(int x, int y);
}
