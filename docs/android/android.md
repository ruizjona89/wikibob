# Android - Google Mobile Application

[Code](https://gitlab.com/bobby.estey/android)

# Troubleshooting

|Issue|Solution|
|--|--|
|Code Execution Errors|- Build -> Clean Project<br>- File -> Invalidate Caches / Restart...|
|Activity Errors|- Navigate to Activity<br>- Look at Component Tree for Errors, RED Exclamation Marks<br>- Select Magic Wand at top|
|[Recovery](https://stackoverflow.com/questions/12732882/reverse-engineering-from-an-apk-file-to-a-project)||

# Acronyms / Glossary

|Term|Description|Reference|
|--|--|
|Intent|calls an activity||

# Notes
- no spaces, uppercases or extensions on images

# Linux - start up Android

- google android studio (gas)
- alias gas='/opt/android-studio/bin/studio.sh &'

# Android Configurations

![Auto Import](autoImport.png)

- Linux / Windows:
     - File -> Settings -> Editor -> General -> Auto Import -> Java
     - Change Insert imports on paste:  Always
     - Check Add unambigious imports on the fly
- Macintosh:
      - Android Studio -> Preferences
      
# menu / menu item

```
// renders the Menu and Menu Items
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

@Override
public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.menu_main, menu);
    return true;
}
```