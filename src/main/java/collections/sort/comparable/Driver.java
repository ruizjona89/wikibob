// Comparable - comparing instances (objects) of the same type

package collections.sort.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Driver {

	public static void main(String[] args) {

		// create a list of type Car
		List<Car> list = new ArrayList<Car>();

		// create an object of type Car and add to list in the following order
		list.add(new Car("Plymouth", 99.3, 1967));
		list.add(new Car("Dodge", 180.3, 2004));
		list.add(new Car("Chrysler", 88.8, 1980));

		// call the Collections class, sort method and pass in the list
		// does not return a new list because we have a reference to the list
		Collections.sort(list);

		// print out the list in sorting order by the year
		System.out.println("List Sorted by Year:\n");

		// print each car one at a time in list order (sorted order)
		for (Car car : list) {

			System.out.println(
					"Name: " + car.getName() + " " + " Miles: " + car.getMiles() + " " + " Year: " + car.getYear());
		}
	}
}