# Function Interface Supplier<R> { R get(); }

- predefined Functional Interface having one abstract method
- Will just supply object without any input
- No chaining
- Write one, use anywhere

```
public class SupplierExample {
  Supplier<Date> currentDate = () -> new Date();
  System.out.println(currentDate.get());
}
```
