// https://www.youtube.com/watch?v=5K4eVKCpuHQ
// input type, return result - only one abstract method result = apply(type)

package lambda.functional.interfaces;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class OptionalFunctionInterfaceExample {

	private static void simpleFunction() {
		// new Function (anonymous Class)
		Function<String, Integer> function1 = new Function<>() {

			@Override
			public Integer apply(String t) {
				return t.length();
			}
		};

		Function<String, Integer> function2 = (t) -> t.length();

		Function<Integer, String> function3 = (number) -> {
			if (number % 2 == 0) {
				return "Number " + number + " is even";
			} else {
				return "Number " + number + " is odd";
			}
		};

		Function<String, Integer> function4 = (t) -> t.length();
		Function<Integer, Integer> function5 = (number) -> number * 2;

		Integer integer = function4.andThen(function5).apply("Bobby");

		System.out.println("function1: " + function1.apply("Bobby")); // function1: 5
		System.out.println("function2: " + function2.apply("Robert")); // function2: 6
		System.out.println("function3: " + function3.apply(13)); // function3: Number 13 is odd
		System.out.println("function4: " + function3.apply(14)); // function4: Number 14 is even
		System.out.println("integer  : " + integer); // 10
	}

	// Function<T, R> function is a Function Interface to a method
	private static <T, R> Map<T, R> convertListToMap(List<T> list, Function<T, R> function) {
		Map<T, R> result = new HashMap<>();
		for (T t : list) {
			result.put(t, function.apply(t));
		}

		return result;
	}

	public static void main(String[] args) {
		OptionalFunctionInterfaceExample.simpleFunction();

		List<String> list = Arrays.asList("Java", "C++", "Python", "TypeScript");

		// convertListToMap is passing the list and lambda expression x.length to the
		// method as a list and function on line 44
		Map<String, Integer> map = convertListToMap(list, x -> x.length()); // x -> x.length
		System.out.println(map); // {TypeScript=10, Java=4, C++=3, Python=6}

	}
}
