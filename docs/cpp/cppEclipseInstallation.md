# ![](../../docs/icons/cv64us50x50.png) C Plus Plus (C++ / CPP)

## Francis Sandy Hill, University Massachusetts, Amherst
- I was fortunate to have two Graphics courses with Dr. Hill who was not only fascinating and knowledgeable, he was also an excellent Professor.  He was one of my favorite teachers and I learned a lot from him.  Dr. Hill was a specialist in Graphics.  We learned a lot from him, anything from basics to Ray Tracers.

Below is a ZIPped up collection of some of my submissions to Dr. Hill
- [ECE 660 / 661]([https://gitlab.com/bobby.estey/cpp/-/blob/master/cpp-ece661.zip)

https://gitlab.com/bobby.estey/cpp

## installation on the eclipse IDE

 - [eclipse installation](https://www.youtube.com/watch?v=O0oExvj5CE4)
