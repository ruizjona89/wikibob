package fundamentals;

public class OverloadingOverriding {

	@Override
	public String toString() {
		return "";
	}

	public int Overloading(int value) {
		return 0;
	}

	// this is overriding not Overloading and even through the return type is
	// different not allowdd
//	public int Overloading() {
//		return 0;
//	}

	public void Overloading() {

	}

	public static void main(String[] args) {

	}
}
