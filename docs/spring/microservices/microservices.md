# Microservices (mS)

- Microservices (mS) is an architectural approach to break up Monolithic systems (one big container of everything that is deployed as a single product) into smaller / manageable components (small autonomous (govern itself) services) and deploying the components (services) separately.  While mS are self sufficient, they also can be dependent on other mS.
- Communication is performed with Application Programming Interface (API)s usually using Representational State Transfer (REST) AKA (RESTful API) and common payload is the JSON protocol.
- Common Tools for mS are products like Postman and Google DevTools
- Advantages of mS - less domino effect (less likely to take the system down), independent deployment (easier and faster to make changes and deploy), can utilizing any technology diversity (doesn't have to be the same technology)
- mS are managed by tools such as ... that provide status, reports and administration.
- mS are sometimes more complex, e.g. feature flag.  Do not turn on the capability until other mS are ready for the new feature.  Whereas a Monolithic is already setup as a single deployment.  mS depend on each other so communication is harder.  A distributed system more complexity and overhead to manage.
- mS is best suitable when there a large applications and scaled up and down.  Smaller teams have faster turnaround.  
- Client certificates are digital certificates used to make authenticated requests to a remote server
- PACT - open source tool to contract testing between service providers and consumers
- OAuth - open protocol that allows trust between mS, and share resources without another sites credentials.  Instead of consistently reauthorizations
- End to End mS testing - test the entire domain, testing the system, not one mS
- Containers are the easiest and effective method to manage mS based applications, e.g. Docker
- Semantic Monitoring - monitoring a use case or process flow, testing a business case
- Consumer Driven Contract (CDC) - Client driving the contract and what they need
- Docker - Containerization - a container environment that contains the software application, dependencies that are tightly packaged together.
- Reactive Extensions (Rx) - writing asynchronous services, not mS
- Continuous Monitoring - CI / CD - keep a check on everything, dashboards, performance, etc.
- Independent mS communicate with each other through HTTP/REST with JSON or Binary protocols

|Microservices|Monolithic Architecture|
|--|--|
|Service Start up is Fast|Takes Time|
|Loosely Coupled|Tightly Coupled|
|Essential Messaging Frameworks||
|Decentralized Governance||

- The new kid on the block is WebClient
-[Start of WebClient](https://www.youtube.com/watch?v=F3uJyeAyv5g)

## [Microservices](https://www.youtube.com/watch?v=y8IQb4ofjDo&list=PLqq-6Pq4lTTZSKAFG6aCDVDP86Qx4lNas&index=1)

- everything comes to an end and REST will eventually be replaced by WebClient
-[End of REST / Start of WebClient](https://www.youtube.com/watch?v=F3uJyeAyv5g)

- ServiceModel serviceModel = restTemplate.getForObject(url, classType);
     - url:  "http://localhost:8112/service2Service/1"
     - ServiceModel.class

## Terminology

- RestTemplate (org.springframework.web.client.RestTemplate) - to make Rest API Calls
- Reactive Programming - programming that is not synchronous (asynchronous)
    - synchronous - wait for response, stop processing
    - asynchronous - continue processing, do not wait
- Beans are Singleton instances and Producers.  Beans should be utilized when calling REST Services
- Once a Bean has been identified, no longer create an instance in the Classes
- Autowired are Consumers looking for something for example Beans
@see Service3Application.java
```
	// returns the Bean just once (singleton)
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
```