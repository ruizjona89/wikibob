<?php
header('Content-Type: application/json');

require_once('../private/configStudent.php');

$databaseConnection=$config['databaseConnection'];

// Create connection
// https://www.cv64.us/cv64/database/public/testConnectionStudent.php
$connection = new mysqli($databaseConnection['servername'], $databaseConnection['username'], $databaseConnection['password'], $databaseConnection['database']);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}
echo "Connected successfully";

$connection->close();
?>
