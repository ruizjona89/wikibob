/* Extract this as crashme.c, cc -o crashme crashme.c and then try this:  */
/* %crashme 1000 10 200							  */

#include <stdio.h>
#include <signal.h>
#include <setjmp.h>

long nbytes,nseed,ntrys;
unsigned char *the_data;

jmp_buf again_buff;

void (*badboy)();

void again_handler(sig, code, scp, addr)
     int sig, code;
     struct sigcontext *scp;
     char *addr;
{char *ss;
 switch(sig)
   {case SIGILL: ss =   " illegal instruction"; break;
    case SIGTRAP: ss =   " trace trap"; break;
    case SIGFPE: ss =   " arithmetic exception"; break;
    case SIGBUS: ss =  " bus error"; break;
    case SIGSEGV: ss =  " segmentation violation"; break;
   default: ss = "";}
 fprintf(stderr,"Got signal %d%s\n",sig,ss);
 longjmp(again_buff,3);}
 

set_up_signals()
{signal(SIGILL,again_handler);
 signal(SIGTRAP,again_handler);
 signal(SIGFPE,again_handler);
 signal(SIGBUS,again_handler);
 signal(SIGSEGV,again_handler);}

compute_badboy()
{long j,n;
 n = (nbytes < 0) ? - nbytes : nbytes;
 for(j=0;j<n;++j) the_data[j] = (rand() >> 7) & 0xFF;
 if (nbytes < 0)
   {fprintf(stdout,"Dump of %ld bytes of data\n",n);
    for(j=0;j<n;++j)
      {fprintf(stdout,"%3d",the_data[j]);
       if ((j % 20) == 19) putc('\n',stdout); else putc(' ',stdout);}
    putc('\n',stdout);}}

try_one_crash()
{compute_badboy();
 if (nbytes >= 0) 
   (*badboy)();}
 
main(argc,argv)
 int argc; char **argv;
{long i;
 if (argc != 4) {fprintf(stderr,"crashme <nbytes> <srand> <ntrys>\n");
		 exit(1);}
 nbytes = atol(argv[1]);
 nseed = atol(argv[2]);
 ntrys = atol(argv[3]);
 fprintf(stdout,"crashem %ld %ld %ld\n",nbytes,nseed,ntrys);
 fflush(stdout);
 the_data = (unsigned char *) malloc((nbytes < 0) ? -nbytes : nbytes);
 badboy = (void (*)()) the_data;
 fprintf(stdout,"Badboy at %d. 0x%X\n",badboy,badboy);
 srand(nseed);
 for(i=0;i<ntrys;++i)
   {fprintf(stderr,"%ld\n",i);
    if(setjmp(again_buff) == 3)
      fprintf(stderr,"Barfed\n");
    else
      {set_up_signals();
       try_one_crash();
       fprintf(stderr,"didn't barf!\n");}}}
