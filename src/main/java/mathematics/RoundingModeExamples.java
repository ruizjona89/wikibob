// https://stackoverflow.com/questions/7124448/how-does-java-math-roundingmode-work
package mathematics;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoundingModeExamples {
	
	private static BigDecimal decimal = null;

	public static double round5Places(double d) {
		return ((long) (d < 0 ? d * 100000 - 0.5 : d * 100000 + 0.5)) / 100000.0;
	}
	
	public static double round4Places(double d) {
		return ((long) (d < 0 ? d * 10000 - 0.5 : d * 10000 + 0.5)) / 10000.0;
	}

	public static double round2Places(double d) {
		return ((long) (d < 0 ? d * 100 - 0.5 : d * 100 + 0.5)) / 100.0;
	}
	
	public static double getDownOrUp(double d) {
		double getDigits = d - (long) d;
		double convertDecimalToInt = round5Places(getDigits) * 100000;
		int lessOrGreater =  (int)(convertDecimalToInt % 10);
		
		if (lessOrGreater < 5) {
			decimal = new BigDecimal(d).setScale(4, RoundingMode.DOWN);
		} else {
			decimal = new BigDecimal(d).setScale(4, RoundingMode.UP);
		}
		
		return decimal.doubleValue();
	}

	public static void main(String[] args) {

		double lowDouble   = 1.55553d;
		double highDouble  = 1.55555d;

		System.out.println("lowDouble:                             " + lowDouble);
		System.out.println("simple - getDownOrUp:                  " + RoundingModeExamples.getDownOrUp(lowDouble));
		System.out.println("simple - round4Places:                 " + RoundingModeExamples.round4Places(lowDouble));
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.UP);
		System.out.println("RoundingMode.UP - lowDouble:           " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.DOWN);
		System.out.println("RoundingMode.DOWN - lowDouble:         " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.CEILING);
		System.out.println("RoundingMode.CEILING - lowDouble:      " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.FLOOR);
		System.out.println("RoundingMode.FLOOR - lowDouble:        " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.HALF_UP);
		System.out.println("RoundingMode.HALF_UP - lowDouble:      " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.HALF_DOWN);
		System.out.println("RoundingMode.HALF_DOWN - lowDouble:    " + decimal.doubleValue());
		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.HALF_EVEN);
		System.out.println("RoundingMode.HALF_EVEN - lowDouble:    " + decimal.doubleValue());
//		decimal = new BigDecimal(lowDouble).setScale(4, RoundingMode.UNNECESSARY);
//		System.out.println("RoundingMode.UNNECESSARY - lowDouble:  " + decimal.doubleValue());
 
		System.out.println("highDouble:                            " + highDouble);
		System.out.println("simple - getDownOrUp:                  " + RoundingModeExamples.getDownOrUp(highDouble));
		System.out.println("simple - round4Places:                 " + RoundingModeExamples.round4Places(highDouble));
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.UP);
		System.out.println("RoundingMode.UP - highDouble:          " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.DOWN);
		System.out.println("RoundingMode.DOWN - highDouble:        " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.CEILING);
		System.out.println("RoundingMode.CEILING - highDouble:     " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.FLOOR);
		System.out.println("RoundingMode.FLOOR - highDouble:       " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.HALF_UP);
		System.out.println("RoundingMode.HALF_UP - highDouble:     " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.HALF_DOWN);
		System.out.println("RoundingMode.HALF_DOWN - highDouble:   " + decimal.doubleValue());
		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.HALF_EVEN);
		System.out.println("RoundingMode.HALF_EVEN - highDouble:   " + decimal.doubleValue());
//		decimal = new BigDecimal(highDouble).setScale(4, RoundingMode.UNNECESSARY);
//		System.out.println("RoundingMode.UNNECESSARY - highDouble: " + decimal.doubleValue());
	}
}
