// https://www.geeksforgeeks.org/convert-base-decimal-vice-versa/
package cs;

class HexBaseConverter {

	// To return value of a char.
	// For example, 2 is returned
	// for '2'. 10 is returned
	// for 'A', 11 for 'B'
	static int val(char c) {
		if (c >= '0' && c <= '9')
			return (int) c - '0';
		else
			return (int) c - 'A' + 10;
	}

	// Function to convert a
	// number from given base
	// 'b' to decimal
	public static int toDecimal(String str, int base) {
		int len = str.length();
		int power = 1; // Initialize
						// power of base
		int num = 0; // Initialize result
		int i;

		// Decimal equivalent is
		// str[len-1]*1 + str[len-1] *
		// base + str[len-1]*(base^2) + ...
		for (i = len - 1; i >= 0; i--) {
			// A digit in input number
			// must be less than
			// number's base
			if (val(str.charAt(i)) >= base) {
				System.out.println("Invalid Number");
				return -1;
			}

			num += val(str.charAt(i)) * power;
			power = power * base;
		}

		return num;
	}

	// To return char for a value. For
	// example '2' is returned for 2.
	// 'A' is returned for 10. 'B' for 11
	static char reVal(int num) {
		if (num >= 0 && num <= 9)
			return (char) (num + 48);
		else
			return (char) (num - 10 + 65);
	}

	// Function to convert a given decimal number
	// to a base 'base' and
	public static String fromDecimal(int base1, int inputNum) {
		String s = "";

		// Convert input number is given
		// base by repeatedly dividing it
		// by base and taking remainder
		while (inputNum > 0) {
			// s += (char)(inputNum + 48) % base1;
			s += reVal(inputNum % base1);
			inputNum /= base1;
		}
		StringBuilder ix = new StringBuilder();

		// append a string into StringBuilder input1
		ix.append(s);

		// Reverse the result
		return new String(ix.reverse());
	}

	// Driver code
	public static void main(String[] args) {
		String str = "123";
		int inputNum = 123;
		int base = 9;
		System.out.println("Decimal equivalent of " + str + " in base " + base + " is " + " " + toDecimal(str, base));
		System.out.println("Equivalent of " + inputNum + " in base " + base + " is " + fromDecimal(base, inputNum));
	}
}
