package fundamentals.interfaces.aircraft;

public class Engines implements Engines2Electronics {

	private int speed = 0;

	@Override
	public int getSpeed() {
		return speed;
	}

	@Override
	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
