/**
 * this equals Farmer (this always equals the Class name strength (initial value
 * = 75) health (initial value = 100) stamina (initial value = 75) speed
 * (initial value = 10) attackPower (initial value = 1)
 */
package fundamentals.ooo.game;

public class Farmer extends Human {

	public Farmer(String name) {
		this.setName(name);
		this.setStrength(75);
		this.setHealth(100);
		this.setStamina(75);
		this.setSpeed(10);
		this.setAttackPower(1);
	}

	public void plow() {
	}

	public void harvest() {
	}
}
