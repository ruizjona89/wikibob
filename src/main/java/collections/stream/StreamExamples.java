package collections.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExamples {

	private static int[] intArray = { 1, 2, 3, 4, 5 };

	public static void example1() {

		Stream.of(1, 2, 3).forEach((n) -> System.out.print(n + "\t"));

		List<Integer> list = Arrays.stream(intArray) // IntStream
				.boxed() // Stream<Integer>
				.collect(Collectors.toList());

		list.forEach((n) -> System.out.print(n + "\t"));
		System.out.println();
	}

	public static void example2() {
		// Creating an IntStream
		IntStream stream = IntStream.range(3, 8);

		// Creating a Stream of Integers
		// Using IntStream boxed() to return
		// a Stream consisting of the elements
		// of this stream, each boxed to an Integer.
		Stream<Integer> stream1 = stream.boxed();

		// Displaying the elements - method reference ::
		// stream1.forEach(x -> System.out.println(x));
		stream1.forEach(System.out::println);
	}

	public static void main(String args[]) {

		StreamExamples.example1();
		StreamExamples.example2();
	}
}