# Concurrency - https://www.youtube.com/watch?v=2Bvz_jsQPHk

// i++; - get i, increment, set i - not thread safe or autonomous
// must add synchronized
public void increment() {
  i++;
}

// another approach is to not use synchronized - us java.util.concurrent.locks.Lock / ReentrantLock
Lock lockForI = new ReentrantLock();

public void incrementI() {
	lockForI.lock(); // get lock for i, do not need synchronized
	i++;
	lockForI.unlock(); // release lock for i
}

private AtomicInteger i = new AtomicInteger(); // atomic, thread safe
return i.get(); // returns int

- Concurrent Collections offer "Thread Safety" without "Significant Performance Impact"

- CopyOnWriteArrayList
     - copies the entire array, very costly if excessive updates to the ArrayList
     - copies only the add method, never the get method