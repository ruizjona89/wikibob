# Multiple Examples

- there are no Tri, Quad, only Bi are available at this time
  
```
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

public class BiExamples {
  public static void main(String[] args) {

    // accepts two inputs and returns boolean
    BiPredicate<Integer, Integer> checkSumTwo = (a, b) -> a + b >= 5;
    System.out.println("sum of 2 and 5 is great than 5: " + checkSumTwo.test(2, 5));
    System.out.println("sum of 2 and 1 is great than 5: " + checkSumTwo.test(2, 1));
    
    // accepts two inputs and returns one output
    BiFunction<Integer, Integer, Integer> multiplyBoth = (a, b) -> a * b;
    System.out.println("Multiplication of 5 and 10 is: " + multiplyBoth.apply(5, 10));
  }
}
```