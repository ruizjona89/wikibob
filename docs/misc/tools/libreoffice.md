# LibreOffice Notes

# disable presenter console
- Tools -> Options -> LibreOffice Impress -> General -> Presentation
- uncheck Enable Presenter Console

![disable presenter console](disablePresenterConsole.png)
