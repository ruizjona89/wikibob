# Angular Unit Testing

- Karma - karma.conf.js - sets up a web server and tests code automatically once code is modified
- Jasmine - protractor.conf.js - (e2e directory) end to end testing within the browser, decoupled from main application code
- src/test.ts - used by Karma to set up the spec files
- mock - instance (object) created to simulate a classes behavior used for testing purposes, e.g. a fake class with the same method signature as the real class
- spy - Jasmine spies provide custom return values, stub and check functions
- app.component.spec.ts - since we generated our project prior to Testing was available we need to create a new app.component.spec.ts file, e.g.  ng g component AppComponent --spec-only
    - describe - what you are testing
    - it - test block, also known as spect
    - expect - expectation of the test
    - fixture - test environment for the component and provides access to the component
    - debugElement - the components rendered HTML
    - beforeEach(async(() =>
    - beforeEach(() =>
    - fixture.detectChanges() - before testing check for changes
    - TestBed - specific tests of the component

    - compileComponents - compiles components HTML and CSS
    - toBeTruthy() something evaluates to true, not the primitive true value

# Angular Unit Testing Configuration and Execution
|Angular Unit Testing Configuration and Execution||
|--|--|
|add JQuery and Bootstrap<br>- karma.conf.js|files: [<br>'./jquery.min.js',<br>'./bootstrap.min.js'<br>],|
|component.spec.ts|import MODULES both at the beginning of the file and in beforeEach|
|- cd ~/git/perfectprocure-ui/NewUI<br>- ng test|- change directory where the configuration files are located<br>- compiles the test and brings up Karma|

# Karma Testing
- Press DEBUG will bring up DEBUG RUNNER and Router: 
- Component toBeTruthy passed

![Karma](karma.png)
![Jasmine](jasmine.png)