// http://www.java2s.com/example/java/swing/choosing-colors-with-jcolorchooser.html
package swing;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

class ShowColor extends JFrame {

	private static final long serialVersionUID = 2657741219477359457L;
	private final JButton changeColorJButton;
	private Color color = Color.LIGHT_GRAY;
	private final JPanel colorJPanel;

	public ShowColor() {
		super("Using JColorChooser");

		// create JPanel for display color
		colorJPanel = new JPanel();
		colorJPanel.setBackground(color);

		// set up changeColorJButton and register its event handler
		changeColorJButton = new JButton("Change Color");

		// define action listener, pass the event e inside the body
		changeColorJButton.addActionListener(e -> {

			color = JColorChooser.showDialog(ShowColor.this, "Choose a color", color);

			// set default color, if no color is returned
			if (color == null)
				color = Color.LIGHT_GRAY;

			// change content pane's background color
			colorJPanel.setBackground(color);
		});

		add(colorJPanel, BorderLayout.CENTER);
		add(changeColorJButton, BorderLayout.SOUTH);

		setSize(400, 130);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // exit Swing
	}
}

public class ShowColors {

	public static void main(String[] args) {
		new ShowColor(); // call the Constructor
	}
}
