package lambda.functional.interfaces;

import java.util.function.IntUnaryOperator;

// Functional Interface have exactly one single abstract method and can have multiple static and default methods
// Java program to demonstrate lambda expressions to implement a user defined functional interface.
// @FunctionalInterface checks if there is only one abstract method
// applyAsInt method is defined as a functional interface in the class IntUnaryOperator

@FunctionalInterface
interface OddEven {
	int applyAsInt(int x);
}

class SimpleFunctionalInterface {
	public static void main(String args[]) {

		final int ODD = 5;
		final int EVEN = 6;

		// lambda expression to define the calculate method
		IntUnaryOperator result = (int x) -> x;

		// parameter passed and return type must be same as defined in the prototype
		System.out.println("odd:  " + result.applyAsInt(ODD));
		System.out.println("even: " + result.applyAsInt(EVEN));
	}
}
