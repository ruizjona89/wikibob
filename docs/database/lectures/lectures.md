# Database Lectures

- [Chapter 1 - Unified Modeling Language](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/lectures/chapter1/lecture1.md)
- [Chapter 2 - Database Application Design](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/lectures/chapter2/lecture2.md)
- [Chapter 3 - Database Connectivity](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/lectures/chapter3/lecture3.md)
- [Chapter 4 - Enterprise Data](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/lectures/chapter4/lecture4.md)
- [Chapter 5 - Database Management](https://gitlab.com/bobby.estey/wikibob/-/blob/master/docs/database/lectures/chapter5/lecture5.md)