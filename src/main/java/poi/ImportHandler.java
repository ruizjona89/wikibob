package poi;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import poi.exceptions.POIException;
import poi.io.SpreadsheetHandler;
import poi.exceptions.SpreadsheetServiceException;

/**
 * ImportHandler - imports spreadsheets
 */
public class ImportHandler {
	/**
	 * SSAPP Spreadsheet handler
	 */
	private SpreadsheetHandler spreadsheetHandler = new SpreadsheetHandler();

	/**
	 * 
	 * ImportHandler.java
	 * 
	 * @param importType - the type of import enumeration
	 * @param inputFile  - the file name of the spreadsheet
	 * @throws IllegalArgumentException
	 * @throws SpreadsheetServiceException
	 */
	protected ImportHandler(int importType, String inputFile)
			throws IllegalArgumentException, SpreadsheetServiceException {
		System.out.println("ImportHandler(" + importType + ", " + inputFile);

		try {
			// opens up the spreadsheet as input
			spreadsheetHandler.initializeInputFile(inputFile);

			// switch on the type of import
			switch (importType) {
			case 0:
				parseSpreadsheet();
				break;

			default:
				System.err.println("Invalid Type");
				break;
			}

			// close spreadsheet
			spreadsheetHandler.closeFiles();

		} catch (POIException exception) {
			throw new SpreadsheetServiceException(exception.getMessage());
		} catch (Exception exception) {
			throw new SpreadsheetServiceException(exception.getMessage());
		}
	}

	/**
	 * method parse spreadsheet import to the database
	 */
	private void parseSpreadsheet() {
		try {
			HSSFWorkbook workbook = spreadsheetHandler.getWorkbook();
			HSSFSheet sheet = workbook.getSheetAt(0);

			int rows = sheet.getPhysicalNumberOfRows();

			System.out.println("ImportHandler - rows to import: " + rows);

			String[] column = new String[4];

			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 4; j++) {
					column[j] = sheet.getRow(i).getCell(j).toString();
					if ((sheet.getRow(i).getCell(j).toString()) == null) {
						column[j] = "";
					}

					System.out.println("Cell Value: " + column[j]);
				}
			}
		} catch (Exception exception) {
			System.err.println("ImportHandler.parseChecklist" + exception.getMessage());
		}
	}
}
