# Eclipse Installation

##### [Download](https://www.eclipse.org/)

##### Create opt (optional directory)
![01](eclipse01.png)

##### Select Eclipse IDE for Java Developers
![00](eclipse00.png)

##### Select Eclipse Marketplace...
![03](eclipse03.png)

##### Select PyDev and Press Install
![04](eclipse04.png)

##### Select Checkbox and Press Confirm >
![05](eclipse05.png)

##### Select I accept the terms of the license agreements
![06](eclipse06.png)

##### Press Resetart Now
![08](eclipse08.png)

##### Create New Project and select PyDev Project and Press Next
![10](eclipse10.png)
