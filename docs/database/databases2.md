# Databases

# Troubleshooting

|Troubleshooting|Solution|
|--|--|
|ORA-02019: connection description for remote database not found 02019. 00000 -  "connection description for remote database not found"|Only DBA Role can fix this connections do not work|
|ORA-28365: wallet is not open|alter system set encryption wallet open identified by "<password>";|

#Miscellaneous

|right outer join with (+) and without|left outer join with (+) and without|full outer join with (+) and without|
|--|--|--|
|SELECT e.last_name, d.department_name FROM employees e,departments d WHERE e.department_id(+) = d.department_id;|SELECT e.last_name, d.department_name FROM employees e,departments d WHERE e.department_id = d.department_id(+);|SELECT e.last_name, d.department_name FROM employees e,departments d WHERE  e.department_id = d.department_id(+) UNION ALL SELECT NULL, d.department_name FROM departments d WHERE NOT EXISTS (SELECT 1 FROM employees e WHERE e.department_id = d.department_id);
|SELECT e.last_name, d.department_name FROM employees e RIGHT OUTER JOIN departments d ON (e.department_id = d.department_id);|SELECT e.last_name, d.department_name FROM employees e LEFT OUTER JOIN departments d ON (e.department_id = d.department_id);|SELECT e.last_name, d.department_name FROM employees e RIGHT FULL JOIN departments d ON (e.department_id = d.department_id);|
|Accounts|To set up account privileges, [Create Account](https://wiki.hubwoo.com/display/ITVD/Create+User+Accounts)|

# Commands
- Trimming
  - TRIM(SUBSTR(poi.userdefined8, (INSTR(poi.userdefined8, '~') +1))) AS CMS_CATEGORY,
- show substr('abcdefg', 3, 4) = cdef
- show substr('abcdefg' -5, 4) = cdef
- instr - returns integer indicating a position of a character in a string
- to_char(myField, 'MM/DD/YYYY HH24:MI:SS') AS field
- to_char(myField, '000000000000000V0000') AS field
- Check the 3rd to the last character of a field if matches G then write ML as ML_MC else write MC as ML_MC
  - CASE WHEN substr(field, length(field) -3, 1 = 'G' THEN 'ML' THEN 'MC' END AS ML_MC

# Date Examples
|date examples|results|
|--|--|
|[Date Format Fields](http://oracledeli.wordpress.com/2013/03/07/sql-developer-date-time-format/)||
|to_char(SYSDATE, 'YYDDD')|julian date|
|TO_CHAR(dateField, 'YYYY-MM-DD HH:MM:SS')|Convert date field to text|
|select to_char(sysdate -8/24, 'hh24:mi:ss') from dual;|date from 8 hours prior|
|(c1_po.createdate BETWEEN (SYSDATE -8/24) AND (SYSDATE))|between -8 hours and current time|
|select SYSDATE, trunc(sysdate) -1 + 20/24 as START_DATE, trunc(sysdate) + 20/24 as END_DATE from dual;|SYSDATE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;08-APR-16 10.43.50 AM<br>START_DATE: 07-APR-16 08.00.00 PM<br>END_DATE:&nbsp;&nbsp;&nbsp;&nbsp;08-APR-16 08.00.00 PM|
|```h.createdate, h.invoice_due_date, TO_CHAR(h.invoice_due_date,'DD/MM/YYYY') || ' ' || TO_CHAR(h.createdate,'HH24:MI:SS') as COMBINATION_CHAR ,```||
|```TO_DATE(  (TO_CHAR(h.invoice_due_date,'DD/MM/YYYY') || ' ' || TO_CHAR(h.createdate,'HH24:MI:SS')), 'DD/MM/YYYY HH24:MI:SS') AS COMBINATION_DATE,```||
```|TO_CHAR(CAST((FROM_TZ(CAST( TO_DATE( (TO_CHAR(h.invoice_due_date,'DD/MM/YYYY') || ' ' || TO_CHAR(h.createdate,'HH24:MI:SS')), 'DD/MM/YYYY HH24:MI:SS') AS Timestamp), 'CST') AT TIME ZONE  'Pacific/Auckland') AS DATE),'DD/MM/YYYY HH24:MI:SS') AS CST_NZT,```||
|SELECT TO_CHAR(SYSDATE) "Today's Date and Time" from DUAL;|09/01/2006 00:00:00|
|SELECT TO_CHAR(TRUNC(SYSDATE),'MM/DD/YYYY') "Today's Date and Time" from DUAL;|09/01/2016|
|SELECT TO_CHAR(TRUNC(SYSDATE)) "Today's Date and Time" from DUAL;|01-SEP-2016 00:00:00|
|SELECT TO_CHAR(SYSDATE,'MM/DD/YYYY HH24:MI:SS') "Today's Date and Time" from DUAL;|09/01/2016 09:41:39|
|SELECT TO_CHAR(SYSDATE) "Today's Date and Time" from DUAL;|01-SEP-2016 09:41:49|
|select TO_CHAR(TO_DATE('01-JAN-16','DD-MON-YY', 'NLS_DATE_LANGUAGE = English'), 'DD/MM/YYYY') from dual;|Convert formats|
|<pre>TO_CHAR(CAST((FROM_TZ(cast(h.createdate as timestamp), 'CST')<br>&nbsp;&nbsp; AT TIME ZONE 'Pacific/Auckland') AS DATE),'DD/MM/YYYY') AS INVOICE_DATE,</pre>|Time Zone (CST to NZT)|
|<pre>EXTRACT( YEAR FROM ( SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland' ) ) \|\|<br>&nbsp;&nbsp;LPAD( EXTRACT( MONTH FROM<br>&nbsp;&nbsp;( SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland' ) ), 3, '0' ) as AccountingPeriod,<br>&nbsp;&nbsp;&nbsp;&nbsp;TO_CHAR( SYSTIMESTAMP AT TIME ZONE 'Pacific/Auckland', 'YYYYMMDD' ) as TransactionDate,</pre>|Time Zone (CST to NZT) 2230 NZT|

# Count Cursors
|count cursor||
|--|--|
|SELECT s.machine,<br>&nbsp;&nbsp;oc.user_name, oc.sql_text, Count(1), s.sid<br>&nbsp;&nbsp;&nbsp;&nbsp;FROM   v$open_cursor oc, v$session s<br>&nbsp;&nbsp;&nbsp;&nbsp;WHERE oc.sid = s.sid<br>&nbsp;&nbsp;&nbsp;&nbsp;GROUP BY user_name, sql_text, machine, s.sid<br>&nbsp;&nbsp;&nbsp;&nbsp;HAVING Count(1) > 2<br>&nbsp;&nbsp;&nbsp;&nbsp;ORDER  BY Count(1) DESC;|SELECT Max(a.value) AS<br>&nbsp;&nbsp;HIGHEST_OPEN_CUR,P.value AS MAX_OPEN_CUR, S.username<br>&nbsp;&nbsp;FROM v$sesstat a, v$statname B, v$parameter P, v$session S<br>&nbsp;&nbsp;WHERE  a.statistic# = b.statistic# AND s.sid = a.sid AND B.name = 'opened cursors current' AND P.name = 'open_cursors'<br>&nbsp;&nbsp;GROUP BY P.value, S.username<br>&nbsp;&nbsp;ORDER  BY 1 DESC;|

#[Bind Variable](https://youtu.be/MnISfllmK74)
## parsing
- setting up the query, memory management, process creation, all the overhead, etc.  very expensive
  - is the query systematically correct
  - is the query statement correct
    - columns, tables, grants, views, etc.
    - optimizer - the best approach to execute the query
## bind variable
- reuse the parsing
- temporary variable as place holders, later replaced with values
- utilize a query multiple times
  - sql - bind variable are defined with the := character
  - java - PreparedStatement, CallableStatement

## sql examples from video
- create table t1 (pk int, data char(100));
- insert into t1 select case when rownum < 20 then rownum else 20 end, rownum from dual connect by level <= 500;
- exec dbms_stats.gather_table_stats('', 't1');
- exec dbms_stats.gather_table_stats('', 't1', method_opt => 'for columns pk size 200');
- select histogram from user_tab_col_statistics where table_name = 't1' and column_name = 'pk';
- alter system flush shared_pool; -- need privilege to execute this
- alter session set "_optim_peek_user_binds" = true; -- default setting

- query example
  - select max(data) max_data from t1 where pk = 19;
  - select max(data) max_data from t1 where pk = 20;

- bind variable example
  - variable v01 number;
  - exec :v01 := 10;
  - select max(data) max_data from t1 where pk = :v01;
