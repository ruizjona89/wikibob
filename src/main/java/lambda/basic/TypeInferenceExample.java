package lambda.basic;

// TypeInference - the compile figures out the expression with the type in this case StringLengthLambda
public class TypeInferenceExample {

	public static void main(String[] args) {



		// we can now make a shortcut, from (String string) to (string) because we know what is
		// being input to the getLength method
		// StringLengthLambda stringLengthLambda = (String string) -> string.length();
		// StringLengthLambda stringLengthLambda = (string) -> string.length();

		// and we can make this even shorter, removing the "(" and ")" to just string
		StringLengthLambda stringLengthLambda = string -> string.length();
		System.out.println(stringLengthLambda.getLength("Hello Lambda"));
		
		printLambda(string -> string.length());
	}
	
	public static void printLambda(StringLengthLambda stringLengthLambda) {
		System.out.println(stringLengthLambda.getLength("Hello Lambda2"));
	}

	interface StringLengthLambda {
		int getLength(String string);
	}

}
