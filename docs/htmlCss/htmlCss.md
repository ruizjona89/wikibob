# HyperText Markup Language (HTML) / Cascading Style Sheets (CSS)

- [Hypertext Transfer Protocol (HTTP)](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)
|Method|Action|
|delete|delete|
|get|select|
|post|insert into|
|put|update|

# Links
- [hangman](https://codepen.io/cathydutton/pen/ldazc)

## [@media](media.md)
- @media standards for various displays