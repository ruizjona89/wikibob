//https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
package mathematics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class RPN {

	private static List<String> tokens = new ArrayList<>();

	public static int evalRPN() {

		String operators = "+-*/";

		Stack<String> stack = new Stack<String>();

		for (String token : tokens) {
			if (!operators.contains(token)) {

				if (token.contains("(")) {
					continue;
				} else if (token.contains(")")) {
					continue;
				}

				// if a number push on stack
				stack.push(token);

				// if operand pop 2 numbers, compute value and push on stack
			} else {
				int a = Integer.valueOf(stack.pop());
				int b = Integer.valueOf(stack.pop());
				int index = operators.indexOf(token);
				switch (index) {
				case 0:
					stack.push(String.valueOf(a + b));
					break;
				case 1:
					stack.push(String.valueOf(b - a));
					break;
				case 2:
					stack.push(String.valueOf(a * b));
					break;
				case 3:
					stack.push(String.valueOf(b / a));
					break;
				}
			}
		}

		// return the final number on the stack
		return Integer.valueOf(stack.pop());
	}

	public static void checkParenthesesMatching() {

		int tokenCount = 0;

		for (int i = 0; i < tokens.size(); i++) {

			if (tokens.get(i).equals("(")) {
				tokenCount++;
			} else if (tokens.get(i).equals(")")) {
				tokenCount--;
			}
		}

		if (tokenCount != 0) {
			System.err.println("Does not have matching parentheses");
		}
	}

	public static void main(String[] args) {

//		String[] tokens = new String[] { "2", "1", "+", "3", "*" };
//		String[] tokens = new String[] { "2", "1", "+", "3", "*", "7", "-", "32", "*" };
//		String string = "2 1 + 3 * 7 - 32 *";

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter String to Compute, e.g. 13 64 + \n");

		// gets user input
		String string = keyboard.nextLine();

		// put the string into a String array and then uses the Arrays Class to import
		// into an ArrayList
		tokens = Arrays.asList(string.split(" "));

		for (int i = 0; i < tokens.size(); i++) {
			System.out.print(tokens.get(i) + " ");
		}

		RPN.checkParenthesesMatching();

		System.out.println("= " + RPN.evalRPN());

		keyboard.close();
	}
}