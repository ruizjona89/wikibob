// https://www.geeksforgeeks.org/comparator-interface-java/
package collections.comparator;

import java.util.ArrayList;
import java.util.Collections;

public class Driver {
	public static void main(String[] args) {

		// ArrayList - object representation of an Array
		// create ArrayList of Students
		ArrayList<Student> arrayList = new ArrayList<Student>();

		// add elements to the ArrayList of type Student
		// add three objects of type Student
		arrayList.add(new Student(111, "bbbb", "london"));
		arrayList.add(new Student(131, "aaaa", "nyc"));
		arrayList.add(new Student(121, "cccc", "jaipur"));

		// now we are going to print out the arrayList unsorted
		System.out.println("Unsorted");
		for (int i = 0; i < arrayList.size(); i++) {
			System.out.println(arrayList.get(i));
		}

		// Sort the Unsorted ArrayList by Name
		Collections.sort(arrayList, new SortByName());

		// Print Sorted ArrayList by Name
		System.out.println("\nSorted By Name");
		for (int i = 0; i < arrayList.size(); i++) {
			System.out.println(arrayList.get(i));
		}

		// Sort the Unsorted ArrayList by Roll
		Collections.sort(arrayList, new SortByRoll());

		// Print Sorted ArrayList by Roll
		System.out.println("\nSorted By Roll");
		for (int i = 0; i < arrayList.size(); i++) {
			System.out.println(arrayList.get(i));
		}
	}
}