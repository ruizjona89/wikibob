//abstract prevents instantiation of a Class
package fundamentals.abstracts.animals;

public abstract class Animal {

	// define an abstract method that must be implemented by subclasses
	abstract public void eat();

	public void walk() {
		System.out.println("Animal walking");
	}

	// overloading the canIDrink method with an extra attribute
	public boolean canIDrink(int age) {
		if (age > 20)
			return true;
		return false;
	}

	public boolean canIDrink() {
		return false;
	}
}
