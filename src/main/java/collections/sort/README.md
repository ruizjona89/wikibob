# collections.sort

## General Sorting Examples

- [Bubble Sort](SortExamples.java) - bubble sort example
- [Collection Sort](SortExamples.java) - collection sort example
- [Insertion Sort](InsertionSort.java) - insertion sort example
- [Merge Sort](MergeSort.java) - merge sort example

- [Loop Sort](LoopSort.java) - loop sort example
- [Selection Sort](SelectionSort.java) - selection sort example

## Comparable vs Comparator

- [Comparable](comparable/Driver.java) - comparing instances of the same type
- [Comparator](comparator/Driver.java) - comparing instances of different types