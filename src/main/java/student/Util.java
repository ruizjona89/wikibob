package student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class Util {

	public static int counter = -1;

	public Util() {

	}

	// Reads the file and builds student array.
	// Open the file using FileReader Object.
	// In a loop read a line using readLine method.
	// Tokenize each line using StringTokenizer Object
	// Each token is converted from String to Integer using parseInt method
	// Value is then saved in the right property of Student Object.
	public static Student[] readFile(String filename, Student[] students) throws StudentGradingException, Exception {

		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			boolean eof = false;
			Student student = new Student();
			int[] scores = new int[5];
			int index = 0;

			while (!eof) {
				String line = bufferedReader.readLine();

				if (line == null)
					eof = true;

				else {
					StringTokenizer stringTokenizer = new StringTokenizer(line);
					student = new Student();
					scores = new int[5];

					while (stringTokenizer.hasMoreTokens()) {
						int sid = Integer.parseInt(stringTokenizer.nextToken());

						for (int i = 0; i < 5; i++) {
							scores[i] = Integer.parseInt(stringTokenizer.nextToken());
						}

						student.setSID(sid);
						student.setScores(scores);
						students[index++] = student;
						counter++;
					}
				}
			}

			bufferedReader.close();

		} catch (IOException e) {

			System.err.println("Util.readFile: Error -- " + e.toString());

			throw new StudentGradingException("Error -- " + e.toString());

		} catch (Exception e) {

			System.err.println("Util.general: Error -- " + e.toString());
			throw new Exception("Error -- " + e.toString());
		}

		return students;
	}
}