# Lambda Expression - anonymous functions
     - without name, return type, access modifier
```
() -> System.out.println("receives no arguments");
(a, b) -> System.out.print(a + b);  // receives 2 arguments and prints the sum
```