# WebStorm - JetBrains JavaScript Integrated Development Environment (IDE)

## Tutorials

- YouTube - [React + TS + TDD](https://www.youtube.com/watch?v=wm8WdAB64gw)
     - Excellent recording of React TypeScript and Test Driven Development
- Recording - [NPM test / start](./testingExample.mp4)
     - Quick example of testing with WebStorm, I will provide a detailed recording later.
     
## Creating a Project

- NUMBER 1!!! - at the time of this document, according to JetBrains, TRUST ME I ASKED, is there a way to convert a Project Type to a different Project Type.  The response was NO.  This is the process to follow, received 20220216T1646GMT:

     - Delete the existing module and add a new one of the correct type, such as Web module instead of Java module.
     - See [Multimodule Projects](https://www.jetbrains.com/help/idea/creating-and-managing-modules.html#multimodule-projects)
     - It's not possible to change the type of the existing module, you can only delete the old module and add a new one.

- I was informed to change the ".iml" file from the previous MODULE to the new MODULE, e.g. JAVA_MODULE to WEB_MODULE would work.  This does not work, for example:
     - ```<module org.jetbrains.idea.maven.project.MavenProjectsManager.isMavenModule="true" type="JAVA_MODULE" version="4">```
     - ```<module type="WEB_MODULE" version="4">```

- The process is to create the WEB_MODULE in a new directory.  Let the process build and then copy your work into the new directory.
     - [Create Project - React Typescript](newProjectReactTypescript.png)

# Configurations

- Linux Setup
     - ```export WEBSTORM_PATH=/opt/webstorm/bin```
     - ```alias webstorm='webstorm.sh &'```

- [Prettier Configuration](./prettier.png)
     - Formatting Code Plugin

- [Jest Testing](./jestAllTests.png)
     - Setting up Testing, this is discussed in details here:  [Jest Testing Recording](https://www.youtube.com/watch?v=Y_TGIsFnvo4) 

- [Debugging Configuration](./debugging.png)
     - Press Run -> Edit Configurations -> Plus -> JavaScript -> Debug
     
- [Debugging Application](./debugApplication.png)
     - Setting up WebStorm Debugging
     
# Notes
- npm test - auto test mode
     - start testing with npm test
     - when the menu appears in the terminal, press a (all tests), this will turn on auto test mode
