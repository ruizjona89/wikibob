package poi;

/**
 * SpreadsheetHeader contains all the header data
 */
public class SpreadsheetHeader {
	/** classification of the report */
	private String classification = "UNCLASSIFIED";
	/** the report name */
	private String reportName = "Report Name";
	/** the point of contact name */
	private String pocName = "POC Name";
	/** the point of contact number */
	private String pocNumber = "000.000.0000";
	/** the last update date / time */
	private String lastUpdated = "Last Updated";

	/**
	 * 
	 * SpreadsheetHeader.java
	 * 
	 * @param classification of the report
	 * @param reportName     the name of the report
	 * @param pocName        the point of contact name
	 * @param pocNumber      the point of contact number
	 * @param lastUpdated    the last update date / time
	 */
	public SpreadsheetHeader(String classification, String reportName, String pocName, String pocNumber,
			String lastUpdated) {
		this.classification = classification;
		this.reportName = reportName;
		this.pocName = pocName;
		this.pocNumber = pocNumber;
		this.lastUpdated = lastUpdated;
	}

	/**
	 * method getClassification returns the classification level
	 * 
	 * @return the classification
	 */
	public final String getClassification() {
		return classification;
	}

	/**
	 * method getReportName returns the report title
	 * 
	 * @return the report title
	 */
	public final String getReportName() {
		return reportName;
	}

	/**
	 * method getPocName returns the Point of Contact Name
	 * 
	 * @return the Point of Contact Name
	 */
	public final String getPocName() {
		return pocName;
	}

	/**
	 * method getPocNumber returns the Point of Contact Number
	 * 
	 * @return the Point of Contact Number
	 */
	public final String getPocNumber() {
		return pocNumber;
	}

	/**
	 * method getLastUpdated returns the last update time
	 * 
	 * @return the the last update time
	 */
	public final String getLastUpdated() {
		return lastUpdated;
	}
}
