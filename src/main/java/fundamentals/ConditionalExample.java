package fundamentals;

import java.util.Scanner;

public class ConditionalExample {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter true if true or anything else will be false: ");
		String input = keyboard.nextLine();

		// logical decision - if statement is true do something else do something else
		if (input.equalsIgnoreCase("true")) {
			System.out.println("The statement is true");
		} else {
			System.out.println("The statement is false");
		}

		System.out.print("Enter one, two or anything else: ");
		input = keyboard.nextLine();

		// logical decision - if statement is true do something else do something else
		if (input.equalsIgnoreCase("one")) {
			System.out.println("The value is one");
		} else if (input.equalsIgnoreCase("two")) {
			System.out.println("The value is two");
		} else {
			System.out.println("The value is NOT one or two");
		}

		keyboard.close();
	}

}
