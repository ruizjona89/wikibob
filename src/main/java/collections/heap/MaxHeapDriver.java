package collections.heap;

// https://www.geeksforgeeks.org/max-heap-in-java/
public class MaxHeapDriver {
	public static void main(String[] args) {
		MaxHeapInterface<String> aHeap = new MaxHeap<>();

		aHeap.add("Jared");
		aHeap.add("Brittany");
		aHeap.add("Brett");
		aHeap.add("Doug");
		aHeap.add("Megan");
		aHeap.add("Jim");
		aHeap.add("Whitney");
		aHeap.add("Matt");
		aHeap.add("Regis");

		if (aHeap.isEmpty())
			System.out.println("The heap is empty");
		else
			System.out.println("The heap is not empty; it contains " + aHeap.getSize() + " entries.");

		System.out.println("The largest entry is " + aHeap.getMax());
		System.out.println("\n\nRemving entries in descending order:");
		while (!aHeap.isEmpty())
			System.out.println("Removing " + aHeap.removeMax());

		System.out.println("\n\nTesting constructor with array parameter:\n");
		String[] nameArray = { "Banana", "Watermelon", "Orange", "Apple", "Kiwi" };

		MaxHeapInterface<String[]> anotherHeap = new MaxHeap<>(nameArray);

		if (anotherHeap.isEmpty())
			System.out.println("The heap is empty");
		else
			System.out.println("The heap is not empty; it contains " + anotherHeap.getSize() + " entries.");

		System.out.println("The largest entry is " + anotherHeap.getMax());
		System.out.println("\n\nRemving entries in descending order:");
		while (!anotherHeap.isEmpty())
			System.out.println("Removing " + anotherHeap.removeMax());
	}
}