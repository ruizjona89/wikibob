# [Map vs FlatMap](https://www.youtube.com/watch?v=TM6TZvVoAko)

## Map Example

```List<Integer> ids = employeesList.stream().map(emp -> emp.getId()).collect(Collectors.toList());```

## FlatMap Example

- Combination of Map and Flat
- Apply Map first then flatten
- Returns a string of values

```Set<String> set = stream1.stream().flatMap(x -> x.getMethod().stream()).collect(Collectors.toSet());```
