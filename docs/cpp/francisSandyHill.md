# ![](../../docs/icons/cv64us50x50.png) Francis Sandy Hill, PhD, University Massachusetts, Amherst (RIP)

# [ECE 660 / 661 - ZIP File](https://gitlab.com/bobby.estey/cpp/-/blob/master/cpp-ece661.zip)

I was fortunate to have two Graphics courses with Dr. Francis Sandy Hill AKA Sandy who was not only fascinating and knowledgeable, Dr. Hill was also an excellent Professor.  Dr. Hill's expertise was Computer Graphics and has written many books, articles, made many presentations and well known in the Computer Graphics World.  What was amazing was my first introduction to Dr. Hill was not in his course, but another Computer Graphics course a year before with Dr. Chuck Anderson from Colorado State University, Greeley, CO who happened to be another Student of Dr. Hill.  Dr. Anderson was also teaching from Dr. Hill's Book.

I was a Student with National Technological University (NTU) where I would receive Satellite recordings from the University Massachusetts, Amherst.  I was in two of Dr. Hill's courses.

- ECE660 Introduction to Computer Graphics
- ECE661 Advanced Computer Graphics
     
Students learned a lot from Dr Hill, anything from basic Graphics to complex mathematical Ray Tracers.  We learned from Dr Hill's book and my last class, I mailed my book for his autograph.

Dr. Hill was a Pleasant, Kind, Generous and SUPER SUPER INTELLIGENT Man.  Dr Hill will always be an excellent role model for anyone.

GOD BLESS YOU Dr. Hill

# Book
|<img src="computerGraphicsFrancisSandyHill1.jpg" width="539" height="300">|<img src="computerGraphicsFrancisSandyHill2.jpg" width="227" height="300">|<img src="autograph.jpg" width="300" height="300">|
|--|--|--|
|Format|Hardcover||
|Language|English||
|ISBN|0023548606||
|ISBN13|9780023548604||
|Release Date|January 1990||
|Publisher|Prentice Hall PTR||
|Length|800 Pages||
|Weight|3.50 lbs||
|Dimensions|1.4" x 8.3" x 10.2"||

# Memories

- [Obituary](https://www.legacy.com/us/obituaries/gazettenet/name/sandy-hill-obituary?id=8257147)
- [Douglass Funeral Service](https://douglassfuneral.com/?tag=sandy-s-hill-sandy-s-hill-obituary-funeralworks-obituary-douglass-funeral-service)