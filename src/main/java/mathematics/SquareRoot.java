package mathematics;

public class SquareRoot {

	public SquareRoot() {

	}

	public void getSquareRoot(double number, double root) {
		boolean isPositive = true;
		double t;
		if (number == 0) {
			System.out.println("Square root is : 0");
		} else if (number < 0) {
			number = -number;
			isPositive = false;
		}

		double squareRoot = number / root;
		do {
			t = squareRoot;
			squareRoot = (t + (number / t)) / root;
		} while ((t - squareRoot) != 0);

		if (isPositive) {
			System.out.println("Square root of " + number + " are");
			System.out.println("+" + squareRoot);
			System.out.println("-" + squareRoot);
		} else {
			System.out.println("Square root of " + number + " are");
			System.out.println("+" + squareRoot + "i");
			System.out.println("-" + squareRoot + "i");
		}
	}

	public static void main(String[] args) {
		SquareRoot squareRoot = new SquareRoot();
		squareRoot.getSquareRoot(64, 2);
	}
}