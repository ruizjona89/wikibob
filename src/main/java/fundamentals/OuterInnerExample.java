// Inner / Nested Class Example
// The Inner Class has access to all attributes and methods (even if private) of the Outer Class
package fundamentals;

public class OuterInnerExample {

	// Outer Class private attribute
	private int privateOuter = 13;

	// Outer Class private method
	private String integer2String() {
		return Integer.toString(privateOuter);
	}

	// Inner Class
	class InnerClass {

		// Inner Class show method - showing access to private attributes and methods
		public void show() {
			System.out.println("InnerClass.show()");
			System.out.println("privateOuter: " + privateOuter);
			privateOuter += 13;
			System.out.println("privateOuter: " + privateOuter);
			System.out.println("integer2String: " + integer2String());
		}
	}

	public static void main(String[] args) {
		OuterInnerExample.InnerClass innerClass = new OuterInnerExample().new InnerClass();
		innerClass.show();
	}
}
