package tomcatMySQL.DynamicWebProject.DynamicWebProject.src.us.cv64;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {

	private static final long serialVersionUID = 1983226292970487732L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public MyServlet() {
        super();
		System.out.println("MyServlet() Constructor");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
	 */
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

		System.out.println("MyServlet.doGet()");
		httpServletResponse.getWriter().append("Served at: ").append(httpServletRequest.getContextPath());
		
		DatabaseManager databaseManager = new DatabaseManager(httpServletRequest, httpServletResponse);
		databaseManager.getConnection();
		databaseManager.executeQuery("show databases");
		databaseManager.closeConnection();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
	 */
	protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
		
		System.out.println("MyServlet.doPost()");
		doGet(httpServletRequest, httpServletResponse);
	}
}