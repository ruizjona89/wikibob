package postscript;

import java.io.PrintWriter;

/**
 * <code>Postscript</code> class
 *
 * <p>This class formats text information into a postscript format.
 */

public class Postscript {

	/**
	* <code>Postscript</code> class
	*
	*  Postscript constructor that sets a PrintWriter, font name, e.g. "Courier",
	*  font scale, e.g. 10, x and y coordinates where to start the print and
	*  if the printout is landscape.
	*
	*  Note:  Postscript's origin starts in the lower left corner and the y-axis
	* increases going up the screen and the x-axis increases going to the right.
	*
	*  @param printWriter is a PrintWriter.
	*  @param fontName is the name of the font.
	*  @param fontScale is the scale of the font.
	*  @param x is the x coordinate.
	*  @param y is the y coordinate.
	*  @param landscape is a boolean telling if a landscape printout.
	*/
	public Postscript(PrintWriter printWriter, String fontName, int fontScale,
	    int x, int y, boolean landscape) {

		printWriter_ = printWriter; // set global variables;
		fontName_ = fontName;
		fontScale_ = fontScale;
		landscape_ = landscape;
		x_ = x;
		y_ = y;

		setPostscript(); // write postscript comment
		setFont(); // set the font name and scale

		if (landscape_) landscape(); // rotate 90% for landscape
		else newPath();
	}
	/**
	* <code>Postscript</code> class
	*
	*  Postscript constructor that sets a PrintWriter, font name, e.g. "Courier",
	*  font scale, e.g. 10.
	*
	*  @param printWriter is a PrintWriter.
	*  @param fontName is the name of the font.
	*  @param fontScale is the scale of the font.
	*/
	public Postscript(PrintWriter printWriter, String fontName, int fontScale) {

		this(printWriter, fontName, fontScale, 0, 0, false);
	}

	/**
	*  <code>Postscript</code> class
	*
	*  Postscript constructor that sets a PrintWriter.
	*
	*  @param printWriter is a PrintWriter.
	*/
	public Postscript(PrintWriter printWriter) {

		this(printWriter, "Courier", 10);
	}

	/**
	*  Method rotates the coordinates 90 degrees.
	*/
	public void landscape() {

		printWriter_.println("newpath 90 rotate\n");
	}

	/**
	*  Method moves the coordinates to a specific x, y coordinate.
	*
	*  @param x is the x coordinate.
	*  @param y is the y coordinate.
	*/
	public void moveTo(int x, int y) {

		printWriter_.println(x + " " + y + " moveto\n");
	}

	/**
	*  Method is a Postscript command that clears out any previous paths.
	*/
	public void newPath() {

		printWriter_.println("newpath");
	}

	/**
	*  Method sets up a string for printing.
	*
	*  @param string is the string that will be printed.
	*/
	public void println(String string) {
	
		if (landscape_) {
			y_ -= fontScale_;
		} else {
			y_ += fontScale_;
		}

		moveTo(x_, y_);
		printWriter_.println("(" + string + ") show\n");
	}

	/**
	*  Method sets the font name and scale.
	*
	*  @param fontName is the name of the font.
	*  @param fontScale is the scale of the font.
	*/
	public void setFont(String fontName, int fontScale) {
	
		printWriter_.println("/" + fontName + " findfont " +
		    fontScale + " scalefont setfont\n");
	}

	/**
	*  Method sets the font name and scale.
	*/
	public void setFont() {

		setFont(fontName_,fontScale_);
	}

	/**
	*  Method sends a postscript comment, telling the printer
	* to interpret the file.
	*/
	public void setPostscript() {

		printWriter_.println("%!\n");
	}

	/**
	*  Method tells the device to render the image.
	*/
	public void showPage() {

		printWriter_.println("showpage\n");
	}

	/** PrintWriter for output */
	private static PrintWriter printWriter_ = null;
	/** A boolean, if true print portrait else landscape */
	private static boolean landscape_ = false;
	/** The name of the font */
	private static String fontName_ = "Courier";
	/** The scale of the font */
	private static int fontScale_ = 10;
	/** The postscript x coordinate */
	private static int x_ = 0;
	/** The postscript y coordinate */
	private static int y_ = 0;
}
