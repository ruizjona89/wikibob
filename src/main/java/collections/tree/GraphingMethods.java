// http://www.java2novice.com/java-collections-and-util/treemap/all-keys/
package collections.tree;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class GraphingMethods {

	/**
	 * Constant used to request a max operation
	 */
	public final static int MAX = 0;

	/**
	 * Constant used to request a min operation
	 */
	public final static int MIN = 1;

	/**
	 * Constant used to request a sum operation
	 */
	public final static int SUM = 2;

	/**
	 * Constant used to request an average operation
	 */
	public final static int AVG = 3;

	/**
	 * The dataSource must consist of one or more lines. If there is not at least
	 * one line, the method throws an IllegalArgumentException whose message
	 * explains what is wrong.
	 * 
	 * Each line must consist of some text (a key), followed by a tab character,
	 * followed by a double literal (a value), followed by a newline.
	 * 
	 * If any lines are encountered that don't meet this criteria, the method throws
	 * an IllegalArgumentException whose message explains what is wrong.
	 * 
	 * Otherwise, the map returned by the method (here called categoryMap) must have
	 * all of these properties:
	 * 
	 * (1) The set of keys contained by categoryMap must be the same as the set of
	 * keys that occur in the Scanner
	 * 
	 * (2) The list valueMap.get(key) must contain exactly the same numbers that
	 * appear as values on lines in the Scanner that begin with key. The values must
	 * occur in the list in the same order as they appear in the Scanner.
	 * 
	 * For example, if the Scanner contains
	 * 
	 * <pre>
	 * Utah        10
	 * Nevada       3
	 * Utah         2
	 * California  14
	 * Arizona     21
	 * Utah         2
	 * California   7
	 * California   6
	 * Nevada      11
	 * California   1
	 * </pre>
	 * 
	 * (where the spaces in each line are intended to be a single tab), then this
	 * map should be returned:
	 * 
	 * <pre>
	 *  Arizona    {21}
	 *  California {14, 7, 6, 1} 
	 *  Nevada     {3, 11}
	 *  Utah       {10, 2, 2}
	 * </pre>
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static TreeMap<String, ArrayList<Double>> readTable(Scanner dataSource) {
		if (dataSource == null) {
			throw new IllegalArgumentException("File contains no lines");
		}

		TreeMap<String, ArrayList<Double>> treeMap = new TreeMap<>();
		String key = "";
		double value = 0;

		do {
			key = dataSource.next(); // has next string
			value = dataSource.nextDouble();

			boolean exist = treeMap.containsKey(key);

			if (exist) {
				ArrayList arrayList = treeMap.get(key);
				arrayList.add(value);

			} else {

				ArrayList arrayList = new ArrayList<Double>();
				treeMap.put(key, arrayList);
				arrayList.add(value);
			}

		} while (dataSource.hasNext());

		return treeMap;
	}

	/**
	 * If categoryMap is of size zero, throws an IllegalArgumentException whose
	 * message explains what is wrong.
	 * 
	 * Else if any of the values in the category map is an empty set, throws an
	 * IllegalArgumentException whose message explains what is wrong.
	 * 
	 * Else if any of the numbers in the categoryMap is not positive, throws an
	 * IllegalAgumentException whose message explains what is wrong.
	 * 
	 * Else if operation is anything other than SUM, AVG, MAX, or MIN, throws an
	 * IllegalArgumentException whose message explains what is wrong.
	 *
	 * Else, returns a TreeMap<String, Double> (here called summaryMap) such that:
	 * 
	 * (1) The sets of keys contained by categoryMap and summaryMap are the same
	 * 
	 * (2) For all keys, summaryMap.get(key) is the result of combining the numbers
	 * contained in the set categoryMap.get(key) using the specified operation. If
	 * the operation is MAX, "combining" means finding the largest of the numbers.
	 * If the operation is MIN, "combining" means finding the smallest of the
	 * numbers. If the operation is SUM, combining means summing the numbers. If the
	 * operation is AVG, combining means averaging the numbers.
	 * 
	 * For example, suppose the categoryMap maps like this:
	 * 
	 * <pre>
	 *  Arizona    {21
	 *  California {14, 7, 6, 1} 
	 *  Nevada     {3, 11}
	 *  Utah       {10, 2, 2}
	 * </pre>
	 * 
	 * and the operation is SUM. The map that is returned must map like this:
	 * 
	 * <pre>
	 *  Arizona    21
	 *  California 28 
	 *  Nevada     14
	 *  Utah       14
	 * </pre>
	 */
	public static TreeMap<String, Double> prepareGraph(TreeMap<String, ArrayList<Double>> categoryMap, int operation) {
		TreeMap<String, Double> summaryMap = new TreeMap<>();
		double value = 0;

		// get all keys in map and put them in variable keys
		Set<String> keys = categoryMap.keySet();

		if (categoryMap.size() == 0) {

			throw new IllegalArgumentException("A value is set to zero");
		}

		for (String key : keys) {
			ArrayList<Double> arrayList = categoryMap.get(key);
			value = 0;

			for (int i = 0; i < arrayList.size(); i++) {
				if (arrayList.get(i) <= 0) {
					throw new IllegalArgumentException("Number is not positive.");
				}
				if (operation == 2) {
					// sum
					value = value + arrayList.get(i);

				} else if (operation == 3) {
					// average
					value = value + arrayList.get(i) / arrayList.size();

				} else if (operation == 0) {
					// https://stackoverflow.com/questions/36211028/how-do-you-find-the-smallest-value-of-an-arraylist
					// how to find largest value, just switched the 'if statement' around to find
					// largest value
					value = arrayList.get(i);
					for (double x : arrayList) {
						if (x > value) {
							value = x;
						}
					}
				} else if (operation == 1) {
					// https://stackoverflow.com/questions/36211028/how-do-you-find-the-smallest-value-of-an-arraylist
					// how to find smallest value
					value = arrayList.get(i);
					for (double x : arrayList) {
						if (x < value) {
							value = x;
						}
					}

				} else {

					throw new IllegalArgumentException("Did not choose MAX, MIN, SUM, or AVG");
				}
			}

			summaryMap.put(key, value);
		}

		return summaryMap;
	}

	/**
	 * If colorMap is empty, throws an IllegalArgumentException.
	 * 
	 * If there is a key in colorMap that does not occur in summaryMap, throws an
	 * IllegalArgumentException whose message explains what is wrong.
	 * 
	 * If any of the numbers in the summaryMap is non-positive, throws an
	 * IllegalArgumentException whose message explains what is wrong.
	 * 
	 * Otherwise, displays on g the subset of the data contained in summaryMap that
	 * has a key that appears in colorMap with either a pie chart (if usePieChart is
	 * true) or a bar graph (otherwise), using the colors in colorMap.
	 * 
	 * Let SUM be the sum of all the values in summaryMap whose keys also appear in
	 * colorMap, let KEY be a key in colorMap, let VALUE be the value to which KEY
	 * maps in summaryMap, and let COLOR be the color to which KEY maps in colorMap.
	 * The area of KEY's slice (in a pie chart) and the length of KEY's bar (in a
	 * bar graph) must be proportional to VALUE/SUM. The slice/bar should be labeled
	 * with both KEY and VALUE, and it should be colored with COLOR.
	 * 
	 * For example, suppose summaryMap has this mapping:
	 * 
	 * <pre>
	 *  Arizona    21
	 *  California 28 
	 *  Nevada     14
	 *  Utah       14
	 * </pre>
	 * 
	 * and colorMap has this mapping:
	 * 
	 * <pre>
	 *  California Color.GREEN
	 *  Nevada     Color.BLUE
	 *  Utah       Color.RED
	 * </pre>
	 * 
	 * Since Arizona does not appear as a key in colorMap, Arizona's entry in
	 * summaryMap is ignored.
	 * 
	 * In a pie chart Utah and Nevada should each have a quarter of the pie and
	 * California should have half. In a bar graph, California's line should be
	 * twice as long as Utah's and Nevada's. Utah's slice/bar should be red,
	 * Nevada's blue, and California's green.
	 * 
	 * The method should display the pie chart or bar graph by drawing on the g
	 * parameter. The example code below draws both a pie chart and a bar graph for
	 * the situation described above.
	 */
	public static void drawGraph(Graphics g, TreeMap<String, Double> summaryMap, TreeMap<String, Color> colorMap,
			boolean usePieChart) {

		final int TOP = 10; // Offset of graph from top edge
		final int LEFT = 10; // Offset of graph from left edge
		final int DIAM = 300; // Diameter of pie chart
		final int WIDTH = 10; // Width of bar in bar chart

		String title = "";
		Color color = null;
		Set<String> keys = summaryMap.keySet();
		ArrayList<String> titles = new ArrayList<>();
		ArrayList<Double> values = new ArrayList<>();
		ArrayList<Color> colors = new ArrayList<>();
		double total = 0;
		int startAngle = 0;

		for (String key : keys) {

			// System.out.println(key);
			double value = summaryMap.get(key);
			// System.out.println(value);
			title = key + " " + value;
			titles.add(title);
			values.add(value);
		}

		Set<String> colorKeys = colorMap.keySet();

		for (String colorKey : colorKeys) {
			color = colorMap.get(colorKey);
			colors.add(color);
		}

		for (Double v : summaryMap.values()) {
			total = total + v;
		}

		if (usePieChart) {

			for (int i = 0; i < summaryMap.size(); i++) {
				double ratio = values.get(i) / total;
				int angleRatio = (int) Math.round(ratio * 360);

				g.setColor(colors.get(i));
				g.fillArc(LEFT, TOP, DIAM, DIAM, startAngle, angleRatio);
				startAngle += angleRatio;
				g.fillRect(LEFT + DIAM + 2 * WIDTH, TOP + 2 * i * WIDTH, WIDTH, WIDTH);
				g.setColor(Color.black);
				g.drawString(titles.get(i), LEFT + DIAM + 4 * WIDTH, TOP + (2 * i + 1) * WIDTH);
			}
		}

		// Draw a bar chart if requested
		else {
			for (int i = 0; i < summaryMap.size(); i++) {
				double ratio = values.get(i) / total;
				g.setColor(colors.get(i));
				g.fillRect((int) (LEFT + DIAM - DIAM * ratio), (TOP + (3 * i) * WIDTH), (int) (DIAM * ratio),
						2 * WIDTH);
				g.setColor(Color.black);
				g.drawString(titles.get(i), LEFT + DIAM + 2 * WIDTH, TOP + (3 * i + 1) * WIDTH + WIDTH / 2);
			}
		}
	}
}
