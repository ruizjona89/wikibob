# Dynamic Web Application (Servlets and Java Server Pages)

## git / videos

- [git](https://github.com/in28minutes/JavaWebApplicationStepByStep)
- [Setting up Java Web Applications](https://www.youtube.com/watch?v=Vvnliarkw48)
- [Passing ArrayList from Servlet to JSP Example](https://www.youtube.com/watch?v=YUGktMly-_0)
- [MariaDB Example] - bad recording()

## create project

- Create Maven Project
- set Packaging to:  war

## run project

- right click project
- select build ...
- Goals:  tomcat7:run
- Run

![configuration](./src/main/resources/webApplication.png)

## examples
- scriptlet.jsp - do not put business logic in JSP (JSPs are views)
    - http://localhost:8080/?isJSP=true
- login.jsp -
    - http://localhost:8080/?username=username1&password=password1


