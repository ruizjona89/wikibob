# Parsing

## Question:

Why is the eXtensible Markup Language (XML) so strict on standards, e.g. well-formedness and validity?

## Answer:

When web browsers were first created the concern was to display a web page and worry about the details at a later date.  Basically HTML is a prototype.  The problem is after several years of the flexibility and the more complicated the languages have become a lot more work was required for web browser software.  Developers had to write conditions for everything that could possibly happen when rendering a web page.   This would make code increase and performance decrease.

By having strict standards the web browsers don't need the extra code checking for all possible ways of validating and displaying the web page.  Well-formedness and Validity use the stack principal (Last In First Out), a very common approach used by developers.

When the web browser program reads the web page, every time a begin element appears (<element>) that element is pushed on the stack.  When an end element appears, (</element>) that element is removed from the stack.  You can only access the top of the stack.  Here's an example, Figure 1, I placed line numbers in front of the HTML file to explain more clearly.

![Figure 1 - HTML File](parsing1.gif)

## Pushing the Stack

Now let's look at the life cycle of the HTML file using the stack by referencing figure 2.  The first element <html> gets pushed on the stack.  The second element <head> now gets pushed onto the stack and on top of the <html> element.  The <html> element can no longer be accessible by any program only the <head> element.  When we get to line 3 we see that the <title> element gets added to the stack, then the contents of the title element, "Parsing Example" is added to the stack.

## Popping the Stack

XML uses the "/" character within the element name for popping elements off of the stack.  Anytime we see the end element that item pops off the stack.  At the end of line 3 we see the end element </title> and this tells the web browser program to go to the first element found and pop the element off the stack, in this case <title>.  Both <title> and the contents "Parsing Example" are popped off the stack.  Line 4 we find the end element </head> which states the next element on the stack must be <head> and pops off the stack and finally the </html> element pops <html> off the stack.

![Figure 2 - Stack Life Cycle](parsing2.gif)

## So what is the big deal?

Developers just need one conditional statement, not hundreds.  When and element tag is found, e.g. <element> then push on the stack and when an end element is found, e.g. </element> check and make sure that the end element name matches EXACTLY the element tag name on the stack.  If not then the web page is invalid.