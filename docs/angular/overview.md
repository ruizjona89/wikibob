# Angular - Single Page Application - "ng" sounds like "Angular"

[Code](https://gitlab.com/bobby.estey/angular)

-  replace the view in the content with another view not the entire page
-  in other words - not all requests from the user going to the client are sent to the server
-  front end / client side JavaScript framework, executes in the Browser
-  MEAN Web Stack (MongoDB - NoSQL Database, Express - Backend Web Framework, Angular, NodeJS - JavaScript Runtime)
-  [Angular is not](https://youtu.be/KhzGSHNhnbI?t=156)
-  [Why Angular](https://youtu.be/KhzGSHNhnbI?t=318)

# Tutorials

-  [Traversy Introduction](https://www.youtube.com/watch?v=KhzGSHNhnbI)
-  [Maximilian Schwarzmuller](https://www.youtube.com/watch?v=AaNZBrP26LQ)
-  [Mosh](https://www.youtube.com/watch?v=_-CD_5YhJTA)
-  [Angular Route Parameters](https://www.youtube.com/watch?v=kcEs2ZajSR8)

# Basics

-  Node Package Manager (NPM) installs packages and dependencies
    -  handles incoming user requests and events due to URL changes, DOM events, key / mouse events, etc.
    
-  TypeScript is a superset of JavaScript, offers more features, Types, Classes, Interfaces, ...
    -  is compiled to JavaScript in the Browser
    -  (Arrow Function) =>
        -  Old-school:  var a2 = a.map(function(s){ return s.length });
        -  ECMAscript 6 using arrow functions:  var a3 = a.map( s => s.length );

# Installation

-  [nodejs](https://nodejs.org) is used to serve the pages
    -  cd ~
    -  sudo apt-get install software-properties-common
    -  sudo apt-get install nodejs npm
    -  sudo apt-get update
    -  node --version
    
-  Angular
    -  sudo npm install -g @angular/cli@latest

    ![ng version](ngVersion.png)
    
# Create Project

-  change directory to git repository, e.g.  cd ~/git/cv64/angular
-  ng new <project>, e.g. ng new bobby
-  (optional) ng update
-  (optional) rm -rf node_modules 
-  (optional) npm install <-- reinstall everything

# Terminology
- @ - decorator

- Animation - an illusion of motion

- Build Tools - ng build, ng serve

- @Component - sections of the User Interface (UI), e.g. Navigation Bar, Links Panel, Widgets (buttons, lists, etc.)
     -  selector - custom HTML element, e.g. <selector></selector>
     -  template - HTML code, e.g. <html>...</html>
     
- Data Binding
      - {{string interpolation}}
      - one way binding, e.g. <button(click) = methodInTSfile()>Press</button>
      - two way binding, e.g. <input type="text" [(ngModel)]="twoWayBinding" name="twoWayBindingName" />

- Directive - dynamic HTML elements, e.g. Data Binding (communication).  Modify DOM elements and / or extend their behavior.  Directives are Instructions in the DOM.

Notes:
- is required a structural directive which add / remove elements
attribute directives only change the element
<input type = "text" autoGrow />
<p appTurnGreen>Green Background</p>
<input type = "text" [(ngModel)] = "name">
<p *ngIf="object">if example is {{ anotherObject }} </p>
<p [ngStyle]="{backgroundColor: getColor()}"  // add style to like a divsss
<ngClass]="{online: serverStatus === 'online'}">  // add class to like a div
<li *ngFor="let value of array">{{value}}</li>
<li *ngFor = "let item of items"> {{item}}</li>
<li *ngFor="let name of names; let i = index">{{i}}:  {{name}}</li>
<li *ngFor="let name of names; let i = index">{{i + 1}}:  {{name}}</li>
@Directive({
    selector:'[appTurnGreen]'
})

- Events - an action was started, e.g. click is the event, e.g. <button(click) = methodInTSfile()>Press</button>

- Injector - insert code into other code, e.g. insert a Service into a Component

- [@Input / @Output](https://www.youtube.com/watch?v=v7RNNj8u13g) - pass attributes between parent and child components
     - @Input - parent sends data to child 
          - <childSelector [target]="parentProperty"></childSelector> @Input() item;
     - @Output - child sends data to parent 
          - @Output() newItem = new EventEmitter<string>();
          <button (click)="addItem(newItem.value">Add item</button>

- Pipes - functions used in template (HTML) accepts input and returns an transformed output
        - internationalization, e.g. <button(click) = methodInTSfile()>{{ 'Submit' | localize: strings['Submit'] }}</button>
        
- Routing - allows a Single Page Application (SPA) to build navigation between multiple views

- Resolver - pre-fetching some of the data when the user is navigating from one route to another, do not load the page until the data is received

- @Service - repeat code that can share data and functionality across Components, e.g. Database Connective
     -  @Injectable - inject the service into the Component, e.g. adds a Service to a Component
     
- @Testing - [Karma / Jasmine}(angularUnitTesting.md)

# [Promise / Observable](https://www.youtube.com/watch?v=JFx3amVu1Yg)

- both Promises and Observables work with Asynchronous Data, meaning make a call continue with execution not waiting for the data to return

## Promise (not lazy / tight coupling)

- Emits a Single Value - execute once and is a callback, has two parameters and returns a Promise:
     - @param onfulfilled (success)
     - @param onrejected (failed)
     - @returns - Promise for the completion of which ever callback is executed
     - not lazy - service call is always made
     - cannot be cancelled

```
// method that returns a Promise
method(): Promise<Class> { 
  return this._http.get("http://localhost:port/api/service).map((response: Response) => 
    <Class>response.json()).toPromise().catch(console.error(response);
  }
  
// another example
private getStudyPeriods(): Promise<CurrentPeriod> {
    let data = [];

    return new Promise(resolve => {
        resolve(data);
    });
}
```

## Observable (lazy / loose coupling)

- Emits Multiple Values - executes over time, has three parameters:
     - an event populates the object and then the object is scanned through a Subscriber, Observables execute when an event occurs
     - next (success of each element in the observable)
     - error (if any error)
     - complete (if all elements are completed)
     - lazy - observable is not called until a subscription is made - this.service.method().subscribe((data)...)
     - observable can be cancelled with unsubscribe() method
     - observables have man operators (map, forEach, filter, reduce, retry, retryWhen)

```
// method that returns an Observable
method(): Observable<Class> { 
  return this._http.get("http://localhost:port/api/service).map((response: Response) => 
    <Class>response.json()).catch(console.error(response);
  }
```
        
# Notes

|item|description|
|----|-----------|
|tsc filename.ts && node filename.js|compile ts into js and then execute js|
|ng g component components/user|creates all files for a component|
|ng g service services/data|creates all files for a service|
|put + in front of string will result in a number|+string = number|
|fat arrow =>||
|// int b = abc("def");<br>private int abc(String s) {<br>...<br>return 13;<br>}|// int b = abc("def");<br>abc = str => {<br>...<br>return 13;<br>}|

Modules app.module.ts | Modules bundles Components, Modules, Services into a Package needed for the application
-  components - declarations
-  modules - imports
-  services - providers
## NOTE: node_modules - location of the dependencies|



export class TurnGreenDirective {
   ...
}

Injectors
Services within Angular using Injectable
Keeps components lean
Ideal for AJAX calls
Reusing code
import { Injectable } from '@angular/core';
import { User } from './user';
import { USERS } from './mock-users';

@Injectable()
export class UserService {
    getUsers():  User[] { return USERS; }
}

Inside components, dependency injection is sent to the Constructor, e.g.

constructor a method that is executed when the the Component is created by Angular
constructor(private userService:UserService)
Components (View)
Instructions for how Angular renders an application.  Encapsulates the template data and the behavior of a view.  View Component, main component is called root.  Component is a TypeScript Class.
component decorator contains metadata
javascript tightly coupled with DOM, e.g. $("#title").text("Hello World");
typescript loosely coupled with DOM, typescript:  title = "HW"  <---- DOM (HTML): <h1>Hello</h1>
selector - how to insert the HTML component
ngOnit() -  called when component is called
@Component - add metadata to Component
Scope (Model)
JS Objects / Data
Expressions
Directly linked to the Scope, the page and data source are dynamically updated
Services (Controller)
A Class, that provides Data Access, Logging, Business Logic, Configuration, Providing HTTP Calls, AJAX, etc.
Manage the state of the application
// injector class

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
  constructor(public http:Http) {
    console.log('Data Service Connected...');
  }

  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts').map(res => res.json());
  }
}

// component class

posts:Post[];

// getPosts returns an Observable
this.dataService.getPosts().subscribe((posts) =>)  { 
    console.log(posts);
    this.posts = posts;
});

interface Post {
  id:  number,
  title: string,
  body: string,
  userId: number
}

Routing
Responsible for Navigation, figures out what components to present to the user as they navigate on the current page or external pages
The user thinks you are on the same page, however, routing, we are moving to other pages
// html file - routers need outlets

<ul>
  <li><a routerLink="/">Root</a></li>

  <li><a routerLink="/about">About</a></li></ul>
<router-outlet></router-outlet>

// app.module.ts

import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    { path:'', component:UserComponent },
    { path:'about', component:AboutComponent}
];

imports:  RouterModule.forRoot(appRoutes)

Observables
Works with Asynchronous code
Forms
Forms
Pipes
What is displayed on the form is displayed on the template at runtime
Authentication

Optimizations & NgModules

Deployment

Animations & Testing

app.component.html
the overall HTML for the Single Page
imports
import Angular modules to the project
selector / component name
reference for an XML element, the XML element that will be used to reference the TypeScript file
selector: 'package-package', e.g. app-other
templateURL	external template, holds the HTML code when rendered
template	internal template, e.g. template:'<h1>Inline Template</h1>'
styleUrls:	external style
styles:	internal styles ['h1 { color:red; |}']
DOM Properties
Document Object Model
<img [src] = "...">
<img (click) = "...">
Component Properties
<cmp [initObj] = "...">
<cmd (rndEvent] = "...">
Directive Properties
<div [ngClass] = "...">
<div (ngSubmit) = "...">
View Encapsulation
shadow DOM

Databinding = Communication

TypeScript (Logic) -> Template (HTML)
String Interpolation {{ data }}
Property Binding [property] = 'data'
TypeScript (Logic) <- Template (HTML)

Event Binding (event) = 'expression'
TypeScript (Logic) <-> Template (HTML)

Two-Way Binding [(ngModel)] = 'data'
one way binding
Component -> View
two way binding
Component -> View
View -> Component

// someComponent.ts
  export class    SomeComponent {
    name = "";
  }

// someComponent.html
    <input type="text" [(ngModel)]="name">
    <p>{{ name }}</p>
Data Binding
data binding = communication
output data into the DOM or react to events
TypeScript Code (Business Logic) ----> Template (HTML)
string interpolation: {{expression}} string interpolation array:  {{array.element}}
e.g. {{ getName() }} // get String through a method
property binding: [property] = "data"
e.g. <button [disabled] = "true | false">
TypeScript Code (Business Logic) <---- Template (HTML)
event binding: (event) = "expression"
e.g. <button (click) = method to be executing>
TypeScript Code (Business Logic) <----> Template (HTML)
two-way binding:  [(ngModel)] = "data"
e.g. <input [(ngModel)] = "object">
Property Binding
@Input() propertyName: string;
Event Binding
@Output() eventName = new EventEmitter();
debugging file
app.component.js.map
typescript compiler
gulp / grunt
dependency injection
initialize Class, e.g. Object object;
then inject, initialize the object, e.g. object = new Object();
tightly coupled

interpolation
{{ object }}
promise
emits a single value
not lazy
observable
emits multiple values overtime
lazy - not called until we subscribe to the observable
subscribe(next, error, complete)
cancelled - unsubscribed()
move methods - map, foreach, filter, reduce, retry, retryWhen, etc.
this.service.getControllers().subscribe(control)



Node Package Manager (npm) commands
create new project
cd /git/cv64
ng new <project_name>
generate new component - same level
generate new component - in path
same level within directory
generate new service
ng generate component <component name>
ng g c <component name> <path>
ng g c <component name> --flat -it -is
ng g s <service name>
install angular cli
npm install -g @angular/cli
install bootstrap
npm install --save bootstrap
add app.component.ts
styles = "..node_modules/bootstrap/dist/css/bootstrap.min.css"
install typescript
npm install -g typescript
install product
cd project
npm install <name>
e.g.  npm -i <package name>
installs into package.json
install seed file
cd into the seed directory (project)
npm install <package name>
ng serve
start server on port 4200
npm start
concurrent \"npm run tsc:w\" \"npm run lite\" "
localhost:3000

concurrently execute running typescript in watch mode
concurrently execute running typescript lite webserver
ng build --prod

install google maps
npm install angular2-google-maps --save
Normalize.css makes browsers render all elements more consistently and in line with modern standards. It precisely targets only the styles that need normalizing.	
  npm install normalize.css


Type Script Code
String Binding

{{ name }}
{{ getName() }}
attribute:type = 'value' for instance
name: string = 'Bobby'
age: number = 12;
getName() { return this.name; }
Property Binding

[ property ]
allowNewServer = false;
constructor() {
  setTimeout(() => {
    this.allowNewServer =
  }, 2000);
}

<button class="btn btn-primary" [disabled]="!allowNewServer">Add Server</button>
<p [innerText]="allowNewServer"></p>
   
onMethodName
good practice to start methods with "on" saying that the template is calling TS
Event Binding
( $event )

$event = the data within the event
(click)="onCreateServer()"

<button class="btn btn-primary" [disabled]="!allowNewServer" (click)="onCreateServer()">Add Server</button>

onCreateServer() {
  this.serverCreationStatus = 'Server Created';
}

<input type="text" class="form-control" (input)="onUpdateServerName($event)">
<p>{{serverName}}</p>

onUpdateServerName(event: Event {
  console.log(event);
  this.serverName = (<HTMLInputElement>event.target).value;
}
Two Way Binding
<input type="text" class="form-control" [(ngModel)]="serverName">
<p>{{serverName}}</p>
Structural Directive 
*ngIf
<p *ngIf="serverCreated">Server was created, server name is {{ serverName }}</p>

onCreateServer() {
  this.serverCreated = true;
}

let isMobile: boolean = window.innerWidth < 800;

<p *ngIf=getMobile()>
     {{mobile}} this is true
</p>

<p *ngIf=!getMobile()>
     {{mobile}} this is false
</p>

<p *ngIf="serverCreated; else noServer">Server was created, server name is {{ serverName }}</p>
<ng-template #noServer>
    No Server was created</p>
</ng-template>
Attribute Directive

ngStyle

Note:  [ brackets ] are not part of the directive name but to bind to the property name of the directive which happens to be ngStyle
constructor() {
  this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline';
}

<p [ngStyle="{backgroundColor: getColor()}">]{{ 'Server' }} with ID {{ serverId }} is {{ getServerStatus() }}</p>

getColor() {
  return this.serverStatus === 'online' ? 'green' : 'red';
}
ngClass
<p [ngStyle="{backgroundColor: getColor()}"
      [ngClass]="{online: serverStratus === 'online'}">]{{ 'Server' }} with ID {{ serverId }} is {{ getServerStatus() }}</p>
ngFor
servers = ['Server 1', 'Server 2'];

onCreateServer() {
  this.servers.push(this.serverName);
}

<app-server *ngFor="let server of servers"></app-server>
array example / within an array
  private featureListings() {

    if (!this.listings) {
      return;
    }

    var primaryList = []
    var list = []
    var index = 1;

    this.listings.forEach((item) => {

      console.log(item['id']);
      console.log(item['price']);
      console.log(item['facid']);

      var bobby = item['id'];

      console.log('id: ' + bobby);

      if (item['id'].indexOf('sl')) {
        console.log('sl010 not found');
      } else {
        console.log('found' + bobby);
      }

      if (index < 4) {
        list.push(item);
      }           
Simple code example
class Person {
    name  string;
 
    constructor(name: string) { this.name = name; }

    getName() { return "hello " + this.name;

    let button = document.createElement('button');
    button.textContent - "Button";
    button.onclick = function() { alert(button.getName()); }

document.body.appendChild(button);

let person = new Person("bobby"); {


object
address:Address;

interface Address {street:string, city:string, state:string, zip:number }
array
stuff:string[];   this.stuff= ['a', 'b', 'c'];

npm - 1.6.3
node package manager
change directory to package
cd git/bobbycv64/angular/material-angular-io-cv64
package.json
"name": "@bobbycv64/material-angular-io-cv64"
login to npm
npm login
get username
npm whoami
publish code as public
npm publish --access public

npm account

mkdir nodePackage

create new module
npm init
install to node_module
npm install @bobbycv64/material-angular-io-cv64

pull from node

index.js
var callModule = require('@bobbycv64/material-angular-io-cv64');
callModule.print


Notes:
	rm package-lock.json
npm install --save angularfire2
connector angular and firebase, authentication, real time databases
npm install --save firebase	

https://material.angular.io/guide/getting-started

ng new material-angular-io-cv64
cd material-angular-io-cv64
npm install --save @angular/material @angular/cdk @angular/animations
ng add @angular/material
npm install --save hammerjs

